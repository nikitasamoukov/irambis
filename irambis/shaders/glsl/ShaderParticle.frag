#version 400 core

out vec4 g_color;

in vec2 f_tex_coords;
in vec4 f_color_scale;

uniform sampler2D texture_color;

void main()
{    
    vec4 color = texture(texture_color, f_tex_coords).rgba;
    if (color.a < 0.002)
        discard;
    g_color = vec4(color.rgb * f_color_scale.rgb * f_color_scale.a, color.a * f_color_scale.a);
}