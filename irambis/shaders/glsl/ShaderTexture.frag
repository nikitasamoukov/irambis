#version 400 core

out vec4 g_color;

in vec2 f_tex_coords;

uniform sampler2D texture_color;

void main()
{    
    vec4 color = texture(texture_color, f_tex_coords).rgba;
    if (color.a < 0.002)
        discard;
    g_color = color.rgba;
}