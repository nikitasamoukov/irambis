#version 400 core

out vec4 g_color;

in vec2 f_tex_coords;

uniform sampler2D texture_color;
uniform vec4 color_scale;
uniform vec3 color_add;

void main()
{    
    vec4 color = texture(texture_color, f_tex_coords).rgba;
    if (color.a < 0.002)
        discard;

    g_color = color_scale * color.rgba + vec4(color_add, 0.0f);
}