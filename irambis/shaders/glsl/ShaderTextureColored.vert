#version 400 core
layout (location = 0) in vec2 a_pos;
layout (location = 1) in vec2 a_tex_coords;
layout (location = 2) in vec3 a_instance_model;

out vec2 f_tex_coords;

uniform mat3 view;
uniform mat3 projection;
uniform float z_pos;
uniform float pos_scale;

void main()
{
    f_tex_coords = a_tex_coords;
    mat3 mat;
    
    float sin_v=sin(a_instance_model.z);
    float cos_v=cos(a_instance_model.z);

    mat[0]=vec3( cos_v*pos_scale, sin_v*pos_scale,0);
    mat[1]=vec3(-sin_v*pos_scale, cos_v*pos_scale,0);
    mat[2]=vec3(a_instance_model.x, a_instance_model.y, 1);
    
    gl_Position = vec4((projection * view * mat * vec3(a_pos, 1.0f)).xy, z_pos, 1.0f); 
}
