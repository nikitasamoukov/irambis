#version 400 core

out vec4 g_color;

in vec2 f_tex_coords;
in vec3 f_color_scale;

uniform sampler2D texture_color;

void main()
{    
    float mipmapLevel = textureQueryLod(texture_color, f_tex_coords).x;
    vec4 color = texture(texture_color, f_tex_coords).rgba;
    if (color.a < 0.002)
        discard;
        
    if(mipmapLevel > 0) {
        g_color = vec4(f_color_scale, 1.0f) * color.rgba * pow(2.0f, mipmapLevel); // HACK correct
    } else {
        g_color = vec4(f_color_scale, 1.0f) * color.rgba;
    }
}