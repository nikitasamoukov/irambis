#version 400 core
layout (location = 0) in vec2 a_pos;
layout (location = 1) in vec2 a_tex_coords;
layout (location = 2) in float a_dist_scale;
layout (location = 3) in vec4 a_instance_pos_pos;
layout (location = 4) in vec4 a_instance_color_scale_laser_width;


out vec2 f_tex_coords;
out vec3 f_color_scale;

uniform mat3 view;
uniform mat3 projection;
uniform float z_pos;
uniform float pos_scale;

void main()
{
    f_tex_coords = a_tex_coords;
    f_color_scale = a_instance_color_scale_laser_width.rgb;
    
    float laser_width = a_instance_color_scale_laser_width.w / 2;

    mat3 mat;

    vec2 vector_laser = a_instance_pos_pos.zw - a_instance_pos_pos.xy;
    
    float vector_laser_dist = length(vector_laser);
    float vector_laser_angle = atan(vector_laser.y, vector_laser.x); // HACK oops asin for sin later...
    
    float sin_v = sin(vector_laser_angle);
    float cos_v = cos(vector_laser_angle);

    float res_pos_scale = pos_scale * laser_width;

    mat[0]=vec3( cos_v*res_pos_scale, sin_v*res_pos_scale,0);
    mat[1]=vec3(-sin_v*res_pos_scale, cos_v*res_pos_scale,0);
    mat[2]=vec3(a_instance_pos_pos.x, a_instance_pos_pos.y, 1);
    
    gl_Position = vec4((projection * view * mat * vec3(a_pos + vec2(a_dist_scale / res_pos_scale, 0) * vector_laser_dist, 1.0f)).xy, z_pos, 1.0f); 
}
