#version 400 core

out vec4 g_color;

in vec2 f_tex_coords;

uniform sampler2D texture_color;

void main()
{
    int mipmapLevel = int(textureQueryLod(texture_color, f_tex_coords).x);

	vec2 texture_color_size = textureSize(texture_color, mipmapLevel);

	if (mipmapLevel > 0)
	{
		//texture_color_size /= 3;
	}

	float alpha_edge[4];
	alpha_edge[0] = texture(texture_color, f_tex_coords + vec2(+1.0f, +1.0f) / texture_color_size).a;
	alpha_edge[1] = texture(texture_color, f_tex_coords + vec2(-1.0f, +1.0f) / texture_color_size).a;
	alpha_edge[2] = texture(texture_color, f_tex_coords + vec2(-1.0f, -1.0f) / texture_color_size).a;
	alpha_edge[3] = texture(texture_color, f_tex_coords + vec2(+1.0f, -1.0f) / texture_color_size).a;

	vec4 alpha = texture(texture_color, f_tex_coords).rgba;

	if (alpha.a >= 0.99)
		discard;
	if (alpha_edge[0] < 0.002f && alpha_edge[1] < 0.002f && alpha_edge[2] < 0.002f && alpha_edge[3] < 0.002f)
		discard;
	g_color = vec4(0, 1, 1, 1);
}