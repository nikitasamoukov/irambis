#include "Assets.h"
#include "components.h"

Assets assets;

Assets::Assets()
{
	Assert(!_is_exist);
	_is_exist = true;
}

void Assets::Load()
{
	LoadTextures();
	LoadObjects();
}

void Assets::Free()
{
	for (auto& texture : _textures)
	{
		texture.Free();
	}
	_textures.clear();
}

bool Assets::HaveTexture(const string& name)
{
	return _texture_name_to_id.find(name) != _texture_name_to_id.end();
}

Texture& Assets::GetTexture(TextureId id)
{
	return _textures[id.id];
}

Texture& Assets::GetTexture(const string& name)
{
	AssertMsg(HaveTexture(name), "No texture: " + name);
	return _textures[_texture_name_to_id[name].id];
}

TextureId Assets::GetTextureId(const string& name)
{
	AssertMsg(HaveTexture(name), "No texture: " + name);
	return _texture_name_to_id[name];
}

string Assets::GetName(TextureId id)
{
	for (auto& [map_name, map_id] : _texture_name_to_id)
	{
		if (id == map_id)
			return map_name;
	}
	return "Unknown";
}

string Assets::GetName(BoxedObjectId id)
{
	for (auto& [map_name, map_id] : _object_name_to_id)
	{
		if (id == map_id)
			return map_name;
	}
	return "Unknown";
}

bool Assets::HaveBoxedObject(const string& name)
{
	return _object_name_to_id.find(name) != _object_name_to_id.end();
}

const BoxedObject& Assets::GetBoxedObject(const string& name)
{
	AssertMsg(HaveBoxedObject(name), "No object: " + name);
	return _objects[_object_name_to_id[name].id];
}

const BoxedObject& Assets::GetBoxedObject(BoxedObjectId id)
{
	return _objects[id.id];
}

BoxedObjectId Assets::GetBoxedObjectId(const string& name)
{
	AssertMsg(HaveBoxedObject(name), "No object: " + name);
	return _object_name_to_id[name];
}

bool Assets::HasTextureDestruction(const TextureId& id)
{
	return _destruction_particles.find(id) != _destruction_particles.end();
}

const DestructionParticles& Assets::GetTextureDestruction(const TextureId& id)
{
	return std::any_cast<DestructionParticles&>(_destruction_particles[id]);
}

void Assets::BindTexture(TextureId id)
{
	glBindTexture(GL_TEXTURE_2D, GetTexture(id).GetId());
}

void Assets::LoadTextures()
{
	fs::path path_img_folder = "./img/";

	xml_document doc;
	auto result = doc.load_file(textures_xml_path);

	
	AssertMsg(result.status != pugi::status_file_not_found, (string)"File not found:" + textures_xml_path);
	AssertMsg(result.status == pugi::status_ok, (string)"File not loaded:" + textures_xml_path);
	if (result.status != pugi::status_ok) std::terminate();

	for (auto node : doc.children("texture"))
	{
		string texture_name = node.attribute("name").as_string();
		auto path_img = path_img_folder / ((string)node.attribute("name").as_string() + ".png");
		TextureId tex_id = { _textures.size() };

		DestructionParticles* dp = nullptr;
		bool gen_destruction = node.attribute("gen_destruction").as_bool(true);
		if (gen_destruction)
		{
			_destruction_particles[tex_id] = DestructionParticles();
			dp = &std::any_cast<DestructionParticles&>(_destruction_particles[tex_id]);
		}

		if (node.attribute("gen_build").as_bool())
		{
			string texture_build_name = texture_name + "_build";
			TextureId tex_build_id = { _textures.size() + 1 };

			DestructionParticles* dp_build = nullptr;
			if (gen_destruction)
			{
				_destruction_particles[tex_build_id] = DestructionParticles();
				dp_build = &std::any_cast<DestructionParticles&>(_destruction_particles[tex_build_id]);
			}

			auto [texture, texture_build] = Texture::LoadWithBuildGen(path_img, node.attribute("z").as_float(), dp, dp_build);
			_texture_name_to_id[texture_name] = tex_id;
			_textures.push_back(texture);

			AssertMsg(_texture_name_to_id.find(texture_build_name) == _texture_name_to_id.end(), "Dup texture:" + texture_build_name);
			_texture_name_to_id[texture_build_name] = tex_build_id;
			_textures.push_back(texture_build);
		}
		else
		{
			Texture texture = Texture::Load(path_img, node.attribute("z").as_float(), dp);
			_texture_name_to_id[texture_name] = tex_id;
			_textures.push_back(texture);
		}
	}
}

void Assets::LoadObject(xml_node node)
{
	Assert(strcmp(node.name(), "object") == 0);

	string object_name = node.attribute("name").as_string();
	AssertMsg(_object_name_to_id.find(object_name) == _object_name_to_id.end(), "Name dup:" + object_name);

	BoxedObjectId id{ _objects.size() };

	BoxedObjectLoader loader(_registry_templates);
	auto [object, entity_template] = loader.Load(node, id);

	_object_name_to_id.insert({ object_name, id });

	_objects.push_back(object);
}

void Assets::LoadObjects()
{
	xml_document doc;
	auto result = doc.load_file(objects_xml_path);

	AssertMsg(result.status != pugi::status_file_not_found, (string)"File not found:" + objects_xml_path);
	AssertMsg(result.status == pugi::status_ok, (string)"File not loaded:" + objects_xml_path);

	for (auto node : doc.children("object"))
	{
		LoadObject(node);
	}

	ProcessUpgrades();
}

void Assets::ProcessUpgrades()
{
	for (auto& bo : _objects)
	{
		if (!bo.Has<StructUpgradesCDesc>()) continue;

		for (auto& upgrade_bo_name : bo.Get<StructUpgradesCDesc>().upgrades)
		{
			AssertMsg(HaveBoxedObject(upgrade_bo_name), "Object upgrade not exist: " + upgrade_bo_name);
			AssertMsg(GetBoxedObjectId(upgrade_bo_name) != bo.Id(), "Object upgrade self: " + upgrade_bo_name);

			auto& up_bo = GetBoxedObject(upgrade_bo_name);

			if (up_bo.Has<StructBuildingC>())
			{
				_registry_templates.get<StructBuildingC>(up_bo._entity_template).is_upgrade = true;
			}
		}
	}

	for (auto& bo : _objects)
	{
		if (!bo.Has<StructBuildingC>()) continue;
		if (bo.Get<StructBuildingC>().is_upgrade) continue;

		auto& struct_building_c = _registry_templates.get<StructBuildingC>(bo._entity_template);
		struct_building_c.cost_sell = struct_building_c.cost;
		struct_building_c.cost_full = struct_building_c.cost;


		queue<pair<f32, BoxedObjectId>> to_ex;
		to_ex.push({ struct_building_c.cost_sell, bo.Id() });

		while (!to_ex.empty())
		{
			auto [cost_ex, bo_id_ex] = to_ex.front();
			to_ex.pop();

			auto& bo_ex = GetBoxedObject(bo_id_ex);

			if (!bo_ex.Has<StructUpgradesCDesc>()) continue;

			for (auto& upgrade_bo_name : bo_ex.Get<StructUpgradesCDesc>().upgrades)
			{
				auto& up_bo = GetBoxedObject(upgrade_bo_name);

				if (up_bo.Has<StructBuildingC>())
				{
					auto& up_struct_building_c = _registry_templates.get<StructBuildingC>(up_bo._entity_template);
					up_struct_building_c.cost_sell = up_struct_building_c.cost * up_sell_scale + cost_ex;
					up_struct_building_c.cost_full = up_struct_building_c.cost + bo_ex.Get<StructBuildingC>().cost_full;
					to_ex.push({ up_struct_building_c.cost_sell, GetBoxedObjectId(upgrade_bo_name) });
				}
			}
		}
	}
}