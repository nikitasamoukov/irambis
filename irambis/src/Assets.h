#pragma once
#include "pch.h"
#include "Texture.h"
#include "BoxedObject.h"
#include "common/IdTypes.h"

inline const char* objects_xml_path = "objects.xml";
inline const char* textures_xml_path = "textures.xml";

struct DestructionParticles;

class Assets
{
public:
	Assets();

	void Load();
	void Free();

	bool HaveTexture(const string& name);
	Texture& GetTexture(TextureId id);
	Texture& GetTexture(const string& name);
	TextureId GetTextureId(const string& name);
	string GetName(TextureId id);

	string GetName(BoxedObjectId id);
	const auto& ObjectsIdMap() { return _object_name_to_id; }
	bool HaveBoxedObject(const string& name);
	const BoxedObject& GetBoxedObject(const string& name);
	const BoxedObject& GetBoxedObject(BoxedObjectId id);
	BoxedObjectId GetBoxedObjectId(const string& name);
	
	bool HasTextureDestruction(const TextureId& id);
	const DestructionParticles& GetTextureDestruction(const TextureId& id);

	void BindTexture(TextureId id);

private:
	void LoadTextures();

	void LoadObjects();
	void ProcessUpgrades();
	void LoadObject(xml_node node);

	vector<Texture> _textures;
	vector<BoxedObject> _objects;
	entt::registry _registry_templates;

	unordered_map<TextureId, any> _destruction_particles;

	bool _is_exist = false;

	unordered_map<string, TextureId> _texture_name_to_id;
	unordered_map<string, BoxedObjectId> _object_name_to_id;

	friend class BoxedObject;
};

extern Assets assets;
