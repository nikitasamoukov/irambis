#include "BoxedC.h"
#include "registry.h"

void BoxedC::CopyTo(entt::entity entity) const
{
	CopyTo(entity, registry);
}

void BoxedC::CopyTo(entt::entity entity, entt::registry& func_registry) const
{
	_copy_func(*this, func_registry, entity);
}

void BoxedC::RemoveFrom(entt::entity entity) const
{
	_remove_func(registry, entity);
}
