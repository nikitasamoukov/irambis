#pragma once
#include "pch.h"

struct BoxedC
{
	template<typename C>
	explicit BoxedC(const C& c)
	{
		_any_component = c;
		_copy_func = [](const BoxedC& boxed_c, entt::registry& func_registry, entt::entity entity)
		{
			const auto& component = std::any_cast<C>(boxed_c._any_component);
			func_registry.emplace_or_replace<C>(entity, component);
		};
		_remove_func = [](entt::registry& func_registry, entt::entity entity)
		{
			func_registry.remove_if_exists<C>(entity);
		};
	}
	void CopyTo(entt::entity entity) const;
	void CopyTo(entt::entity entity, entt::registry& func_registry) const;
	void RemoveFrom(entt::entity entity) const;

private:
	std::any _any_component;
	function<void(const BoxedC& boxed_c, entt::registry&, entt::entity)> _copy_func;
	function<void(entt::registry&, entt::entity)> _remove_func;
};
