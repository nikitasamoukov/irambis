#include "BoxedObject.h"
#include "components.h"
#include "registry.h"
#include "Assets.h"


template<typename C>
BoxedC LoadC(xml_node node)
{
	C component{};
	component.Load(node);
	return BoxedC{ component };
}

entt::entity BoxedObject::CreateBuildOnly() const
{
	entt::entity entity = registry.create();
	registry.emplace_or_replace<TrsC>(entity);
	PlaceToPartBuild(entity);
	registry.emplace_or_replace<RenderC>(entity, RenderC{ _build_texture });
	return entity;
}

entt::entity BoxedObject::CreateFull() const
{
	entt::entity entity = registry.create();
	registry.emplace_or_replace<TrsC>(entity);
	PlaceToPartBuild(entity);
	PlaceToPartReady(entity);
	registry.remove_if_exists<StructBuildingC>(entity);
	return entity;
}

void BoxedObject::PlaceToPartReady(entt::entity entity) const
{
	for (auto& boxed_c : _components_ready)
	{
		boxed_c.CopyTo(entity);
	}

	if (registry.all_of<TurretRotateC>(entity) && registry.all_of<TrsC>(entity)) // HACK change rotate
	{
		auto pos = registry.get<TrsC>(entity).pos;
		if (pos.x == 0 && pos.y == 0)
		{
			pos.x = 1;
		}
		registry.get<TurretRotateC>(entity).a = atan2(pos.y, pos.x);
	}
}

void BoxedObject::PlaceToPartBuild(entt::entity entity) const
{
	for (auto& boxed_c : _components_build)
	{
		boxed_c.CopyTo(entity);
	}
}

void BoxedObject::UpgradeTo(const BoxedObject& up_bo, entt::entity entity) const
{

	// HACK Bad way upd
	// HpC EnergyC TypeC
	// StructBuildingC

	f32 hp = 0;
	if (registry.all_of<HpC>(entity))
		hp = registry.get<HpC>(entity).hp;
	f32 en = 0;
	if (registry.all_of<EnergyC>(entity))
		en = registry.get<EnergyC>(entity).en;


	for (auto& boxed_c : _components_ready)
	{
		boxed_c.RemoveFrom(entity);
	}

	//HpC
	f32 prev_hp_max = 0;
	if (Has<HpC>())
		prev_hp_max = Get<HpC>().hp_max;

	f32 up_hp_max = 0;
	if (up_bo.Has<HpC>())
	{
		up_hp_max = up_bo.Get<HpC>().hp_max;

		registry.emplace_or_replace<HpC>(entity, HpC{ clamp(hp, 0.0f, up_hp_max), up_hp_max });
	}
	else
	{
		registry.remove_if_exists<HpC>(entity);
	}

	//StructBuildingC
	if (up_bo.Has<StructBuildingC>())
	{
		registry.emplace_or_replace<StructBuildingC>(entity, up_bo.Get<StructBuildingC>());
		if (up_bo.Has<HpC>())
		{
			registry.get<StructBuildingC>(entity).hp_build = max((up_hp_max - prev_hp_max) * (1 + 0.01f), 0.0f);
		}
	}

	//EnergyC
	if (up_bo.Has<EnergyC>())
	{
		f32 up_en_max = up_bo.Get<EnergyC>().en_max;
		registry.emplace_or_replace<EnergySaveWhenUpgradeC>(entity, EnergySaveWhenUpgradeC{ clamp(en, 0.0f, up_en_max) });
	}
	else
	{
		registry.remove_if_exists<EnergyC>(entity);
	}

	//TypeC
	registry.emplace_or_replace<TypeC>(entity, TypeC{ up_bo.Id() });

	//RenderC
	registry.emplace_or_replace<RenderC>(entity, RenderC{ up_bo.BuildTexture() });
}

BoxedObjectId BoxedObject::Id() const
{
	return _id;
}

float BoxedObject::Size() const
{
	return _registry_templates.get<PhysC>(_entity_template).r;
}

float BoxedObject::Cost() const
{
	if (_registry_templates.all_of<StructBuildingC>(_entity_template))
		return _registry_templates.get<StructBuildingC>(_entity_template).cost;
	return 0;
}

f32 BoxedObject::CostSell(Object obj) const
{
	if (obj.IsNull()) return 0;

	if (Has<StructBuildingC>())
	{
		if (registry.all_of<StructZeroRewardSell>(obj.entity))
			return 0;

		if (registry.all_of<HpC>(obj.entity))
		{
			auto& hp_c = registry.get<HpC>(obj.entity);

			if (registry.all_of<StructBuildingC>(obj.entity))
			{
				auto& struct_building_c = registry.get<StructBuildingC>(obj.entity);
				f32 hp_at_end_build = hp_c.hp + struct_building_c.hp_build * max(0.0f, (1 - struct_building_c.build_progress));
				return Get<StructBuildingC>().cost_sell * clamp(hp_at_end_build / max(0.0001f, hp_c.hp_max), 0.0f, 1.0f);
			}

			return Get<StructBuildingC>().cost_sell * clamp(hp_c.hp / max(0.0001f, hp_c.hp_max), 0.0f, 1.0f);
		}
		return Get<StructBuildingC>().cost_sell;
	}
	return 0;
}

bool BoxedObject::IsStruct() const
{
	return assets._registry_templates.all_of<StructC>(_entity_template);
}

TextureId BoxedObject::BuildTexture() const
{
	return _build_texture;
}

string BoxedObject::InfoTextBuild() const
{
	return InfoText(true);
}

string BoxedObject::InfoTextObject() const
{
	return InfoText(false);
}

string BoxedObject::InfoText(bool is_build_include) const
{
	string res;

	if (is_build_include)
	{
		res += "Cost: " + ToString(Cost(), 0) + "\n";

		if (Has<StructBuildingC>())
		{
			auto& c = Get<StructBuildingC>();
			res += "Build time: " + ToString(c.build_time, 0) + "\n";
			res += "Build energy: " + ToString(c.build_energy_per_second * c.build_time, 0) + "\n";
		}

		if (Has<HpC>())
		{
			auto& c = Get<HpC>();
			res += "Hp: " + ToString(c.hp_max, 0) + "\n";
		}

		if (Has<ConnectIdC>())
		{
			auto& c = Get<ConnectIdC>();
			if (!c.is_only_one_connect)
			{
				res += "Can connect multiply\n";
			}
		}

		if (Has<EnergyC>())
		{
			auto& c = Get<EnergyC>();
			res += "Energy store: " + ToString(c.en_max, 0) + "\n";
		}
	}

	if (Has<EnergyC>())
	{
		auto& c = Get<EnergyC>();
		if (c.gen > 0)
		{
			res += "Energy generation: " + ToString(c.gen, 1) + "\n";
		}
	}
	
	if (Has<TurretC>())
	{
		auto& c = Get<TurretC>();
		res += "Range: " + ToString(c.range, 0) + "\n";
	}

	if (Has<TurretLaserC>())
	{
		auto& c = Get<TurretLaserC>();
		res += "DPS: " + ToString(c.dps, 1) + "\n";
		res += "Energy / second: " + ToString(c.en_per_second, 1) + "\n";
	}

	if (Has<TurretHighLaserC>())
	{
		auto& c = Get<TurretHighLaserC>();
		res += "Max DPS: " + ToString(c.dps, 1) + "\n";
		res += "Max energy / second: " + ToString(c.en_per_second, 1) + "\n";
		res += "Charge time: " + ToString(1.0f / max(0.00001f, c.charge_per_frame * frames_per_second), 0) + "\n";
		res += "Targeting heavy\n";
	}

	if (Has<TurretCannonC>())
	{
		auto& c = Get<TurretCannonC>();

		f32 dps = 0;
		auto& shoot_bo = assets.GetBoxedObject(c.shoot_obj_id);
		f32 shoot_dmg = 0;
		if (shoot_bo.Has<DmgC>())
			shoot_dmg = shoot_bo.Get<DmgC>().dmg;
		dps = shoot_dmg / max(0.00001f, c.shoot_cooldown) * frames_per_second;

		res += "DPS: " + ToString(dps, 1) + "\n";
		res += "Energy / second: " + ToString(c.en_per_shoot / max(0.00001f, c.shoot_cooldown) * frames_per_second, 1) + "\n";
		res += "Attack speed: " + ToString(1.0f / max(0.00001f, c.shoot_cooldown) * frames_per_second, 2) + "\n";
	}


	return res;
}

void BoxedObject::PlaceToFull(entt::entity entity, entt::registry& registry) const
{
	for (auto& boxed_c : _components_build)
	{
		boxed_c.CopyTo(entity, registry);
	}
	for (auto& boxed_c : _components_ready)
	{
		boxed_c.CopyTo(entity, registry);
	}
}

enum class ComponentType
{
	Build,
	Ready,
	Desc,
};

#define MACRO_LOADER_C(CT, C) { #C, {ComponentType::CT, LoadC<C>} },

unordered_map<string, pair<ComponentType, function<BoxedC(xml_node)>>> loaders{
	MACRO_LOADER_C(Build, HpC)
	MACRO_LOADER_C(Build, PhysC)
	MACRO_LOADER_C(Build, EnemyC)
	MACRO_LOADER_C(Build, StructC)
	MACRO_LOADER_C(Build, ConnectIdC)
	MACRO_LOADER_C(Build, ConnectPosC)
	MACRO_LOADER_C(Build, StructBuildingC)

	MACRO_LOADER_C(Ready, RenderC)
	MACRO_LOADER_C(Ready, EnergyC)
	MACRO_LOADER_C(Ready, TurretC)
	MACRO_LOADER_C(Ready, TurretLaserC)
	MACRO_LOADER_C(Ready, TurretHighLaserC)
	MACRO_LOADER_C(Ready, TurretRepairC)
	MACRO_LOADER_C(Ready, RenderTurretC)
	MACRO_LOADER_C(Ready, TurretRotateC)
	MACRO_LOADER_C(Ready, TurretCannonC)
	MACRO_LOADER_C(Ready, DmgC)
	MACRO_LOADER_C(Ready, BulletC)
	MACRO_LOADER_C(Ready, EnemyShootingC)
	MACRO_LOADER_C(Ready, EnemyRamC)
	MACRO_LOADER_C(Ready, EnemyLaserC)
	MACRO_LOADER_C(Ready, EnemySplitC)
	MACRO_LOADER_C(Ready, RenderEnergyC)

	MACRO_LOADER_C(Desc, StructUpgradesCDesc)
	MACRO_LOADER_C(Desc, EnemyCDesc)
};


tuple<BoxedObject, entt::entity> BoxedObjectLoader::Load(xml_node object_node, BoxedObjectId bo_id)
{
	BoxedObject bo(_registry_templates);
	entt::entity entity_template = _registry_templates.create();

	for (auto node : object_node)
	{
		AssertMsg(loaders.find(node.name()) != loaders.end(),
			(string)"Error no loader for component:" + node.name());

		auto [component_type, loader_func] = loaders[node.name()];

		BoxedC boxed_c = loader_func(node);

		switch (component_type)
		{
		case ComponentType::Build:
		{
			bo._components_build.push_back(boxed_c);
			boxed_c.CopyTo(entity_template, _registry_templates);
			break;
		}
		case ComponentType::Ready:
		{
			bo._components_ready.push_back(boxed_c);
			boxed_c.CopyTo(entity_template, _registry_templates);
			break;
		}
		case ComponentType::Desc:
		{
			boxed_c.CopyTo(entity_template, _registry_templates);
			break;
		}
		default:
			AssertMsgFail("Unreachable code");
			break;
		}
	}

	bo._components_build.emplace_back(BoxedC(TypeC{ bo_id }));
	bo._id = bo_id;
	bo._entity_template = entity_template;

	if (_registry_templates.all_of<RenderC>(entity_template))
	{
		auto tex_id = _registry_templates.get<RenderC>(entity_template).id;
		string tex_build_name = assets.GetName(tex_id) + "_build";
		if (assets.HaveTexture(tex_build_name))
			bo._build_texture = assets.GetTextureId(tex_build_name);
		else
			bo._build_texture = tex_id;

		bo._components_build.emplace_back(RenderC{ tex_id });
	}

	return { bo, entity_template };
}

