#pragma once
#include "pch.h"
#include "BoxedC.h"
#include "common/IdTypes.h"
#include "helpers/Object.h"

struct RenderC;

class BoxedObject
{
public:
	explicit BoxedObject(entt::registry& registry_templates) :_registry_templates(registry_templates) {};

	entt::entity CreateBuildOnly() const;
	entt::entity CreateFull() const;
	void PlaceToPartReady(entt::entity entity) const;
	void UpgradeTo(const BoxedObject& up_bo, entt::entity entity) const;

	template<typename C>
	const C& Get() const
	{
		return _registry_templates.get<C>(_entity_template);
	}

	template<typename C>
	bool Has() const
	{
		return _registry_templates.all_of<C>(_entity_template);
	}

	BoxedObjectId Id() const;
	float Size() const;
	float Cost() const;
	f32 CostSell(Object obj) const;
	bool IsStruct() const;
	TextureId BuildTexture() const;
	string InfoTextBuild() const;
	string InfoTextObject() const;
private:
	string InfoText(bool is_build_include) const;
	void PlaceToPartBuild(entt::entity entity) const;
	void PlaceToFull(entt::entity entity, entt::registry& registry) const;
	friend class BoxedObjectLoader;
	friend class Assets;
	vector<BoxedC> _components_build;
	vector<BoxedC> _components_ready;
	entt::entity _entity_template = entt::null;
	entt::registry& _registry_templates;
	TextureId _build_texture;
	BoxedObjectId _id;
};

class BoxedObjectLoader
{
public:
	explicit BoxedObjectLoader(entt::registry& registry_templates) :_registry_templates(registry_templates) {}

	//BoxedO_build, BoxedO_ready entity_template
	tuple<BoxedObject, entt::entity> Load(xml_node object_node, BoxedObjectId bo_id);

private:
	entt::registry& _registry_templates;
};