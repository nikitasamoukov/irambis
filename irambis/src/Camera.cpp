#include "Camera.h"
#include "Inputs.h"
#include "Settings.h"
#include "helpers/HelpersMouse.h"

void Camera::Reset()
{
	required_pos_want_zoom = 200;
	required_pos = {};
	pos = {};
	required_pos.zoom = GetNearPerfectZoom(required_pos.zoom);
	pos.zoom = GetNearPerfectZoom(pos.zoom);
}

void Camera::Update()
{
	pos.pos = glm::mix(pos.pos, required_pos.pos, per_frame_move);
	pos.zoom = glm::mix(pos.zoom, required_pos.zoom, per_frame_move);
}

mat3 Camera::GetMatrix()
{
	mat3 mat(1);

	if (pos.zoom > 0.0001)
	{
		mat = glm::scale(mat, { 1 / pos.zoom, 1 / pos.zoom });
	}
	mat = glm::translate(mat, -pos.pos);

	return mat;
}

bool InPerfectZoomRange(float zoom)
{
	if (inputs.screen.size.y == 0) return false;
	if (zoom < 0.001) return false;
	float px_size = inputs.screen.size.y / 2 / zoom;

	return px_size >= 0.99 && px_size <= 10.01;
}

float GetNearPerfectZoom(float zoom, int inc)
{
	if (!InPerfectZoomRange(zoom)) return zoom;
	if (inputs.screen.size.y == 0) return zoom;
	if (zoom < 0.001) return zoom;
	float px_size = inputs.screen.size.y / 2 / zoom;

	return inputs.screen.size.y / 2 / floor(px_size + 0.5f + inc);
}

void Camera::Scroll(float scroll, bool is_mouse)
{
	float zoom_1 = camera.required_pos.zoom;
	float zoom_2_want = camera.required_pos_want_zoom * (float)pow(1.1, scroll);
	float zoom_2 = GetNearPerfectZoom(zoom_2_want);

	if (!settings.is_perfect_zoom)
	{
		zoom_2 = zoom_2_want;
	}

	//vec2 p1 = ConvertMousePosToGameReq(inputs.mouse.pos);

	vec2 v1 = ConvertMousePosToGameReq(inputs.mouse.pos) - ConvertMousePosToGameReq(inputs.screen.GetCenter());
	if (!is_mouse)
	{
		v1 = vec2(0);
	}

	camera.required_pos.pos += v1 * (zoom_1 - zoom_2) / zoom_1;
	camera.required_pos.zoom = zoom_2;
	camera.required_pos_want_zoom = zoom_2_want;

	//vec2 p2 = ConvertMousePosToGameReq(inputs.mouse.pos);
}

Camera camera;
