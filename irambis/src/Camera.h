#pragma once
#include "pch.h"

struct Camera
{
	struct Pos
	{
		vec2 pos{ 0, 0 };
		float zoom = 200;
	};
	
	float required_pos_want_zoom=200;
	Pos required_pos;
	Pos pos;
	
	void Reset();
	void Update();
	mat3 GetMatrix();
	void Scroll(float scroll, bool is_mouse = true);

	constexpr static float per_frame_move = 0.2;
};

float GetNearPerfectZoom(float zoom, int inc = 0);

extern Camera camera;
