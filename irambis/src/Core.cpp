#include "Core.h"
#include "Render.h"
#include "Inputs.h"
#include "Assets.h"
#include "Camera.h"
#include "ImGui/imgui_impl_opengl3.h"
#include "ImGui/imgui_impl_glfw.h"
#include "Levels.h"
#include "helpers/Object.h"
#include "gui/GuiHandlers.h"
#include "gui/SelectedObjectGui.h"
#include "Player.h"
#include "systems/SystemPhys.h"
#include "systems/SystemEnemyTargetFollow.h"
#include "gui/BuildGui.h"
#include "systems/SystemEnergy.h"
#include "gui/DebugGui.h"
#include "systems/SystemTurrets.h"
#include "systems/SystemKill.h"
#include "systems/SystemEnemyAttack.h"
#include "helpers/EventHandlers.h"
#include "systems/SystemBullets.h"
#include "systems/SystemGlobalTargets.h"
#include "gui/PlayerGui.h"
#include "gui/TimeWarpGui.h"
#include "Settings.h"
#include "registry.h"
#include "GlobalTargets.h"
#include "helpers/Query.h"
#include "gui/MainMenuGui.h"
#include "gui/PauseMenuGui.h"
#include "gui/WavesGui.h"
#include "gui/LevelEndGui.h"
#include "systems/SystemLevelEnd.h"
#include "systems/SystemParticles.h"

Core core;

template<typename T>
void SleepSome(T duration)
{
	auto start_time = chrono::high_resolution_clock::now();
	while (chrono::high_resolution_clock::now() - start_time < duration - chrono::milliseconds(1))
	{
		EASY_BLOCK("Sleep", profiler::colors::DarkBlue);
		std::this_thread::sleep_for(chrono::milliseconds(1));
	}
}

void Core::Run()
{
	Init();

	auto first_time_update_point = chrono::high_resolution_clock::now();

	int frames_count = 0;
	int skip_frames_count = 0;

	int upf = 1;

	while (!glfwWindowShouldClose(_window))
	{
		EASY_BLOCK("Frame", profiler::colors::Dark);
		auto start_time = chrono::high_resolution_clock::now();

		auto time_point_to_start_upd = first_time_update_point + (frames_count + skip_frames_count) * frame_time;

		auto time_for_sleep_before_start_upd = time_point_to_start_upd - start_time;

		auto sleep_time_start = chrono::high_resolution_clock::now();
		if (time_for_sleep_before_start_upd > chrono::microseconds(0))
		{
			if (settings.time_speed != Settings::TimeSpeed::Max)
			{
				SleepSome(time_for_sleep_before_start_upd);
			}
			else
			{
				SleepUpdate(time_for_sleep_before_start_upd); // HACK replace sleep by update
			}
		}
		auto sleep_time = chrono::high_resolution_clock::now() - sleep_time_start;

		if (time_for_sleep_before_start_upd < -chrono::microseconds(5000)) // so slow upd -> down to 30 fps
			upf = max(2, upf);
		if (upf == 2 && time_for_sleep_before_start_upd < -chrono::microseconds(10000)) // epic slow upd -> down to 20 fps
			upf = 3;

		if (info.avg_render_time + info.update_frame_time * 2 < frame_time * 2 - chrono::microseconds(2000))
			upf = 2;
		if (info.avg_render_time + info.update_frame_time < frame_time - chrono::microseconds(1000))
			upf = 1;

		if (time_for_sleep_before_start_upd < -chrono::microseconds(50000)) // epical slow upd
		{
			auto to_skip = chrono::nanoseconds(-time_for_sleep_before_start_upd - chrono::microseconds(50000)).count() /
				chrono::nanoseconds(frame_time).count();

			skip_frames_count += to_skip;
		}

		auto start_update_time = chrono::high_resolution_clock::now();
		for (u64 i = 0; i < upf; i++)
		{
			ProcessEvents();
			Update();
			frames_count++;
		}

		auto updates_time = duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now() - start_update_time);
		auto update_frame_time = chrono::nanoseconds(updates_time.count() / upf);

		auto start_render_time = chrono::high_resolution_clock::now();

		Render();

		auto render_time = duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now() - start_render_time);

		info.updates_time = updates_time;
		info.update_frame_time = update_frame_time;
		info.render_time = render_time;
		info.sleep_time = sleep_time;
		info.UpdateAvg(update_frame_time, render_time);
	}

	settings.Save();
	assets.Free();
	render.Terminate();
}

void Core::CleanGame()
{
	_game_frames = 0;
	player.money = 0;
	player.selected_obj = {};
	player.selected_building_to_place = {};
	settings.ResetToEnterLevel();
	registry.clear();
	levels.Clear();
	gt.targets.clear();
	ClearGrids();
	dispatcher.clear();
	camera.Reset();
	waves_gui.Clear();
	level_end_gui.Reset();
}

Frames Core::GetGameFrame()
{
	return _game_frames;
}

void Core::QuitGame()
{
	glfwSetWindowShouldClose(_window, true);
}

void Core::ProcessEvents()
{
	inputs.keys.Clear();
	glfwPollEvents();
}

void Core::Update()
{
	EASY_FUNCTION(profiler::colors::Dark);
	if (holds_alternative<Player::MenuGameState>(player.menu))
	{
		static auto time_prev = chrono::high_resolution_clock::now(); // HACK time_step localy get
		auto time_now = chrono::high_resolution_clock::now();
		f32 time_scale = clamp(chrono::duration_cast<chrono::nanoseconds>(time_now - time_prev).count() / 1'000'000'000.0f * 4.0f, 0.0f, 0.3f);
		time_prev = time_now;

		if (inputs.keys[GLFW_KEY_W].hold) camera.required_pos.pos.y += camera.required_pos.zoom * time_scale;
		if (inputs.keys[GLFW_KEY_S].hold) camera.required_pos.pos.y -= camera.required_pos.zoom * time_scale;
		if (inputs.keys[GLFW_KEY_A].hold) camera.required_pos.pos.x -= camera.required_pos.zoom * time_scale;
		if (inputs.keys[GLFW_KEY_D].hold) camera.required_pos.pos.x += camera.required_pos.zoom * time_scale;

		if (inputs.keys[GLFW_KEY_C].hold)
		{
			vec2 pos_old = camera.pos.pos;

			camera.Reset();
			vec2 pos;
			if (!gt.targets.empty())
				pos = gt.targets[0];
			for (auto& trg_pos : gt.targets)
				if (distance(pos, pos_old) > distance(trg_pos, pos_old))
					pos = trg_pos;
			camera.pos.pos = pos;
			camera.required_pos.pos = pos;
		}
		if (inputs.keys[GLFW_KEY_EQUAL].hold || inputs.keys[GLFW_KEY_KP_ADD].hold) camera.Scroll(-1, false);
		if (inputs.keys[GLFW_KEY_MINUS].hold || inputs.keys[GLFW_KEY_KP_SUBTRACT].hold) camera.Scroll(1, false);

	}

	if (inputs.keys[GLFW_KEY_ESCAPE].hit)
	{
		if (holds_alternative<Player::MenuPauseState>(player.menu))
		{
			player.MenuSwitchToGame();
		}
		else if (holds_alternative<Player::MenuGameState>(player.menu))
		{
			player.MenuSwitchToPause();
		}
	}

	camera.Update();

	_is_skip_by_slow = !_is_skip_by_slow;
	if (settings.time_speed != Settings::TimeSpeed::Pause &&
		(_is_skip_by_slow || settings.time_speed != Settings::TimeSpeed::Slow))
	{
		u64 loops = settings.time_speed == Settings::TimeSpeed::Fast ? 2 : 1;
		for (u64 i = 0; i < loops; i++)
		{
			UpdateWorld();
		}
	}
}

void Core::UpdateWorld()
{
	waves_gui.Update();

	CleanRender();

	dispatcher.update();

	levels.Update();

	SystemEnergy();
	SystemGlobalTargets();

	SystemEnemyTargetFollow();

	SystemTurrets();
	SystemEnemyAttack();

	SystemBullets();

	SystemParticles();
	
	SystemKill();

	SystemPhys();

	SystemLevelEnd();

	_game_frames++;
}

void ImGuiPreRender()
{
	EASY_FUNCTION(profiler::colors::Dark);
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void ImGuiPostRender()
{
	EASY_FUNCTION(profiler::colors::Dark);
	// Rendering
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void ImGuiRender()
{
	EASY_FUNCTION(profiler::colors::Dark);
	
	if constexpr (build_type != BuildType::Retail) if (settings.is_show_debug_gui) 
		ImGui::ShowDemoWindow();

	if (holds_alternative<Player::MenuMainState>(player.menu))
	{
		MainMenuGui();
	}
	else if (holds_alternative<Player::MenuPauseState>(player.menu))
	{
		PauseMenuGui();
	}
	else if (holds_alternative<Player::MenuGameState>(player.menu))
	{
		if (!player.selected_obj.IsNull())
			SelectedObjectGui(player.selected_obj);

		PlayerGui();
		BuildGui();
		TimeWarpGui();
		waves_gui.Draw();
		DebugGui();
		level_end_gui.Draw();
	}
}

void Core::Render()
{
	EASY_FUNCTION(profiler::colors::Dark);

	render.SetFullscreen(_window, settings.is_fullscreen);

	if (holds_alternative<Player::MenuMainState>(player.menu)) // HACK complete mess in render
		render.CleanLists();

	render.Draw();

	{
		EASY_BLOCK("ImGui", profiler::colors::Dark);
		ImGuiPreRender();
		ImGuiRender();
		ImGuiPostRender();
	}

	render.SwapBuffers(_window);
}

void Core::CleanRender()
{
	render.CleanLists();
}

void Core::Init()
{
	settings.Load();

	_window = render.Init(settings.is_fullscreen);

	RegisterInputCallbacks(_window);
	RegisterGuiHandlers();
	RegisterHandlers();

	assets.Load();

	InitImGui();
}

void Core::InitImGui()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	static ImGuiIO& io = ImGui::GetIO();
	(void)io;

	ImGui_ImplGlfw_InitForOpenGL(_window, true);
	ImGui_ImplOpenGL3_Init();
	ImGui::StyleColorsDark();

	ImGui::GetStyle().WindowRounding = 0;
}
