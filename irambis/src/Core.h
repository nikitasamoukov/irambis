#pragma once
#include "pch.h"
#include "common/util.h"

class Core
{
public:
	void Run();
	struct Info
	{
		chrono::nanoseconds updates_time{ 0 };
		chrono::nanoseconds update_frame_time{ 0 };
		chrono::nanoseconds render_time{ 0 };
		chrono::nanoseconds sleep_time{ 0 };

		chrono::nanoseconds avg_update_time{ 0 };
		chrono::nanoseconds avg_render_time{ 0 };

		void UpdateAvg(chrono::nanoseconds update_time, chrono::nanoseconds render_time)
		{
			UpdateAvgT(avg_update_time, update_time);
			UpdateAvgT(avg_render_time, render_time);
		}
	};
	Info info;
	
	void CleanGame();
	Frames GetGameFrame();
	void QuitGame();
	
	void UpdateWorld();
private:
	Frames _game_frames = 0;
	
	void ProcessEvents();
	void Update();
	void Render();
	void CleanRender();
	void Init();
	void InitImGui();
	template<typename T>
	void SleepUpdate(T duration)
	{
		auto start_time = chrono::high_resolution_clock::now();
		u64 count_loop = 0;
		while (chrono::high_resolution_clock::now() - start_time < duration - chrono::milliseconds(1))
		{
			if (count_loop < 19)
			{
				EASY_BLOCK("Upd", profiler::colors::Dark);
				UpdateWorld();
			}
			else
			{
				EASY_BLOCK("Sleep", profiler::colors::DarkBlue);
				std::this_thread::sleep_for(chrono::milliseconds(1));
			}
			count_loop++;
		}
	}

	bool _is_skip_by_slow = false;

	GLFWwindow* _window = nullptr;
};

extern Core core;