#pragma once
#include "pch.h"
#include "common/IdTypes.h"
#include "components.h"

struct EClick
{
	vec2 pos;
	int mods = 0;
};

struct EShoot
{
	f32 range = 0;
	TrsC trs;
	BoxedObjectId id;
};

struct ESpawnEnemy
{
	BoxedObjectId id;
	TrsC trs;
	f32 speed = 0; //units/frame
};

struct ETryUpgradeStructure
{
	Object obj;
	string upgrade_name;
};

struct ETryBuildStructure
{
	vec2 pos;
	BoxedObjectId id;
};