#include "Inputs.h"
#include "Camera.h"
#include "dispatcher.h"
#include "Events.h"
#include "helpers/HelpersMouse.h"
#include "Settings.h"
#include "Player.h"

Inputs inputs;

void MousePosCallback(GLFWwindow* window, double x_pos, double y_pos)
{
	int diff_x = (int)x_pos - inputs.mouse.pos.x;
	int diff_y = (int)y_pos - inputs.mouse.pos.y;
	inputs.mouse.pos.x = (int)x_pos;
	inputs.mouse.pos.y = (int)y_pos;
	inputs.mouse.vel.x = diff_x;
	inputs.mouse.vel.y = diff_y;
}

void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	if (IsMouseOnGui())
		return;
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		dispatcher.trigger(EClick{ inputs.mouse.pos, mods });
	}
}

void KeyCallback(GLFWwindow* window, int key, int scan_code, int action, int mode)
{
	if (IsKeyboardOnGui())
		return;
	if (key >= 0 && key < 512)
	{
		switch (action)
		{
		case GLFW_PRESS:
			inputs.keys[key].hit = 1;
			inputs.keys[key].hold = 1;
			break;
		case GLFW_RELEASE:
			inputs.keys[key].up = 1;
			inputs.keys[key].hold = 0;
			break;
		default:
			return;
		}
	}
}

void ScrollCallback(GLFWwindow* window, double scroll_x, double scroll_y)
{
	if (IsMouseOnGui())
		return;
	if (!holds_alternative<Player::MenuGameState>(player.menu)) return;

	camera.Scroll(-scroll_y);
}

void FramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
	inputs.screen.size = { width, height };

	if (settings.is_perfect_zoom)
	{
		camera.required_pos.zoom = GetNearPerfectZoom(camera.required_pos_want_zoom);
	}
}

void RegisterInputCallbacks(GLFWwindow* window)
{
	glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);
	glfwSetCursorPosCallback(window, MousePosCallback);
	glfwSetKeyCallback(window, KeyCallback);
	glfwSetScrollCallback(window, ScrollCallback);
	glfwSetMouseButtonCallback(window, MouseButtonCallback);
}
