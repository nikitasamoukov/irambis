#pragma once
#include "pch.h"

class Mouse
{
public:
	vec2 pos;
	vec2 vel;
};

class Keyboard
{
public:
	std::array<int, 512> hold{};
	std::array<int, 512> hit{};
	std::array<int, 512> up{};

	void Clear()
	{
		for (int i = 0; i < 512; i++)
		{
			hit[i] = 0;
		}
		for (int i = 0; i < 512; i++)
		{
			up[i] = 0;
		}
	}

	struct CombinedKeyInfo
	{
		int& hold;
		int& hit;
		int& up;
		CombinedKeyInfo(int& hold, int& hit, int& up) :hold(hold), hit(hit), up(up) {}
	};

	CombinedKeyInfo operator [](int i)
	{
		if (i >= 0 && i < 512)
		{
			return { hold[i], hit[i], up[i] };
		}
		throw;
	}
};

struct Screen
{
	vec2i size = { 640, 480 };
	vec2 GetCenter() const { return vec2(size) / 2.0f; }
};

struct Inputs
{
	Mouse mouse;
	Keyboard keys;
	Screen screen;
};

extern Inputs inputs;
void RegisterInputCallbacks(GLFWwindow* window);
