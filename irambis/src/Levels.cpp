#include "Levels.h"
#include "registry.h"
#include "components.h"
#include "helpers/Object.h"
#include "Assets.h"
#include "helpers/HelpersStruct.h"
#include "Player.h"
#include "Core.h"
#include "gui/WavesGui.h"

Levels levels;

void EnemyWave::Load(xml_node node, chrono::milliseconds time_wave)
{
	dist = node.attribute("dist").as_float();
	dist_rand = node.attribute("dist_rand").as_float();
	angle = node.attribute("angle").as_float();
	angle_size = node.attribute("angle_size").as_float();

	low_rand = node.attribute("low_rand").as_bool();

	id = assets.GetBoxedObjectId(node.attribute("enemy").as_string());
	count = node.attribute("count").as_ullong();
	time = time_wave.count() / 1000.0f * frames_per_second; // s*(f/s)=f
}

chrono::milliseconds EnemyWaveGen::Load(xml_node node, chrono::milliseconds time_wave)
{
	chrono::milliseconds full_wawes_time{ 0 };
	generated_waves.clear();

	angle_step = node.attribute("angle_step").as_float();
	time_step = chrono::seconds(node.attribute("time_step").as_int());
	count = node.attribute("count").as_ullong();
	scale_up = node.attribute("scale_up").as_float();

	vector<vector<EnemyWave>> waves_steps;

	{
		vector<EnemyWave> waves_step;

		for (auto node_wave : node.children())
		{
			if (strcmp(node_wave.name(), "wave") == 0)
			{
				EnemyWave wave;
				wave.Load(node_wave, time_wave);
				waves_step.push_back(wave);
			}
			else if (strcmp(node_wave.name(), "timer") == 0)
			{
				if (!waves_step.empty())
					waves_steps.push_back(waves_step);
				waves_step.clear();
			}
		}
		if (!waves_step.empty())
			waves_steps.push_back(waves_step);
	}

	u64 idx_step = 0;
	u64 generation = 0;
	for (u64 i = 0; i < count; i++)
	{
		f32 count_scale = pow(scale_up, (float)generation);

		auto& waves_step = waves_steps[idx_step];
		for (auto& wave : waves_step)
		{
			auto wave_scaled = wave;
			wave_scaled.count *= count_scale;
			wave_scaled.angle += angle_step * i;

			chrono::milliseconds spawn_time = (i + 1) * time_step + time_wave;
			wave_scaled.time = spawn_time.count() / 1000.0f * frames_per_second;

			generated_waves.push_back(wave_scaled);
		}

		idx_step++;
		if (idx_step >= waves_steps.size())
		{
			idx_step = 0;
			generation++;
		}

		full_wawes_time += time_step;
	}

	return full_wawes_time;
}

void Levels::Set(u32 level)
{


	//CreateObjectFull(assets.GetBoxedObjectId("Solar mk1")).SetTrs({ vec2(0, 0), 0 });
	//CreateObjectFull(assets.GetBoxedObjectId("Connect mk1")).SetTrs({ vec2(100, 0), 0 });

	//SpawnEnemy(assets.GetBoxedObjectId("Shooter mk1"), { 0, 1000 }, 300, 50);
	//SpawnEnemy(assets.GetBoxedObjectId("Shooter mk1"), { 0, -1000 }, 300, 50);
	//SpawnEnemy(assets.GetBoxedObjectId("Shooter mk1"), { 1000, 0 }, 300, 50);
	//SpawnEnemy(assets.GetBoxedObjectId("Shooter mk1"), { -1000, 0 }, 300, 50);

	//SpawnEnemy(assets.GetBoxedObjectId("Shooter mk1"), { {-250,250},2.25147462 });

	LoadSetup(to_string(level) + ".xml");

	if (is_sandbox)
		player.money += 1000000;
}

void SpawnWave(EnemyWave const& wave, u64 seed)
{
	mt19937 rg(seed);

	for (u64 i = 0; i < wave.count; i++)
	{
		f32 dist = wave.dist + (rg() % 1001) / 1000.0f * wave.dist_rand;
		f32 angle = glm::radians(wave.angle - wave.angle_size / 2 + (rg() % 1001) / 1000.0f * wave.angle_size);

		if (wave.low_rand)
			angle = glm::radians(wave.angle - wave.angle_size / 2 + wave.angle_size * (i + (rg() % 1001) / 1000.0f) / wave.count);

		TrsC trs_rand{
			.pos = vec2(dist * cos(angle),dist * sin(angle)),
			.angle = glm::radians(f32(rg() % 360))
		};
		SpawnEnemy(wave.id, trs_rand);
	}
	waves_gui.AddWave(wave.id, wave.count);
}

void Levels::Update()
{
	if (_level.waves.empty()) return;
	if (_level.waves.back().time > core.GetGameFrame()) return;

	SpawnWave(_level.waves.back(), _level.waves.size());

	_level.waves.pop_back();
}

bool Levels::IsNoWaves()
{
	return _level.waves.empty();
}

void Levels::LoadSetup(string const& level_file)
{
	xml_document doc;
	string xml_file_name = ("levels/" + level_file);
	auto result = doc.load_file(xml_file_name.c_str());
	AssertMsg(result.status != pugi::status_file_not_found, (string)"File not found:" + xml_file_name);
	AssertMsg(result.status == pugi::status_ok, (string)"File not loaded:" + xml_file_name);

	vector<Object> objects;
	for (auto node : doc.children("object"))
	{
		string struct_name = node.attribute("name").as_string();
		f32 struct_x = node.attribute("x").as_float();
		f32 struct_y = node.attribute("y").as_float();

		auto obj = PlaceStructFull({ struct_x, struct_y }, assets.GetBoxedObjectId(struct_name), false);
		if (!obj.IsNull())
		{
			objects.push_back(obj);
		}
	}
	for (auto obj : objects)
	{
		ConnectStruct(obj);
		registry.emplace_or_replace<StructZeroRewardSell>(obj.entity);
	}

	player.money = 0;
	for (auto node : doc.children("player"))
	{
		player.money = node.attribute("money").as_float();
	}

	for (auto node_waves : doc.children("waves"))
	{
		chrono::milliseconds time_wave{ 0 };
		for (auto node : node_waves.children())
		{
			if (strcmp(node.name(), "wave") == 0)
			{
				EnemyWave wave;
				wave.Load(node, time_wave);
				_level.waves.push_back(wave);
			}
			else if (strcmp(node.name(), "timer") == 0)
			{
				time_wave += chrono::seconds(u64(node.attribute("time").as_float()));
			}
			else if (strcmp(node.name(), "wavegen") == 0)
			{
				EnemyWaveGen wave_gen;
				auto wawes_time = wave_gen.Load(node, time_wave);
				_level.waves.insert(_level.waves.end(), wave_gen.generated_waves.begin(), wave_gen.generated_waves.end());

				time_wave += wawes_time;
			}
		}
	}
	sort(_level.waves.begin(), _level.waves.end(), [](EnemyWave const& a, EnemyWave const& b) {return a.time > b.time; });
}

void SaveSetup()
{
	xml_document doc;
	for (auto entity : registry.view<TrsC, StructC, TypeC>())
	{
		auto&& [trs_c, type_c] = registry.get<TrsC, TypeC>(entity);

		auto node = doc.append_child("object");

		string struct_name = assets.GetName(type_c.id);

		node.append_attribute("x").set_value(trs_c.pos.x);
		node.append_attribute("y").set_value(trs_c.pos.y);
		node.append_attribute("name").set_value(struct_name.c_str());
	}

	bool res = doc.save_file("saved_level_setup.xml", "\t", pugi::format_no_declaration);

	if (!res)
		cout << "Cant save level setup" << endl;
}