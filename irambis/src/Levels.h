#pragma once
#include "pch.h"
#include "common/IdTypes.h"
#include "common/util.h"

struct EnemyWave
{
	f32 dist = 0;
	f32 dist_rand = 0;
	std::chrono::seconds spawn_time{ 0 };
	f32 angle = 0;
	f32 angle_size = 0;

	bool low_rand = false;

	BoxedObjectId id;
	Frames time = 0;
	u64 count = 0;

	void Load(xml_node node, chrono::milliseconds time_wave);
};

struct EnemyWaveGen
{
	f32 angle_step = 0;
	std::chrono::seconds time_step{ 0 };
	u64 count = 1;
	f32 scale_up = 1;

	vector<EnemyWave> generated_waves;

	chrono::milliseconds Load(xml_node node, chrono::milliseconds time_wave);
};

struct Level
{
	vector<EnemyWave> waves;
	void Clear() { waves.clear(); }
};

class Levels
{
public:
	void Set(u32 level);
	void Update();
	void Clear() { _level.Clear(); }
	bool IsNoWaves();

	void LoadSetup(string const& level_file);
private:
	Level _level;
};

extern Levels levels;

void SaveSetup();