#include "Player.h"
#include "Core.h"

Player player;

void Player::MenuSwitchToPause()
{
	player.menu = MenuPauseState{ settings.time_speed };
	settings.time_speed = Settings::TimeSpeed::Pause;
}

void Player::MenuSwitchToGame()
{
	auto prev_time_speed = get<MenuPauseState>(player.menu).prev_time_speed;
	settings.time_speed = prev_time_speed;
	player.menu = MenuGameState();
}

void Player::MenuSwitchToMain()
{
	player.menu = MenuMainState();
	core.CleanGame();
}
