#pragma once
#include "pch.h"
#include "helpers/Object.h"
#include "Settings.h"

struct Player
{
	struct MenuMainState {};
	struct MenuPauseState
	{
		Settings::TimeSpeed prev_time_speed = Settings::TimeSpeed::Normal;
	};
	struct MenuGameState {};

	float money;
	Object selected_obj;
	optional<BoxedObjectId> selected_building_to_place;

	variant<MenuMainState, MenuPauseState, MenuGameState> menu = MenuMainState();

	void MenuSwitchToPause();
	void MenuSwitchToGame();
	void MenuSwitchToMain();
	
};

extern Player player;
