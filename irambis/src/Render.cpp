#include "pch.h"
#include "Render.h"
#include "../shaders/ShaderTexture.h"
#include "Inputs.h"
#include "Assets.h"
#include "Camera.h"
#include "Player.h"
#include "helpers/HelpersMouse.h"
#include "helpers/Query.h"
#include "registry.h"
#include "systems/SystemRender.h"

Render render;


void APIENTRY GlDebugOutput(GLenum source,
	GLenum type,
	unsigned int id,
	GLenum severity,
	GLsizei length,
	const char* message,
	const void* user_param);

struct VertexSprite
{
	vec2 pos;
	vec2 tex_coord;
};

struct VertexLaser
{
	vec2 pos;
	vec2 tex_coord;
	float dist_scale;
};

class MeshVao // HACK dangerous to leak texture resource handle.
{
public:
	template <typename Vertex>
	void Init(const vector<Vertex>& vertices, const vector<GLuint>& indices)
	{
		glGenVertexArrays(1, &_vao);
		glGenBuffers(1, &_vbo);
		glGenBuffers(1, &_ebo);

		glBindVertexArray(_vao);

		_indices_count = indices.size();

		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

		InitAttributes<Vertex>();
		// position attribute
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		glEnableVertexAttribArray(0);
		// texture coord attribute
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec2)));
		glEnableVertexAttribArray(1);

		glBindVertexArray(0);
	}

	template <typename Vertex>
	void InitAttributes();

	i32 IndicesCount()
	{
		return _indices_count;
	}

	void Bind()
	{
		glBindVertexArray(_vao);
	}

	void UnBind()
	{
		glBindVertexArray(0);
	}

	void Destroy()
	{
		glDeleteVertexArrays(1, &_vao);
		glDeleteBuffers(1, &_vbo);
		glDeleteBuffers(1, &_ebo);
	}

private:
	size_t _indices_count = 0;
	GLuint _vbo = -1;
	GLuint _vao = -1;
	GLuint _ebo = -1;
};

template <>
void MeshVao::InitAttributes<VertexSprite>()
{
	// position attribute
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(VertexSprite), (void*)0);
	glEnableVertexAttribArray(0);
	// texture coord attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VertexSprite), (void*)(sizeof(vec2)));
	glEnableVertexAttribArray(1);
}

template <>
void MeshVao::InitAttributes<VertexLaser>()
{
	// position attribute
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(VertexLaser), (void*)0);
	glEnableVertexAttribArray(0);
	// texture coord attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VertexLaser), (void*)(sizeof(vec2)));
	glEnableVertexAttribArray(1);
	// distance attribute
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(VertexLaser), (void*)(sizeof(vec2) + sizeof(vec2)));
	glEnableVertexAttribArray(2);
}

MeshVao vao_sprite;

MeshVao vao_laser;

Render::Render()
{
	Assert(!_is_exist);
	_is_exist = true;
}

GLFWwindow* Render::Init(bool is_fullscreen)
{
	GLFWwindow* window;
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();

	if constexpr (build_type == BuildType::Retail)
	{
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	}
	else
	{
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	}

	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_RED_BITS, 16);
	glfwWindowHint(GLFW_GREEN_BITS, 16);
	glfwWindowHint(GLFW_BLUE_BITS, 16);

	if (is_fullscreen)
	{
		const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		glfwWindowHint(GLFW_RED_BITS, mode->redBits);
		glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
		glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);


		if constexpr (danger_fullscreen)
		{
			window = glfwCreateWindow(mode->width, mode->height, window_title, glfwGetPrimaryMonitor(), NULL);
		}
		else
		{
			glfwWindowHint(GLFW_DECORATED, true);
			glfwWindowHint(GLFW_FLOATING, false);

			window = glfwCreateWindow(mode->width, mode->height, window_title, NULL, NULL);
			int xpos;
			int ypos;
			glfwGetMonitorPos(glfwGetPrimaryMonitor(), &xpos, &ypos);
			glfwSetWindowPos(window, xpos, ypos);
		}
		inputs.screen.size.x = mode->width;
		inputs.screen.size.y = mode->height;
	}
	else
	{
		window = glfwCreateWindow(inputs.screen.size.x, inputs.screen.size.y, "Irambis DEMO", NULL, NULL);
	}

	_is_fullscreen = is_fullscreen;

	if (window == NULL)
	{
		glfwTerminate();
		AssertMsgFail("Failed to create GLFW window");
	}
	glfwMakeContextCurrent(window);

	gladLoadGL();


	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#ifndef RETAIL
	glDebugMessageCallback(GlDebugOutput, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
#endif

	// build and compile our shader zprogram
	// ------------------------------------
	_main_shader.Init();
	_select_shader.Init();
	_colored_shader.Init();
	_laser_shader.Init();
	_particle_shader.Init();


	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------
	vector<VertexSprite> vertices_sprite = {
		{{ 0.5,  0.5}, {1.0f, 0.0f}},
		{{ 0.5, -0.5}, {1.0f, 1.0f}},
		{{-0.5, -0.5}, {0.0f, 1.0f}},
		{{-0.5,  0.5}, {0.0f, 0.0f}},
	};
	vector<GLuint> indices_sprite = {
		0, 1, 3,
		1, 2, 3,
	};

	vector<VertexLaser> vertices_laser = {
		{{ 0.5,  0.5}, {1.0f, 0.0f},1},//0
		{{ 0.5, -0.5}, {1.0f, 1.0f},1},//1
		{{ 0.0, -0.5}, {0.5f, 1.0f},1},//2
		{{ 0.0,  0.5}, {0.5f, 0.0f},1},//3

		{{ 0.0, -0.5}, {0.5f, 1.0f},0},//4
		{{ 0.0,  0.5}, {0.5f, 0.0f},0},//5
		{{-0.5, -0.5}, {0.0f, 1.0f},0},//6
		{{-0.5,  0.5}, {0.0f, 0.0f},0},//7
	};
	vector<GLuint> indices_laser = {
		0, 1, 3,
		1, 2, 3,

		2, 3, 4,
		3, 4, 5,

		4, 5, 6,
		5, 6, 7,
	};


	vao_sprite.Init(vertices_sprite, indices_sprite);
	vao_laser.Init(vertices_laser, indices_laser);

	return window;
}

mat3 ProjMat2d(vec2i screen_size)
{
	mat3 mat(1);

	if (screen_size.y != 0)
	{
		mat[0][0] = screen_size.y / (float)screen_size.x;
	}
	mat[1][1] = 1;
	return mat;
}

class TmpInstanceDataBuffer // HACK someching complete wrong with this...
{
public:
	TmpInstanceDataBuffer() = default;

	template <typename Shader>
	void Init(u64 count, void* data);

	~TmpInstanceDataBuffer()
	{
		glDeleteBuffers(1, &_instance_vbo);
	}
	TmpInstanceDataBuffer(TmpInstanceDataBuffer&&) = delete;
	TmpInstanceDataBuffer& operator =(TmpInstanceDataBuffer&&) = delete;
	TmpInstanceDataBuffer(const TmpInstanceDataBuffer&) = delete;
	TmpInstanceDataBuffer& operator =(const TmpInstanceDataBuffer&) = delete;
private:
	GLuint _instance_vbo = -1;
};

template <>
void TmpInstanceDataBuffer::Init<ShaderTexture>(u64 count, void* data)
{
	glGenBuffers(1, &_instance_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, _instance_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * count, data, GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), (void*)(0 * sizeof(vec3)));
	glVertexAttribDivisor(2, 1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

template <>
void TmpInstanceDataBuffer::Init<ShaderTextureLaser>(u64 count, void* data)
{
	glGenBuffers(1, &_instance_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, _instance_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(InstanceLaserShaderData) * count, data, GL_STATIC_DRAW);

	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(InstanceLaserShaderData), (void*)(0 * sizeof(vec4)));
	glVertexAttribDivisor(3, 1);

	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(InstanceLaserShaderData), (void*)(1 * sizeof(vec4)));
	glVertexAttribDivisor(4, 1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

template <>
void TmpInstanceDataBuffer::Init<ShaderParticle>(u64 count, void* data)
{
	glGenBuffers(1, &_instance_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, _instance_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(InstanceParticleShaderData) * count, data, GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(InstanceParticleShaderData), (void*)(0 * sizeof(vec4)));
	glVertexAttribDivisor(2, 1);

	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(InstanceParticleShaderData), (void*)(1 * sizeof(vec4)));
	glVertexAttribDivisor(3, 1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Render::Draw()
{
	EASY_FUNCTION(profiler::colors::Dark);
	{
		EASY_BLOCK("Setup and clear", profiler::colors::Dark);

		glClearColor(0.03f, 0.03f, 0.03f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		glClear(GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL); // Depth for same z need owerdraw


		glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA); // Some magic
		//std::vector<mat3> models(100, mat3{ 1 });
		//
	}

	// HACK half draws here half spread all game code...

	for (auto& e : _draw_lists)
		e.clear();


	DrawStars();

	DrawConnects();

	//if (!holds_alternative<Player::MenuMainState>(player.menu))
	//	render.AddSpriteToDraw(assets.GetTextureId("debug_grid"), TrsC{ vec2(0), 0 });

	SystemRender();
	DrawLists();

	DrawEnergyBars();

	DrawLaserLists();

	DrawBars();

	DrawParticles();

	DrawSelection();
	DrawBuildHighlight();
	DrawBuildConnectHighlight();
	if (player.selected_building_to_place)
		DrawObjectRanges(*player.selected_building_to_place, ConvertMousePosToStructPos(inputs.mouse.pos));

	if (!player.selected_obj.IsNull())
		DrawObjectRanges(player.selected_obj.GetType(), player.selected_obj.GetTrs().pos, false);

}

void Render::DrawLists()
{
	EASY_FUNCTION(profiler::colors::Dark);

	_main_shader.Use();
	_main_shader.texture_color = 0;

	for (u64 i = 0; i < textures_count; i++)
	{
		std::vector<TrsC>& models = _draw_lists[i];

		if (models.empty())
			continue;

		TextureId tex_id{ i };

		assets.BindTexture(tex_id);

		_main_shader.z_pos = assets.GetTexture(tex_id).GetZ() / 1000.0f;
		_main_shader.pos_scale = assets.GetTexture(tex_id).GetSize().x;
		_main_shader.view = camera.GetMatrix();
		_main_shader.projection = ProjMat2d(inputs.screen.size);

		{
			vao_sprite.Bind();
			TmpInstanceDataBuffer instance_buffer;
			instance_buffer.Init<ShaderTexture>(models.size(), models.data());
			glDrawElementsInstanced(GL_TRIANGLES, vao_sprite.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
		}

	}
}

void AddDrawRect(std::vector<InstanceLaserShaderData>& models, vec2 pos1, vec2 pos2, vec3 color)
{
	vec2 size = abs(pos1 - pos2);

	if (abs(size.x - size.y) < 0.0001)// HACK eps
	{
		if (size.x < 0.0001) return;
		size.x += 0.0001;
	}

	vec2 pos_abs = (pos1 + pos2) / 2.0f;

	if (size.x > size.y)
	{
		f32 diff = (size.x - size.y) / 2;
		models.push_back({ pos_abs - vec2(diff, 0), pos_abs + vec2(diff, 0), color, size.y });
	}
	else
	{
		f32 diff = (size.y - size.x) / 2;
		models.push_back({ pos_abs - vec2(0, diff), pos_abs + vec2(0, diff), color, size.x });
	}
}

void AddDrawLineHor(std::vector<InstanceLaserShaderData>& models, vec2 pos_start, f32 width, vec3 color)
{
	if (width > 1)
	{
		models.push_back({ pos_start + vec2(0.5f, 0), pos_start + vec2(width - 0.5f, 0), color, 1 });
	}
	else
	{
		vec2 pos_avg = pos_start + vec2(width / 2, 0);
		f32 width_small = width;
		f32 height_half = 0.5 - width_small / 2;

		models.push_back({ pos_avg + vec2(0, -height_half), pos_avg + vec2(0, height_half), color, width_small });
	}
}

void Render::DrawEnergyBars()
{
	EASY_FUNCTION(profiler::colors::DarkBlue);

	_laser_shader.Use();
	_laser_shader.texture_color = 0;

	_laser_shader.z_pos = z_for_energy_bars / 1000.0f;
	_laser_shader.pos_scale = assets.GetTexture("laser").GetSize().x;
	_laser_shader.view = camera.GetMatrix();
	_laser_shader.projection = ProjMat2d(inputs.screen.size);

	assets.BindTexture(assets.GetTextureId("laser"));
	std::vector<InstanceLaserShaderData> models;

	for (auto entity : registry.view<RenderEnergyC, TrsC, EnergyC>())
	{
		auto& render_energy_c = registry.get<RenderEnergyC>(entity);
		auto& trs_c = registry.get<TrsC>(entity);
		auto& energy_c = registry.get<EnergyC>(entity);

		f32 percent = energy_c.en / max(energy_c.en_max, 0.0001f);// HACK eps

		vec2 pos1 = trs_c.pos - render_energy_c.size / 2.0f;
		vec2 pos2 = trs_c.pos + vec2(render_energy_c.size.x / 2.0f, render_energy_c.size.y * (percent - 1 / 2.0f));
		AddDrawRect(models, pos1, pos2, color_energy_bar);
	}

	if (!models.empty())
	{
		vao_laser.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTextureLaser>(models.size(), models.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_laser.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
	}
}

void Render::DrawLaserLists()
{
	EASY_FUNCTION(profiler::colors::Dark);

	_laser_shader.Use();
	_laser_shader.texture_color = 0;

	_laser_shader.z_pos = z_for_lasers / 1000.0f;
	_laser_shader.pos_scale = assets.GetTexture("laser").GetSize().x;
	_laser_shader.view = camera.GetMatrix();
	_laser_shader.projection = ProjMat2d(inputs.screen.size);

	assets.BindTexture(assets.GetTextureId("laser"));

	std::vector<InstanceLaserShaderData>& models = _laser_draw_lists;
	if (!models.empty())
	{
		vao_laser.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTextureLaser>(models.size(), models.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_laser.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
	}
}

void Render::DrawConnects()
{
	EASY_FUNCTION(profiler::colors::Dark);

	_laser_shader.Use();
	_laser_shader.texture_color = 0;

	_laser_shader.z_pos = z_for_connect / 1000.0f;
	_laser_shader.pos_scale = assets.GetTexture("laser").GetSize().x;
	_laser_shader.view = camera.GetMatrix();
	_laser_shader.projection = ProjMat2d(inputs.screen.size);

	assets.BindTexture(assets.GetTextureId("laser"));
	std::vector<InstanceLaserShaderData> models_dis;
	std::vector<InstanceLaserShaderData> models_en;

	auto group = registry.group<ConnectPosC, ConnectIdC>(entt::get<TrsC>);

	for (entt::entity entity : group)
	{
		auto& connect_pos_c = group.get<ConnectPosC>(entity);
		auto& connect_id_c = group.get<ConnectIdC>(entity);
		auto& trs_c = group.get<TrsC>(entity);
		bool is_build = registry.all_of<StructBuildingC>(entity);

		for (u64 i = 0; i < connect_pos_c.connects.size(); i++)
		{
			if (connect_id_c.connects[i].entity < entity) continue;
			auto& connect_str = connect_pos_c.connects[i];

			if (!connect_str.IsValid())
				continue;

			bool is_child_build = registry.all_of<StructBuildingC>(connect_id_c.connects[i].entity);

			vec2 pos = trs_c.pos + vec2(connect_str.x, connect_str.y);

			if (is_child_build & is_build)
			{
				models_dis.push_back({ trs_c.pos, pos, vec3(0.0, 0.3, 0.3), 2 });
			}
			else
			{
				models_en.push_back({ trs_c.pos, pos, vec3(0.2, 0.8, 0.8), 2 });
			}
		}
	}
	if (!models_dis.empty())
	{
		vao_laser.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTextureLaser>(models_dis.size(), models_dis.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_laser.IndicesCount(), GL_UNSIGNED_INT, 0, models_dis.size());
	}
	if (!models_en.empty())
	{
		vao_laser.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTextureLaser>(models_en.size(), models_en.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_laser.IndicesCount(), GL_UNSIGNED_INT, 0, models_en.size());
	}
}

void Render::DrawStars()
{
	EASY_FUNCTION(profiler::colors::Dark);

	_main_shader.Use();
	_main_shader.texture_color = 0;

	std::vector<TrsC> models;

	models.push_back({ {0, 0}, 0 });

	TextureId tex_id = assets.GetTextureId("pexels-philippe-donn");

	f32 scale_proj = inputs.screen.size.y / (f32)max(inputs.screen.size.x, 1);
	f32 scale_img = assets.GetTexture(tex_id).GetSize().x / (f32)max(1, assets.GetTexture(tex_id).GetSize().y);

	f32 min_scale_img = max(1.0f, 1 / max(0.0001f, scale_img * scale_proj));

	f32 img_pixels_y = min_scale_img * inputs.screen.size.y;
	f32 pixel_size = img_pixels_y / (f32)max(1, assets.GetTexture(tex_id).GetSize().y);
	f32 pixel_size_ceil = ceil(pixel_size);
	min_scale_img *= pixel_size_ceil / max(0.0001f, pixel_size);


	mat3 img_rescale_mat(1);
	img_rescale_mat[0][0] *= scale_proj;
	img_rescale_mat[0][0] *= scale_img;
	assets.BindTexture(tex_id);

	_main_shader.z_pos = assets.GetTexture(tex_id).GetZ() / 1000.0f;
	_main_shader.pos_scale = 2 * min_scale_img;
	_main_shader.view = mat3(1);
	_main_shader.projection = mat3(1) * img_rescale_mat;

	{
		vao_sprite.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTexture>(models.size(), models.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_sprite.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
	}
}

struct BarDrawInfo
{
	vec2 pos;
	f32 width;
	f32 percent;
	vec3 color;
	vec3 color_back;
};

void AddBarDraw(std::vector<InstanceLaserShaderData>& models, BarDrawInfo info)
{
	AddDrawLineHor(models, info.pos + vec2(info.width * (-1.0f / 2), 0), info.width * info.percent, info.color);
	AddDrawLineHor(models, info.pos + vec2(info.width * (-1.0f / 2 + info.percent), 0), info.width * (1 - info.percent), info.color_back);
}

void Render::DrawBars()
{
	EASY_FUNCTION(profiler::colors::Dark);

	_laser_shader.Use();
	_laser_shader.texture_color = 0;

	_laser_shader.z_pos = z_for_bars / 1000.0f;
	_laser_shader.pos_scale = assets.GetTexture("laser").GetSize().x;
	_laser_shader.view = camera.GetMatrix();
	_laser_shader.projection = ProjMat2d(inputs.screen.size);

	assets.BindTexture(assets.GetTextureId("laser"));
	std::vector<InstanceLaserShaderData> models;

	{
		EASY_BLOCK("Bars add", profiler::colors::Dark);

		auto view = registry.view<PhysC, TrsC>();
		for (entt::entity entity : view)
		{
			auto&& [phys_c, pos_c] = view.get<PhysC, TrsC>(entity);

			vec2 bar_pos = pos_c.pos + vec2(0, phys_c.r + 0.5f);
			f32 bar_width = phys_c.r * 2;

			BarDrawInfo bar_info = {
				.pos = bar_pos,
				.width = bar_width,
				.percent = 0,
				.color = vec3(1, 1, 1),
				.color_back = vec3(0.3, 0.3, 0.3)
			};

			if (registry.all_of<HpC>(entity))
			{
				auto& hp_c = registry.get<HpC>(entity);
				if (hp_c.hp < hp_c.hp_max)
				{
					BarDrawInfo hp_bar_info = bar_info;
					hp_bar_info.percent = hp_c.hp / hp_c.hp_max;
					hp_bar_info.color = color_hp_bar;
					hp_bar_info.pos += vec2(0, 2);
					AddBarDraw(models, hp_bar_info);
				}
			}

			if (registry.all_of<EnergyC>(entity))
			{
				auto& energy_c = registry.get<EnergyC>(entity);
				if (energy_c.en < energy_c.en_max)
				{
					BarDrawInfo hp_bar_info = bar_info;
					hp_bar_info.percent = energy_c.en / energy_c.en_max;
					hp_bar_info.color = color_energy_bar;
					hp_bar_info.pos += vec2(0, 1);
					AddBarDraw(models, hp_bar_info);
				}
			}

			if (registry.all_of<StructBuildingC>(entity))
			{
				auto& struct_building_c = registry.get<StructBuildingC>(entity);
				BarDrawInfo hp_bar_info = bar_info;
				hp_bar_info.percent = clamp(struct_building_c.build_progress, 0.0f, 1.0f);
				hp_bar_info.color = color_build_bar;
				hp_bar_info.pos += vec2(0, 3);
				AddBarDraw(models, hp_bar_info);
			}

			if (registry.all_of<EnemyShootingC>(entity))
			{
				auto& enemy_shooting_c = registry.get<EnemyShootingC>(entity);
				if (enemy_shooting_c.cooldown > 0) // HACK zero compare
				{
					BarDrawInfo hp_bar_info = bar_info;
					hp_bar_info.percent = clamp(1 - enemy_shooting_c.cooldown / max(0.00001f, enemy_shooting_c.shoot_cooldown), 0.0f, 1.0f);
					hp_bar_info.color = color_reload_bar;
					hp_bar_info.pos += vec2(0, 3);
					AddBarDraw(models, hp_bar_info);
				}
			}
		}
	}

	if (!models.empty())
	{
		vao_laser.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTextureLaser>(models.size(), models.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_laser.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
	}
}

void Render::DrawParticles()
{
	EASY_FUNCTION(profiler::colors::Dark);

	_particle_shader.Use();
	_particle_shader.texture_color = 0;

	_particle_shader.pos_scale = assets.GetTexture("particle").GetSize().x / 2.0f;
	_particle_shader.view = camera.GetMatrix();
	_particle_shader.projection = ProjMat2d(inputs.screen.size);

	std::vector<InstanceParticleShaderData> models;

	auto view = registry.view<TrsC, RenderParticleC, ParticleLifetimeC>();
	for (auto& entity : view)
	{
		auto&& [trs_c, render_particle_c, particle_lifetime_c] = view.get<TrsC, RenderParticleC, ParticleLifetimeC>(entity);

		vec4 vec;
		vec.x = trs_c.pos.x;
		vec.y = trs_c.pos.y;
		vec.z = trs_c.angle;
		vec.w = render_particle_c.z / 1000.0f;
		models.push_back({ vec, vec4(render_particle_c.col, min(particle_lifetime_c.life * 2, 1.0f)) });
	}

	assets.BindTexture(assets.GetTextureId("particle"));

	if (!models.empty())
	{
		vao_sprite.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderParticle>(models.size(), models.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_sprite.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
		glDisableVertexAttribArray(3);
	}
}

void Render::DrawSelection()
{
	EASY_FUNCTION(profiler::colors::Dark);
	if (player.selected_obj.IsNull())
		return;

	auto obj_entity = player.selected_obj.entity;

	if (!registry.all_of<RenderC>(obj_entity))
		return;
	if (!registry.all_of<TrsC>(obj_entity))
		return;

	TextureId tex_id = registry.get<RenderC>(obj_entity).id;
	auto obj_trs = registry.get<TrsC>(obj_entity);

	_select_shader.Use();
	_select_shader.texture_color = 0;

	_select_shader.z_pos = z_for_select_highlight / 1000.0f;
	_select_shader.pos_scale = assets.GetTexture(tex_id).GetSize().x;
	_select_shader.view = camera.GetMatrix();
	_select_shader.projection = ProjMat2d(inputs.screen.size);

	assets.BindTexture(tex_id);

	std::vector<TrsC> models;

	models.push_back(obj_trs);

	if (!models.empty())
	{
		vao_sprite.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTexture>(models.size(), models.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_sprite.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
	}
}

void Render::DrawBuildHighlight()
{
	EASY_FUNCTION(profiler::colors::Dark);
	if (!player.selected_building_to_place)
		return;

	auto obj_id = *player.selected_building_to_place;
	auto& bo = assets.GetBoxedObject(obj_id);
	TextureId tex_id = bo.Get<RenderC>().id;
	TrsC obj_trs;
	obj_trs.pos = ConvertMousePosToStructPos(inputs.mouse.pos);

	_colored_shader.Use();
	_colored_shader.texture_color = 0;

	if (CanPlaceObject(obj_trs.pos, obj_id))
	{
		_colored_shader.color_scale = vec4(0.2, 1, 0.2, 0.5f);
		_colored_shader.color_add = vec3(0, 0.3, 0);
	}
	else
	{
		_colored_shader.color_scale = vec4(1, 0.2, 0.2, 0.5f);
		_colored_shader.color_add = vec3(0.3, 0, 0);
	}

	if (player.money < bo.Cost())
	{
		_colored_shader.color_scale = vec4(0.4f, 0.4f, 0.4f, 0.5f);
		if (CanPlaceObject(obj_trs.pos, obj_id))
		{
			_colored_shader.color_add = vec3(0.3f, 0.3f, 0.3f);
		}
		else
		{
			_colored_shader.color_add = vec3(0.5f, 0.1f, 0.1f);
		}
	}

	_colored_shader.z_pos = z_for_build_highlight / 1000.0f;
	_colored_shader.pos_scale = assets.GetTexture(tex_id).GetSize().x;
	_colored_shader.view = camera.GetMatrix();
	_colored_shader.projection = ProjMat2d(inputs.screen.size);

	assets.BindTexture(tex_id);

	std::vector<TrsC> models;
	models.push_back(obj_trs);

	if (!models.empty())
	{
		vao_sprite.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTexture>(models.size(), models.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_sprite.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
	}
}

void Render::DrawBuildConnectHighlight()
{
	EASY_FUNCTION(profiler::colors::Dark);
	if (!player.selected_building_to_place)
		return;

	auto obj_id = *player.selected_building_to_place;
	TrsC obj_trs;
	obj_trs.pos = ConvertMousePosToStructPos(inputs.mouse.pos);

	auto& bo = assets.GetBoxedObject(obj_id);

	_laser_shader.Use();
	_laser_shader.texture_color = 0;

	vec3 connects_color_scale;

	if (CanPlaceObject(obj_trs.pos, obj_id))
	{
		connects_color_scale = vec3(0.2, 1, 0.2);
	}
	else
	{
		connects_color_scale = vec3(1, 0.2, 0.2);
	}

	if (player.money < bo.Cost())
		connects_color_scale = vec3(0.4f, 0.4f, 0.4f);

	_laser_shader.z_pos = z_for_build_connect_highlight / 1000.0f;
	_laser_shader.pos_scale = assets.GetTexture("laser").GetSize().x;
	_laser_shader.view = camera.GetMatrix();
	_laser_shader.projection = ProjMat2d(inputs.screen.size);

	assets.BindTexture(assets.GetTextureId("laser"));

	std::vector<InstanceLaserShaderData> models;

	auto connects = GetPossibleConnectionsPredict(obj_trs.pos, obj_id);

	for (auto& connect_str : connects)
	{
		vec2 pos = registry.get<TrsC>(connect_str.obj.entity).pos;
		models.push_back({ obj_trs.pos, pos, connects_color_scale, 2 });
	}

	if (!models.empty())
	{
		vao_laser.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTextureLaser>(models.size(), models.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_laser.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
	}
}

void Render::DrawObjectRanges(BoxedObjectId id, vec2 pos, bool draw_conn)
{
	EASY_FUNCTION(profiler::colors::Dark);

	auto& bo = assets.GetBoxedObject(id);
	TrsC obj_trs;
	obj_trs.pos = pos;

	if (bo.Has<TurretC>())
	{
		auto& c = bo.Get<TurretC>();
		DrawCircle(obj_trs.pos, c.range, 1, vec4(1, 0.1, 0.1, 1), 0);
	}
	if (draw_conn && bo.Has<ConnectIdC>())
	{
		DrawCircle(obj_trs.pos, connect_dist, 1, vec4(0.1, 0.8, 0.8, 1), 0);
	}

	if (bo.Has<EnemyC>())
	{
		auto& c = bo.Get<EnemyC>();
		DrawCircle(obj_trs.pos, c.range, 1, vec4(1, 0.1, 0.1, 1), 0);
		DrawCircle(obj_trs.pos, c.agro_range, 1, vec4(0.8, 0.8, 0.8, 1), 0);
	}
}

void Render::DrawCircle(vec2 pos, f32 range, f32 width, vec4 color, f32 rotate_scale)
{
	_laser_shader.Use();
	_laser_shader.texture_color = 0;

	_laser_shader.z_pos = z_for_build_connect_highlight / 1000.0f;
	_laser_shader.pos_scale = assets.GetTexture("laser").GetSize().x;
	_laser_shader.view = camera.GetMatrix();
	_laser_shader.projection = ProjMat2d(inputs.screen.size);

	assets.BindTexture(assets.GetTextureId("laser"));
	std::vector<InstanceLaserShaderData> models;

	int count_lines = 30;
	float line_size = 1.0f / 3;

	auto time_now = chrono::high_resolution_clock::now().time_since_epoch();
	chrono::milliseconds time_frac_miliseconds = chrono::duration_cast<chrono::milliseconds>(time_now) - chrono::duration_cast<chrono::seconds>(time_now);
	f32 time_frac = time_frac_miliseconds.count() / 1000.0f;

	float time_add_angle = glm::radians(time_frac * 360 / (float)count_lines) * rotate_scale;

	for (int i = 0; i < count_lines; i++)
	{
		f32 a1 = glm::radians((float)i * 360 / (float)count_lines) + time_add_angle;
		f32 a2 = glm::radians((float)((float)i + line_size) * 360 / (float)count_lines) + time_add_angle;

		vec2 pos_offs1(cos(a1) * range, sin(a1) * range);
		vec2 pos_offs2(cos(a2) * range, sin(a2) * range);

		models.push_back({ pos + pos_offs1, pos + pos_offs2, color, width });
	}

	{
		vao_laser.Bind();
		TmpInstanceDataBuffer instance_buffer;
		instance_buffer.Init<ShaderTextureLaser>(models.size(), models.data());
		glDrawElementsInstanced(GL_TRIANGLES, vao_laser.IndicesCount(), GL_UNSIGNED_INT, 0, models.size());
	}
}

void Render::SwapBuffers(GLFWwindow* window)
{
	EASY_FUNCTION(profiler::colors::Dark);
	glfwSwapBuffers(window);
}

void Render::Terminate()
{
	vao_sprite.Destroy();
	vao_laser.Destroy();
	glfwTerminate();
}

void Render::AddSpriteToDraw(TextureId id, const TrsC& trs)
{
	//mat3 mat = scale(rotate(translate(mat3(1), trs.pos), trs.angle), tex_size);
	_draw_lists[id.id].push_back(trs);
}

void Render::AddLaserToDraw(const InstanceLaserShaderData& cmd)
{
	_laser_draw_lists.emplace_back(cmd);
}

void Render::CleanLists()
{
	for (auto& e : _draw_lists)
		e.clear();
	_laser_draw_lists.clear();
}

void Render::SetFullscreen(GLFWwindow* window, bool is_fullscreen)
{
	if (_is_fullscreen == is_fullscreen) return;
	auto monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);

	_is_fullscreen = is_fullscreen;

	if (_is_fullscreen)
	{
		glfwWindowHint(GLFW_RED_BITS, mode->redBits);
		glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
		glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
		glfwWindowHint(GLFW_DECORATED, false);

		if constexpr (danger_fullscreen)
		{
			glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
		}
		else
		{
			int xpos;
			int ypos;
			//glfwWindowHint(GLFW_FLOATING, true);
			glfwGetMonitorPos(glfwGetPrimaryMonitor(), &xpos, &ypos);
			glfwSetWindowMonitor(window, NULL, xpos, ypos, mode->width, mode->height, mode->refreshRate);
		}
	}
	else
	{
		glfwWindowHint(GLFW_RED_BITS, mode->redBits);
		glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
		glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
		glfwWindowHint(GLFW_DECORATED, true);
		glfwWindowHint(GLFW_FLOATING, false);

		glfwSetWindowMonitor(window, NULL, 0, 0, 640, 480, mode->refreshRate);
		glfwSetWindowPos(window, 10, 50);
	}
}

void APIENTRY GlDebugOutput(GLenum source,
	GLenum type,
	unsigned int id,
	GLenum severity,
	GLsizei length,
	const char* message,
	const void* user_param)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

	std::cout << "---------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
	default:                              std::cout << "Source: UNKNOWN"; break;
	}
	std::cout << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	default:                                std::cout << "Type: UNKNOWN"; break;
	}
	std::cout << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
	default:                             std::cout << "Severity: UNKNOWN"; break;
	}
	std::cout << std::endl;

	__debugbreak();
}
