#pragma once
#include "pch.h"
#include "../shaders/ShaderTexture.h"
#include "components.h"
#include "../shaders/ShaderTextureSelect.h"
#include "../shaders/ShaderTextureColored.h"
#include "../shaders/ShaderTextureLaser.h"
#include "../shaders/ShaderParticle.h"

struct InstanceLaserShaderData
{
	vec2 pos1;
	vec2 pos2;
	vec3 color;
	float width = 2;
};

struct InstanceParticleShaderData
{
	vec4 vec;
	vec4 color;
};

class Render
{
public:
	Render();
	GLFWwindow* Init(bool is_fullscreen);
	void Draw();
	void SwapBuffers(GLFWwindow* window);
	void Terminate();
	void AddSpriteToDraw(TextureId id, const TrsC& trs);
	void AddLaserToDraw(const InstanceLaserShaderData& cmd);
	void CleanLists();
	void SetFullscreen(GLFWwindow* window, bool is_fullscreen);
private:
	void DrawLists();
	void DrawEnergyBars();
	void DrawLaserLists();
	void DrawConnects();
	void DrawStars();
	void DrawBars();
	void DrawParticles();


	void DrawSelection();
	void DrawBuildHighlight();
	void DrawBuildConnectHighlight();
	void DrawObjectRanges(BoxedObjectId id, vec2 pos, bool draw_conn = true);
	void DrawCircle(vec2 pos, f32 range, f32 width, vec4 color, f32 rotate_scale = 1);


	ShaderTexture _main_shader;
	ShaderTextureSelect _select_shader;
	ShaderTextureColored _colored_shader;
	ShaderTextureLaser _laser_shader;
	ShaderParticle _particle_shader;
	
	bool _is_exist = false;
	bool _is_fullscreen = false;

	constexpr static u64 textures_count = 100;
	vector<vector<TrsC>> _draw_lists{ textures_count };

	vector<InstanceLaserShaderData> _laser_draw_lists{};
};

extern Render render;