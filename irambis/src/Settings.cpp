#include "Settings.h"
#include "Inputs.h"

Settings settings;

void Settings::SetTimeSpeed(TimeSpeed ts)
{
	if (ts != TimeSpeed::Pause)
		time_speed_prev = ts;
	time_speed = ts;
}

void Settings::OnSpacebar() // HACK gui logic in settings
{
	if (time_speed == TimeSpeed::Pause)
	{
		if (time_speed_prev == TimeSpeed::Pause)
			time_speed_prev = TimeSpeed::Normal;
		time_speed = time_speed_prev;
	}
	else
	{
		time_speed = TimeSpeed::Pause;
	}
}

void Settings::ResetToEnterLevel()
{
	time_speed = TimeSpeed::Pause;
	time_speed_prev = TimeSpeed::Normal;
}

void Settings::Load()
{
	xml_document doc;
	string xml_file_name = "settings.xml";
	auto result = doc.load_file(xml_file_name.c_str());
	if (result.status != pugi::status_ok) return;
	for (auto node : doc.children("settings"))
	{
		is_fullscreen = node.attribute("is_fullscreen").as_bool(is_fullscreen);
		is_perfect_zoom = node.attribute("is_perfect_zoom").as_bool(is_perfect_zoom);
		is_show_debug_gui = node.attribute("is_show_debug_gui").as_bool(is_show_debug_gui);
	}
}

void Settings::Save()
{
	xml_document doc;
	string xml_file_name = "settings.xml";

	auto node = doc.append_child("settings");

	node.append_attribute("is_fullscreen").set_value(is_fullscreen);
	node.append_attribute("is_perfect_zoom").set_value(is_perfect_zoom);
	node.append_attribute("is_show_debug_gui").set_value(is_show_debug_gui);

	(void)doc.save_file(xml_file_name.c_str(), "\t", pugi::format_no_declaration);
}

Settings::ButtonsSize::operator float() const
{
	if (inputs.screen.size.x >= 1000)
		return 80;
	return 40;
}

Settings::ButtonsBuildLeftSide::operator bool() const
{
	if (inputs.screen.size.x >= 1300)
		return false;
	return true;
}

Settings::BarSize::operator float() const
{
	if (inputs.screen.size.x >= 1000)
		return 200;
	return 130;
}
