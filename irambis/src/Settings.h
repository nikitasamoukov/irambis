#pragma once
#include "pch.h"

class Settings
{
public:
	
	bool is_perfect_zoom = false;
	bool is_fullscreen = false;
	bool is_show_debug_gui = false;

	enum class TimeSpeed
	{
		Pause,
		Slow,
		Normal,
		Fast,
		Max
	};

	TimeSpeed time_speed = TimeSpeed::Normal;
	TimeSpeed time_speed_prev = TimeSpeed::Normal; // HACK save prev speed

	void SetTimeSpeed(TimeSpeed ts);
	void OnSpacebar();
	void ResetToEnterLevel();
	void Load();
	void Save();

	// HACK C++ havent property, but i want property
	class ButtonsSize
	{
	public:
		operator float() const;
	} buttons_size;
	class ButtonsBuildLeftSide
	{
	public:
		operator bool() const;
	} buttons_build_left_side;
	class BarSize
	{
	public:
		operator float() const;
	} bar_size;
};

extern Settings settings;