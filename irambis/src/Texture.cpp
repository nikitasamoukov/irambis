#include "Texture.h"
#include "components.h"

struct TextureData
{
	struct Pixel
	{
		unsigned char r;
		unsigned char g;
		unsigned char b;
		unsigned char a;
	};
	NO_COPY_NO_MOVE_CLASS_DEF(TextureData);

	TextureData() = default;
	void Load(const fs::path& path);
	void Free();
	~TextureData();
	void ChangeToBuildTex();
	DestructionParticles GenerateParticles(float tex_z);

	int width = 0;
	int height = 0;
	static constexpr int n_channels = 4;

	unsigned char* data = nullptr;
private:
	Pixel& GetPixel(u64 x, u64 y)
	{
		return *(Pixel*)(data + (x + y * width) * 4);
	}
};

void TextureData::Load(const fs::path& path)
{
	Free();

	data = stbi_load(path.string().c_str(), &width, &height, nullptr, 4);

	AssertMsg(data, "Failed to load texture:" + path.string());

	for (u64 i = 0; i < width * height; i++)
	{
		if (data[i * 4 + 3] != 255)
		{
			data[i * 4] = 0;
			data[i * 4 + 1] = 0;
			data[i * 4 + 2] = 0;
		}
	}
}

void TextureData::Free()
{
	if (data)
		stbi_image_free(data);
	data = nullptr;
}

TextureData::~TextureData()
{
	Free();
}

void TextureData::ChangeToBuildTex()
{
	for (u64 y = 1; y < width - 1; y++)
		for (u64 x = 1; x < height - 1; x++)
		{
			auto& pixel = GetPixel(x, y);
			if (pixel.a > 0)
			{
				if (GetPixel(x + 1, y).a > 0 &&
					GetPixel(x - 1, y).a > 0 &&
					GetPixel(x, y + 1).a > 0 &&
					GetPixel(x, y - 1).a > 0)
				{
					pixel.r = 0;
					pixel.g = 0;
					pixel.b = 0;
				}
			}
		}
}

DestructionParticles TextureData::GenerateParticles(float tex_z)
{
	DestructionParticles dp;
	f32 offsx = (f32)-width / 2.0f;
	f32 offsy = (f32)-height / 2.0f;

	u64 idx = 0;
	for (int y = height - 1; y >= 0; y--)
		for (int x = 0; x < width; x++, idx++)
		{
			Pixel const& pixel = ((Pixel*)data)[idx];
			if (pixel.a > 0)
			{
				dp.particles.push_back(PackedParticle{
					.pos = {(f32)x + offsx + 0.5f, (f32)y + offsy + 0.5f},
					.render_particle_c = {vec3(pixel.r, pixel.g, pixel.b) / 255.0f, tex_z},
					});
			}
		}

	return dp;
}

Texture Texture::GenGlTextureFromData(TextureData& td, float z)
{
	Texture tex;

	glGenTextures(1, &tex._id);
	glBindTexture(GL_TEXTURE_2D, tex._id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	AssertMsg(td.n_channels == 4, "Not rgba TextureData");
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, td.width, td.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, td.data);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	tex._size = { td.width, td.height };

	tex._z = z;

	return tex;
}

Texture Texture::Load(const fs::path& path, float z, DestructionParticles* dp)
{
	TextureData td;
	td.Load(path);

	Texture tex;
	tex = GenGlTextureFromData(td, z);

	if (dp)
		*dp = td.GenerateParticles(z);

	return tex;
}

tuple<Texture, Texture> Texture::LoadWithBuildGen(const fs::path& path, float z, DestructionParticles* dp, DestructionParticles* dp_build)
{
	TextureData td;
	td.Load(path);

	Texture tex = GenGlTextureFromData(td, z);
	if (dp)
		*dp = td.GenerateParticles(z);

	td.ChangeToBuildTex();

	Texture tex_build = GenGlTextureFromData(td, z);
	if (dp_build)
		*dp_build = td.GenerateParticles(z);

	return { tex, tex_build };
}

void Texture::Free()
{
	if (_id != -1)
	{
		glDeleteTextures(1, &_id);
		_id = -1;
	}
}
