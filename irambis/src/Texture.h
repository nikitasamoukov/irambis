#pragma once
#include "pch.h"

struct DestructionParticles;
struct TextureData;

// HACK dangerous to leak texture resource handle.
// Warn: do not double free, do not create copies
struct Texture
{
	GLuint GetId() const { return _id; }
	vec2i GetSize() const { return _size; }
	float GetZ() const { return _z; }

	static Texture Load(const fs::path& path, float z, DestructionParticles* dp);

	// texture and build texture
	static tuple<Texture, Texture> LoadWithBuildGen(const fs::path& path, float z, DestructionParticles* dp, DestructionParticles* dp_build);
	
	void Free();
private:
	static Texture GenGlTextureFromData(TextureData &td, float z);
	GLuint _id = -1;
	vec2i _size;
	float _z = 0;
};
