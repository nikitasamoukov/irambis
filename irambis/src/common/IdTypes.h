#pragma once
#include "../pch.h"

struct BoxedObjectTag {};
using BoxedObjectId = Id<u64, BoxedObjectTag>;

struct TextureTag {};
using TextureId = Id<u64, TextureTag>;

