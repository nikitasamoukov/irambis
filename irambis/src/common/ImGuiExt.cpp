#include "ImGuiExt.h"

#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
#include "../ImGui/imgui_internal.h"

namespace ImGui {
	// size_arg (for each axis) < 0.0f: align to end, 0.0f: auto, > 0.0f: specified size
	void PointsBar(float fraction, const ImVec2& size_arg, const char* overlay) // HACK touch imgui_internal
	{
		ImGuiWindow* window = GetCurrentWindow();
		if (window->SkipItems)
			return;

		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;

		ImVec2 pos = window->DC.CursorPos;
		ImVec2 size = CalcItemSize(size_arg, CalcItemWidth(), g.FontSize + style.FramePadding.y * 2.0f);
		ImRect bb(pos, { pos.x + size.x, pos.y + size.y });
		ItemSize(size, style.FramePadding.y);
		if (!ItemAdd(bb, 0))
			return;

		// Render
		fraction = ImSaturate(fraction);
		RenderFrame(bb.Min, bb.Max, GetColorU32(ImGuiCol_FrameBg), true, style.FrameRounding);
		bb.Expand(ImVec2(-style.FrameBorderSize, -style.FrameBorderSize));
		const ImVec2 fill_br = ImVec2(ImLerp(bb.Min.x, bb.Max.x, 0), bb.Max.y);
		RenderRectFilledRangeH(window->DrawList, bb, GetColorU32(ImGuiCol_PlotHistogram), 0.0f, fraction, style.FrameRounding);

		// Default displaying the fraction as percentage string, but user can override it
		char overlay_buf[32];
		if (!overlay)
		{
			ImFormatString(overlay_buf, IM_ARRAYSIZE(overlay_buf), "%.0f%%", fraction * 100 + 0.01f);
			overlay = overlay_buf;
		}

		ImVec2 overlay_size = CalcTextSize(overlay, NULL);
		if (overlay_size.x > 0.0f)
			RenderTextClipped(ImVec2(ImClamp(fill_br.x + style.ItemSpacing.x, bb.Min.x, bb.Max.x - overlay_size.x - style.ItemInnerSpacing.x), bb.Min.y), bb.Max, overlay, NULL, &overlay_size, ImVec2(0.0f, 0.5f), &bb);
	}

	// frame_padding < 0: uses FramePadding from style (default)
	// frame_padding = 0: no framing
	// frame_padding > 0: set framing size
	bool ImageButtonText(int uid, vector<ImTextureID> textures, const char* text, const ImVec2& size, const ImVec2& uv0, const ImVec2& uv1, int frame_padding, const ImVec4& bg_col, const ImVec4& tint_col)
	{
		ImGuiContext& g = *GImGui;
		ImGuiWindow* window = g.CurrentWindow;
		if (window->SkipItems)
			return false;

		// Default to using texture ID as ID. User can still push string/integer prefixes.
		PushID((int)uid);
		PushID((void*)(intptr_t)textures[0]);
		const ImGuiID id = window->GetID("#image");
		PopID();
		PopID();

		const ImVec2 padding = (frame_padding >= 0) ? ImVec2((float)frame_padding, (float)frame_padding) : g.Style.FramePadding;

		if (window->SkipItems)
			return false;

		const ImRect bb(window->DC.CursorPos, window->DC.CursorPos + size + padding * 2);
		ItemSize(bb);
		if (!ItemAdd(bb, id))
			return false;

		bool hovered, held;
		bool pressed = ButtonBehavior(bb, id, &hovered, &held);

		// Render
		ImU32 col_with_alpha = GetColorU32((held && hovered) ? ImGuiCol_ButtonActive : hovered ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
		ImColor col_vec(col_with_alpha);

		col_vec.Value.x *= col_vec.Value.w;
		col_vec.Value.y *= col_vec.Value.w;
		col_vec.Value.z *= col_vec.Value.w;
		col_vec.Value.w = 1;

		ImU32 col = col_vec;

		RenderNavHighlight(bb, id);
		RenderFrame(bb.Min, bb.Max, col, true, ImClamp((float)ImMin(padding.x, padding.y), 0.0f, g.Style.FrameRounding));
		if (bg_col.w > 0.0f)
			window->DrawList->AddRectFilled(bb.Min + padding, bb.Max - padding, GetColorU32(bg_col));

		for (auto texture : textures)
			window->DrawList->AddImage(texture, bb.Min + padding, bb.Max - padding, uv0, uv1, GetColorU32(tint_col));

		auto text_size = CalcTextSize(text);
		RenderFrame(bb.Min + ImVec2(2, 2), ImClamp(bb.Min + ImVec2(4, 4) + text_size, bb.Min, bb.Max), col, true, ImClamp((float)ImMin(padding.x, padding.y), 0.0f, g.Style.FrameRounding));

		RenderText(bb.Min + ImVec2(3, 3), text);

		return pressed;
	}

	ImVec2 ButtonSize(const char* label)
	{
		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;
		const ImVec2 label_size = CalcTextSize(label, NULL, true);
		ImGuiWindow* window = g.CurrentWindow;

		ImVec2 pos = window->DC.CursorPos;

		ImVec2 size = CalcItemSize(ImVec2(0, 0), label_size.x + style.FramePadding.x * 2.0f,
			label_size.y + style.FramePadding.y * 2.0f);

		return size;
	}

	void AlignCenterButton(const char* label)
	{
		float val = (ImGui::GetWindowSize().x - ImGui::ButtonSize(label).x) / 2 - ImGui::GetStyle().ItemSpacing.x;
		if (val != 0)
			ImGui::Indent(val);
		else
			ImGui::Indent(0.0000001f);
	}
	void AlignCenterButtonEnd(const char* label)
	{
		float val = (ImGui::GetWindowSize().x - ImGui::ButtonSize(label).x) / 2 - ImGui::GetStyle().ItemSpacing.x;
		if (val != 0)
			ImGui::Unindent(val);
		else
			ImGui::Unindent(0.0000001f);
	}

	void Image(vector<ImTextureID> textures, const ImVec2& size, const ImVec2& uv0, const ImVec2& uv1, const ImVec4& tint_col, const ImVec4& border_col)
	{
		ImGuiWindow* window = GetCurrentWindow();
		if (window->SkipItems)
			return;

		ImRect bb(window->DC.CursorPos, window->DC.CursorPos + size);
		if (border_col.w > 0.0f)
			bb.Max += ImVec2(2, 2);
		ItemSize(bb);
		if (!ItemAdd(bb, 0))
			return;

		if (border_col.w > 0.0f)
		{
			window->DrawList->AddRect(bb.Min, bb.Max, GetColorU32(border_col), 0.0f);
			for (auto user_texture_id : textures)
				window->DrawList->AddImage(user_texture_id, bb.Min + ImVec2(1, 1), bb.Max - ImVec2(1, 1), uv0, uv1, GetColorU32(tint_col));
		}
		else
		{
			for (auto user_texture_id : textures)
				window->DrawList->AddImage(user_texture_id, bb.Min, bb.Max, uv0, uv1, GetColorU32(tint_col));
		}
	}
}