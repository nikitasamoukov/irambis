#pragma once
#include "../pch.h"

namespace ImGui {
	// size_arg (for each axis) < 0.0f: align to end, 0.0f: auto, > 0.0f: specified size
	void PointsBar(float fraction, const ImVec2& size_arg, const char* overlay); // HACK touch imgui_internal


   // frame_padding < 0: uses FramePadding from style (default)
   // frame_padding = 0: no framing
   // frame_padding > 0: set framing size
	bool ImageButtonText(int uid, vector<ImTextureID> textures, const char* text, const ImVec2& size, const ImVec2& uv0 = ImVec2(0, 0), const ImVec2& uv1 = ImVec2(1, 1), int frame_padding = -1, const ImVec4& bg_col = ImVec4(0, 0, 0, 0), const ImVec4& tint_col = ImVec4(1, 1, 1, 1));

	ImVec2 ButtonSize(const char* label);

	void AlignCenterButton(const char* label);
	void AlignCenterButtonEnd(const char* label);
	void Image(vector<ImTextureID> textures, const ImVec2& size, const ImVec2& uv0 = ImVec2(0, 0), const ImVec2& uv1 = ImVec2(1, 1), const ImVec4& tint_col = ImVec4(1, 1, 1, 1), const ImVec4& border_col = ImVec4(0, 0, 0, 0));
}
