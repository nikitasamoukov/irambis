#include "MemoryWatcher.h"

#include "backward.hpp"

bool is_trace_alloc = false;

std::string MemoryWatcher::trace_to_string(backward::StackTrace const& st)
{
	using namespace backward;
	std::string res;

	static TraceResolver tr;
	tr.load_stacktrace(st);
	for (size_t i = 0; i < st.size(); ++i)
	{
		auto it = pointer_to_str.find(st[i].addr);
		if (it == pointer_to_str.end())
		{
			ResolvedTrace trace = tr.resolve(st[i]);
			stringstream ss;
			ss << " " << trace.source.filename
				<< " " << trace.source.function
				<< " (line " << trace.source.line << ", col " << trace.source.col << ")"
				<< " [" << trace.addr << "]";
			pointer_to_str[st[i].addr] = ss.str();
			it = pointer_to_str.find(st[i].addr);
		}
		if (!res.empty()) res += '\n';
		res += "#" + to_string(i);
		res += it->second;
	}

	return res;
}

void MemoryWatcher::init()
{
	is_trace_alloc = true;
}

void MemoryWatcher::process_new(void* p, size_t size)
{
	using namespace backward;
	StackTrace st;
	st.load_here(32);
	st.skip_n_firsts(3);
	//cout << trace_to_string(st);
	current_mem.insert({ p, {st, size} });
}

void MemoryWatcher::process_delete(void* p)
{
	using namespace backward;
	StackTrace st;
	st.load_here(32);
	st.skip_n_firsts(3);
	//cout << trace_to_string(st);
	trace_to_string(st);
	auto src = trace_to_string(st);
	//assert(current_mem.find(p) != current_mem.end() || p == nullptr);
	if (auto it = current_mem.find(p); it != current_mem.end())
		current_mem.erase(it);
}

string MemoryWatcher::get_current()
{
	lock_guard lg(m_alloc);
	recurse_depth++;
	string res;
	int i = 0;
	for (auto& pair : current_mem)
	{
		res += '"' + to_string(i) + '"' + ","
			+ '"' + to_string((size_t)pair.first) + '"' + ","
			+ '"' + to_string(pair.second.size) + '"' + ","
			+ '"' + trace_to_string(pair.second.st) + '"' + '\n';

		i++;
	}
	recurse_depth--;

	return res;
}

void* operator new(size_t size)
{
	void* p = malloc(size);

	if (is_trace_alloc) {
		auto& watcher = MemoryWatcher::get();
		lock_guard lg(watcher.m_alloc);
		watcher.recurse_depth++;
		if (watcher.recurse_depth == 1) {
			watcher.process_new(p, size);
		}
		watcher.recurse_depth--;
	}
	return p;
}

void operator delete(void* p)
{
	if (is_trace_alloc) {
		auto& watcher = MemoryWatcher::get();
		lock_guard lg(watcher.m_alloc);
		watcher.recurse_depth++;
		if (watcher.recurse_depth == 1 && is_trace_alloc) {
			watcher.process_delete(p);
		}
		watcher.recurse_depth--;
	}

	free(p);
}