#pragma once

#include <mutex>

#include "backward.hpp"

extern bool is_trace_alloc;

struct MemoryWatcher
{
	recursive_mutex m_alloc;
	thread::id current_th;
	int recurse_depth = 0;

	struct MemCell
	{
		backward::StackTrace st;
		size_t size = 0;
	};

	unordered_map<void*, string> pointer_to_str;
	unordered_map<void*, MemCell> current_mem;

	MemoryWatcher()
	{
	}
	~MemoryWatcher()
	{
		is_trace_alloc = false;
	}
	static MemoryWatcher& get() {
		static MemoryWatcher instance;
		return instance;
	}

	std::string trace_to_string(backward::StackTrace const& st);

	void init();
	void process_new(void* p, size_t size);
	void process_delete(void* p);
	string get_current();
};

extern std::unique_ptr<MemoryWatcher> watcher;
