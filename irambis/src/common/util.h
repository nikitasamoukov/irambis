#pragma once
#include "../pch.h"

inline string ToString(float val, int precision)
{
	std::stringstream ss;
	ss << std::fixed << std::setprecision(precision) << val;
	return ss.str();
}

template<typename T1, typename T2>
void UpdateAvgT(T1& avg_t, T2 current_t, double new_scale = 0.01)
{
	double avg_t_c = duration_cast<T1>(avg_t).count();
	double current_t_c = duration_cast<T1>(current_t).count();
	double avg_t_new_c = glm::mix(avg_t_c, current_t_c, new_scale);

	avg_t = T1((i64)avg_t_new_c);
}

inline optional<uint32_t> StrToHex(const string& s)
{
	if (!std::all_of(s.begin(), s.end(), [](unsigned char e) {return std::isxdigit(e); }))
		return {};
	switch (s.size())
	{
	case 8:
		return strtoul(s.c_str(), nullptr, 16);
	case 6:
		return (strtoul(s.c_str(), nullptr, 16) << 8) + 0xFF;
	default:
		return {};
	}
}

inline vec4 HexToColor(uint32_t hex)
{
	return
		vec4{
		(hex >> 24) & 0xFF,
		(hex >> 16) & 0xFF,
		(hex >> 8) & 0xFF,
		hex & 0xFF
	} / 255.0f;
}

inline optional<vec4> StrHexToColor(const string& s)
{
	if (s.size() != 7 && s.size() != 9)
		return {};
	if (s[0] != '#')
		return {};

	string str_num = s;
	str_num.erase(0, 1);

	auto hex = StrToHex(str_num);
	if (!hex)
		return {};

	return HexToColor(*hex);
}

inline float RotateToAngle(float base_angle, float target_angle, float step_angle)
{
	base_angle = glm::wrapAngle(base_angle);
	target_angle = glm::wrapAngle(target_angle);

	float rotation_difference = base_angle - target_angle;

	if (abs(rotation_difference) > glm::pi<float>())
	{
		if (rotation_difference > 0)
			rotation_difference -= glm::two_pi<float>();
		else
			rotation_difference += glm::two_pi<float>();
	}

	if (abs(rotation_difference) <= step_angle)
		return target_angle;

	return rotation_difference < 0 ?
		base_angle + step_angle :
		base_angle - step_angle;
}

using Frames = i32;
using FramesF = f32;