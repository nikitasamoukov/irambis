#include "components.h"
#include "Assets.h"
#include "common/util.h"
#include "registry.h"


void HpC::Load(xml_node node)
{
	hp_max = node.attribute("hp").as_float();
	Assert(hp_max > 0);
	hp = hp_max;
}

void RenderC::Load(xml_node node)
{
	string sprite_name = node.attribute("sprite").as_string();
	AssertMsg(assets.HaveTexture(sprite_name), "No texture: " + sprite_name);
	id = assets.GetTextureId(sprite_name);
}

void EnemyC::Load(xml_node node)
{
	range = node.attribute("range").as_float();
	agro_range = node.attribute("agro_range").as_float();
	speed = node.attribute("speed").as_float();
	rotate_speed = glm::radians(node.attribute("rotate_speed").as_float());
}

void EnemyCDesc::Load(xml_node node)
{
	bounty = node.attribute("bounty").as_float();
}

void EnemyShootingC::Load(xml_node node)
{
	shoot_cooldown = frames_per_second / node.attribute("attack_speed").as_float(); // (f/s)/(a/s)=f/a frames per attack
	shoot_obj_id = assets.GetBoxedObjectId(node.attribute("shoot_name").as_string()); // HACK need later load links between assets
}

void EnemyLaserC::Load(xml_node node)
{
	dps = node.attribute("dps").as_float();
	color = *StrHexToColor(node.attribute("color").as_string());
	width = node.attribute("width").as_float(1);
}

void EnemySplitC::Load(xml_node node)
{
	count = node.attribute("count").as_int();
	AssertMsg(count > 0, "EnemySplitC count:" + to_string(count));
	speed = node.attribute("speed").as_float();
	split_obj_id = assets.GetBoxedObjectId(node.attribute("split_name").as_string()); // HACK need later load links between assets
}

void StructBuildingC::Load(xml_node node)
{
	cost = node.attribute("cost").as_float();
	build_time = node.attribute("build_time").as_float();
	AssertMsg(build_time > 0, "build_time is: " + to_string(build_time));

	build_energy_per_second = node.attribute("build_energy").as_float() / max(build_time, 0.000001f);

	is_upgrade = node.attribute("is_upgrade").as_bool(false);

	//Frames frames_to_end = (1 - build_progress) * build_time * frames_per_second; // ()*(s)*(f/s)=f
	f32 en_per_frame = build_energy_per_second * seconds_in_frame; // (e/s)*(s/f)=(e/f)
	en_max = en_per_frame;
}

f32 StructBuildingC::ProgressIncrease(f32 percent_of_frame_request)
{
	f32 percent_of_build = min(percent_of_frame_request / max(build_time * frames_per_second, 0.00001f), 1.0f);// (f)/((s)*(f/s))/s=e
	build_progress += percent_of_build;
	return percent_of_build;
}

void EnergyC::Load(xml_node node)
{
	en_max = node.attribute("en").as_float(0);
	Assert(en_max > 0);
	en = 0;
	gen = node.attribute("gen").as_float(0);
	is_consumer = node.attribute("is_consumer").as_bool(false);
}

u64 ConnectIdC::FindEmpty()
{
	u64 max_conn = connects.size();
	if (is_only_one_connect)
		max_conn = 1;

	for (u64 i = 0; i < max_conn; i++)
	{
		if (!registry.valid(connects[i].entity))
		{
			return i;
		}
	}
	return connects.size();
}

void ConnectIdC::Load(xml_node node)
{
	is_only_one_connect = node.attribute("is_only_one_connect").as_bool(false);
}

void StructUpgradesCDesc::Load(xml_node node)
{
	for (auto up_node : node.children("Upgrade"))
	{
		upgrades.emplace_back(up_node.attribute("name").as_string());
	}
}

void RenderTurretC::Load(xml_node node)
{
	string sprite_name = node.attribute("sprite").as_string();
	AssertMsg(assets.HaveTexture(sprite_name), "No texture: " + sprite_name);
	id = assets.GetTextureId(sprite_name);
}

void RenderEnergyC::Load(xml_node node)
{
	size.x = node.attribute("size_x").as_float() + 0.03f; // HACK no black line(laser shader bad draw rect)
	size.y = node.attribute("size_y").as_float() + 0.03f;
}

void TurretC::Load(xml_node node)
{
	range = node.attribute("range").as_float();
}

void TurretLaserC::Load(xml_node node)
{
	en_per_second = node.attribute("en_per_second").as_float();
	dps = node.attribute("dps").as_float();
	color = *StrHexToColor(node.attribute("color").as_string());
	failure_cooldown = node.attribute("failure_cooldown").as_float() * frames_per_second;
	width = node.attribute("width").as_float(1);
}

void TurretHighLaserC::Load(xml_node node)
{
	en_per_second = node.attribute("en_per_second").as_float();
	dps = node.attribute("dps").as_float();
	failure_cooldown = node.attribute("failure_cooldown").as_float() * frames_per_second;
	FramesF charge_time = max(node.attribute("charge_time").as_float(), 0.0001f) * frames_per_second; // HACK tiny hack
	charge_per_frame = 1.0f / charge_time;
	color = *StrHexToColor(node.attribute("color").as_string());
	width = node.attribute("width").as_float(1);
}

void TurretRepairC::Load(xml_node node)
{
	en_per_second = node.attribute("en_per_second").as_float();
	dps = node.attribute("dps").as_float();
	color = *StrHexToColor(node.attribute("color").as_string());
	failure_cooldown = node.attribute("failure_cooldown").as_float() * frames_per_second;
	width = node.attribute("width").as_float(1);
}

void TurretCannonC::Load(xml_node node)
{
	shoot_cooldown = frames_per_second / node.attribute("attack_speed").as_float(); // (f/s)/(a/s)=f/a frames per attack
	shoot_obj_id = assets.GetBoxedObjectId(node.attribute("shoot_name").as_string()); // HACK need later load links between assets
	en_per_shoot = node.attribute("en_per_shoot").as_float(); 
}

void DmgC::Load(xml_node node)
{
	dmg = node.attribute("dmg").as_float();

	string dmg_type_str = node.attribute("type").as_string("thermal");
	if (dmg_type_str == "penetration")
	{
		t = Type::Penetration;
	}
	else if (dmg_type_str == "thermal")
	{
		t = Type::Thermal;
	}
	else if (dmg_type_str == "explosive")
	{
		t = Type::Explosive;
	}
	else
	{
		AssertMsgFail("unknown dmg type:" + dmg_type_str);
	}
}

void BulletC::Load(xml_node node)
{
	speed = node.attribute("speed").as_float();
	AssertMsg(speed, "Bullet speed cant be zero");
	string faction_str = node.attribute("faction").as_string("enemy");
	if (faction_str == "enemy")
	{
		faction = Faction::Enemy;
	}
	else if (faction_str == "player")
	{
		faction = Faction::Player;
	}
	else
	{
		AssertMsgFail("unknown faction type:" + faction_str);
	}
}
