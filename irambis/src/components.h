#pragma once
#include "pch.h"
#include "common/IdTypes.h"
#include "helpers/Object.h"
#include "common/util.h"

extern entt::dispatcher dispatcher;

struct HpC
{
	f32 hp = 0;
	f32 hp_max = 0;

	void Load(xml_node node);
};

struct TrsC // HACK its common used as a transform not as component
{
	vec2 pos{ 0 };
	f32 angle = 0;
};

struct PhysC
{
	vec2 vel;
	f32 r = 1;
	bool is_static = false;

	void Load(xml_node node)
	{
		r = node.attribute("r").as_float(1);
		is_static = node.attribute("is_static").as_bool(false);
	}
};

struct RenderC
{
	TextureId id;

	void Load(xml_node node);
};

struct KillCTag {};
struct KillNoBountyCTag {};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Enemy

struct TypeC
{
	BoxedObjectId id;
};

struct EnemyC
{
	f32 range = 0;
	f32 agro_range = 0;
	f32 speed = 0;
	f32 rotate_speed = 0;

	void Load(xml_node node);
};

struct EnemyCDesc
{
	f32 bounty = 0;

	void Load(xml_node node);
};

struct EnemyShootingC
{
	// 0.1 mean need shoot on 10% frame time after current frame time
	FramesF cooldown = 0;
	//  Frame         0               1               2               3               4
	//                F---------------F---------------F---------------F---------------F
	// Current frame                                  ^ ^
	// time: 2.0  ------------------------------------| |
	//                                                  |
	// Shoot time: 2 + 0.1 -----------------------------|

	FramesF shoot_cooldown = 0;
	BoxedObjectId shoot_obj_id;

	void Load(xml_node node);
};

struct EnemyRamC
{
	void Load(xml_node node) {}
};

struct EnemyLaserC
{
	f32 dps = 0;
	vec3 color;
	f32 width = 1;
	void Load(xml_node node);
};

struct EnemySplitC
{
	i32 count = 0;
	f32 speed = 0;
	BoxedObjectId split_obj_id;
	void Load(xml_node node);
};

struct EnemyTargetC
{
	Object target;
	vec2 target_pos;
};

// When attack enemy stop move
struct EnemyAttackNowCTag {};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Struct

struct ConnectGroupTag {};
using ConnectGroupId = Id<u64, ConnectGroupTag>;

enum class EnergyType
{
	Nothing,
	Build,
	Storage,
	Consumer,
};

inline string EnergyTypeToString(EnergyType et)
{
	switch (et)
	{
	case EnergyType::Nothing: return "Nothing";
	case EnergyType::Build: return "Build";
	case EnergyType::Storage: return "Storage";
	case EnergyType::Consumer: return "Consumer";
	default: return "ERROR";
	}
}

struct StructC
{
	ConnectGroupId id{ 0 };
	EnergyType et = EnergyType::Nothing;
	void Load(xml_node node) {}
};

struct StructBuildingC
{
	f32 en_max = 0;

	f32 cost = 0;
	f32 cost_sell = 0;
	f32 cost_full = 0;
	f32 build_energy_per_second = 0;
	f32 build_time = 0;
	f32 build_progress = 0;
	f32 hp_build = 0;

	bool is_upgrade = false;

	void Load(xml_node node);
	f32 ProgressIncrease(f32 percent_of_frame_request);
};

struct StructZeroRewardSell {};

struct EnergyC
{
	f32 gen = 0;
	f32 en = 0;
	f32 en_max = 0;
	bool is_consumer = false;

	void Load(xml_node node);
};

struct ConnectIdC // connects ids
{
	array<Object, max_connects> connects{};
	bool is_only_one_connect = false;

	u64 FindEmpty();

	void Load(xml_node node);
};

struct ConnectPosC
{
	struct PackedPosOffset
	{
		i8 x = 0;
		i8 y = 0;
		bool IsValid() { return x | y; }
	};

	array<PackedPosOffset, max_connects> connects{};

	void Load(xml_node node) {}
};

struct StructUpgradesCDesc
{
	vector<string> upgrades;

	void Load(xml_node node);
};



struct RenderTurretC
{
	TextureId id;

	void Load(xml_node node);
};

struct TurretRotateC
{
	f32 a;

	void Load(xml_node node) {}
};

struct RenderEnergyC
{
	vec2 size;

	void Load(xml_node node);
};

struct TurretTargetC
{
	Object target;
};

struct TurretC
{
	f32 range = 0;

	void Load(xml_node node);
};

struct TurretLaserC
{
	f32 en_per_second = 0;
	f32 dps = 0;
	Frames cooldown = 1; // HACK 1 colldown to fast start on power up
	Frames failure_cooldown = 1;
	vec3 color;
	f32 width = 1;

	void Load(xml_node node);
};

struct TurretHighLaserC
{
	f32 en_per_second = 0;
	f32 dps = 0;
	f32 charge = 0;

	f32 charge_per_frame = 1;
	Frames cooldown = 1; // HACK 1 colldown to fast start on power up
	Frames failure_cooldown = 1;
	vec3 color;
	f32 width = 1;

	void Load(xml_node node);
};

struct TurretRepairC
{
	f32 en_per_second = 0;
	f32 dps = 0;

	f32 charge_per_frame = 1;
	Frames cooldown = 1; // HACK 1 colldown to fast start on power up
	Frames failure_cooldown = 1;
	vec3 color;
	f32 width = 1;

	void Load(xml_node node);
};

struct TurretCannonC
{
	// 0.1 mean need shoot on 10% frame time after current frame time
	FramesF cooldown = 0;
	//  Frame         0               1               2               3               4
	//                F---------------F---------------F---------------F---------------F
	// Current frame                                  ^ ^
	// time: 2.0  ------------------------------------| |
	//                                                  |
	// Shoot time: 2 + 0.1 -----------------------------|

	FramesF shoot_cooldown = 0;
	BoxedObjectId shoot_obj_id;
	f32 en_per_shoot = 0;

	void Load(xml_node node);
};

struct DmgC
{
	enum class Type
	{
		Thermal,
		Penetration,
		Explosive,
	};

	f32 dmg = 0;
	Type t = Type::Thermal;

	void Load(xml_node node);
};

struct BulletC
{
	enum class Faction
	{
		Player,
		Enemy,
	};
	Faction faction = Faction::Enemy;
	f32 speed = 0;
	FramesF lifetime = 0;

	void Load(xml_node node);
};



// HACK HACKS

struct EnergySaveWhenUpgradeC
{
	f32 en;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Particles

struct RenderParticleC
{
	vec3 col;
	f32 z;
};

struct ParticleLifetimeC
{
	f32 life = 1;
	f32 frame_decay = 1;
};

struct PackedParticle // HACK not component
{
	vec2 pos;
	RenderParticleC render_particle_c;
};

struct DestructionParticles
{
	vector<PackedParticle> particles;
};