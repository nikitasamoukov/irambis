#pragma once
#include "pch.h" // HACK include loop
#include <chrono>

constexpr u64 max_connects = 10;
constexpr float struct_spacing = 2;
constexpr float connect_dist = 50;

constexpr float connect_width = 0.5;

constexpr float up_sell_scale = 0.5;


constexpr float z_for_connect = 95;
constexpr float z_for_build_highlight = -210;
constexpr float z_for_build_connect_highlight = 90;
constexpr float z_for_bars = -105;
constexpr float z_for_select_highlight = -200;
constexpr float z_for_lasers = -100;

constexpr float z_for_energy_bars = 5;

constexpr auto frame_time = chrono::microseconds(16666);
constexpr float energy_min_delta = 0.0001;
constexpr float seconds_in_frame =
chrono::duration_cast<chrono::nanoseconds>(frame_time).count() /
(float)chrono::duration_cast<chrono::nanoseconds>(std::chrono::seconds(1)).count();
constexpr float frames_per_second = 1 / seconds_in_frame;



constexpr float speed_to_accel_scale = 1 / 19.0f / 60.0f; // HACK try solve lim for speed and damping


constexpr vec3 color_energy_bar = { 0.1, 0.9, 1 };
const ImColor color_energy_bar_gui = { color_energy_bar.r * 0.7f, color_energy_bar.g * 0.7f, color_energy_bar.b * 0.7f }; // HACK copy code
constexpr vec3 color_hp_bar = { 0.1, 0.9, 0.1 };
const ImColor color_hp_bar_gui = { color_hp_bar.r * 0.7f, color_hp_bar.g * 0.7f, color_hp_bar.b * 0.7f };
constexpr vec3 color_build_bar = { 1, 1, 0.1 };
const ImColor color_build_bar_gui = { color_build_bar.r * 0.7f, color_build_bar.g * 0.7f, color_build_bar.b * 0.7f };
constexpr vec3 color_reload_bar = { 0.5, 0.5, 0.5 };
const ImColor color_reload_bar_gui = { color_reload_bar.r * 0.7f, color_reload_bar.g * 0.7f, color_reload_bar.b * 0.7f };




constexpr float max_struct_radius = 30;
constexpr float max_enemy_radius = 30;

constexpr float grid_cell_size = 100;


constexpr bool is_sandbox = false;



enum class BuildType
{
	Debug,
	Release,
	Retail,
	Unknown,
};

static constexpr BuildType build_type =
#if defined(_DEBUG)
BuildType::Debug
#elif defined(RETAIL)
BuildType::Retail
#elif defined(NDEBUG)
BuildType::Release
#else
BuildType::Unknown
#endif
;


static constexpr const char* window_title = "Irambis";
static constexpr bool danger_fullscreen = true;