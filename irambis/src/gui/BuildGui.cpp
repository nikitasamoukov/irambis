#include "BuildGui.h"
#include "../Player.h"
#include "../Assets.h"
#include "../components.h"
#include "../common/util.h"
#include "../common/ImGuiExt.h"
#include "../Settings.h"

void BuildGui()
{
	EASY_FUNCTION(profiler::colors::Dark);

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImGuiIO& io = ImGui::GetIO();

	ImVec2 window_pos = ImVec2(io.DisplaySize.x / 2, io.DisplaySize.y);
	ImVec2 window_pos_pivot = ImVec2(0.5f, 1.0f);

	if (settings.buttons_build_left_side)
	{
		window_pos = ImVec2(0, io.DisplaySize.y);
		window_pos_pivot = ImVec2(0, 1.0f);
	}

	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::Begin("Build menu", nullptr, window_flags);

	bool is_second_iterate = false;

	for (auto& [name, id] : assets.ObjectsIdMap())
	{
		const auto& bo = assets.GetBoxedObject(id);

		if (bo.IsStruct() && !bo.Get<StructBuildingC>().is_upgrade)
		{
			Texture tex = assets.GetTexture(bo.Get<RenderC>().id);
			GLuint tex_id = tex.GetId();
			f32 uv_sz = (bo.Size() + 2) / f32(tex.GetSize().x);
			if (is_second_iterate)
				ImGui::SameLine();

			if (player.selected_building_to_place == id)
			{
				if (player.money >= bo.Cost())
					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.4f, 0.6f, 0.8f, 1.0f });
				else
					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.6f, 0.6f, 0.6f, 1.0f });
			}
			else
			{
				if (player.money >= bo.Cost())
					ImGui::PushStyleColor(ImGuiCol_Button, ImGui::GetStyleColorVec4(ImGuiCol_Button));
				else
					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.3f, 0.3f, 0.3f, 1.0f });
			}

			if (player.money >= bo.Cost())
			{
				ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImGui::GetStyleColorVec4(ImGuiCol_ButtonHovered));
				ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImGui::GetStyleColorVec4(ImGuiCol_ButtonActive));
			}
			else
			{
				ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.4f, 0.4f, 0.4f, 1.0f });
				ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.6f, 0.6f, 0.6f, 1.0f });
			}

			vector<ImTextureID> textures;
			textures.push_back((ImTextureID)tex_id);
			if (bo.Has<RenderTurretC>())
			{
				auto& tex_turr = assets.GetTexture(bo.Get<RenderTurretC>().id);
				textures.push_back((ImTextureID)tex_turr.GetId());
			}

			if (ImGui::ImageButtonText((int)id.id, textures, ToString(bo.Cost(), 0).c_str(), { settings.buttons_size, settings.buttons_size }, { 0.5f - uv_sz, 0.5f - uv_sz }, { 0.5f + uv_sz, 0.5f + uv_sz }))
			{
				if (player.selected_building_to_place != id)
					player.selected_building_to_place = id;
				else
					player.selected_building_to_place = {};
			}

			if (ImGui::IsItemHovered())
			{
				ImGui::BeginTooltip();
				ImGui::Text("%s", bo.InfoTextBuild().c_str());
				ImGui::EndTooltip();
			}

			ImGui::PopStyleColor(3);
			is_second_iterate = true;
		}
	}

	ImGui::End();
}
