#include "DebugGui.h"
#include "../common/util.h"
#include "../Core.h"
#include "../systems/SystemEnergy.h"
#include "../Levels.h"
#include "../common/ImGuiExt.h"
#include "../Assets.h"
#include "../components.h"
#include "../Settings.h"
#include "../common/MemoryWatcher.h"


void PerformanceWindow();
void EnergyGroupsWindow();

void DebugGui()
{
	EASY_FUNCTION(profiler::colors::Dark);
	PerformanceWindow();
	EnergyGroupsWindow();
}

#define MACRO_GET_C(T,n) 	T n;\
	if (bo.Has<T>())\
	{\
		(n) = bo.Get<T>();\
	}

void DrawBItem(BoxedObject const& bo, bool is_first = true) // HACK very hacky
{
	MACRO_GET_C(HpC, hp_c);
	MACRO_GET_C(EnergyC, energy_c);
	MACRO_GET_C(StructBuildingC, struct_building_c);
	MACRO_GET_C(StructUpgradesCDesc, struct_upgrades_c_desc);
	MACRO_GET_C(TurretC, turret_c);

	f32 dps = 0;
	f32 eps = 0;

	ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(0.3, 0.3, 0.3, 1));

	if (bo.Has<TurretLaserC>())
	{
		MACRO_GET_C(TurretLaserC, turret_laser_c);
		dps = turret_laser_c.dps;
		eps = turret_laser_c.en_per_second;
	}

	if (bo.Has<TurretCannonC>())
	{
		MACRO_GET_C(TurretCannonC, turret_cannon_c);
		auto& shoot_bo = assets.GetBoxedObject(turret_cannon_c.shoot_obj_id);

		dps = shoot_bo.Get<DmgC>().dmg / turret_cannon_c.shoot_cooldown * frames_per_second;
		eps = turret_cannon_c.en_per_shoot / turret_cannon_c.shoot_cooldown * frames_per_second;
	}

	if (bo.Has<TurretHighLaserC>())
	{
		MACRO_GET_C(TurretHighLaserC, turret_high_laser_c);
		dps = turret_high_laser_c.dps;
		eps = turret_high_laser_c.en_per_second;
	}

	{
		Texture tex = assets.GetTexture(bo.Get<RenderC>().id);
		GLuint tex_id = tex.GetId();
		f32 uv_sz = (bo.Size() + 2) / f32(tex.GetSize().x);

		vector<ImTextureID> textures;
		textures.push_back((ImTextureID)tex_id);
		if (bo.Has<RenderTurretC>())
		{
			auto& tex_turr = assets.GetTexture(bo.Get<RenderTurretC>().id);
			textures.push_back((ImTextureID)tex_turr.GetId());
		}
		ImGui::Image(textures, { settings.buttons_size, settings.buttons_size }, { 0.5f - uv_sz, 0.5f - uv_sz }, { 0.5f + uv_sz, 0.5f + uv_sz });

		ImGui::NextColumn();
	}

	{
		ImGui::PointsBar(0, { -1, 0 }, assets.GetName(bo.Id()).c_str());
		ImGui::NextColumn();
	}
	{
		string s;
		s += ToString(hp_c.hp_max, 0);
		ImGui::PointsBar(0, { -1, 0 }, s.c_str());
		ImGui::NextColumn();
	}
	{
		string s;
		s += ToString(energy_c.en_max, 0);
		ImGui::PointsBar(0, { -1, 0 }, s.c_str());
		ImGui::NextColumn();
	}
	{
		string s;
		s += ToString(struct_building_c.cost_full, 0);
		ImGui::PointsBar(0, { -1, 0 }, s.c_str());
		ImGui::NextColumn();
	}
	{
		string s;
		s += ToString(dps, 1);
		ImGui::PointsBar(0, { -1, 0 }, s.c_str());
		ImGui::NextColumn();
	}
	{
		string s;
		s += ToString(eps, 1);
		ImGui::PointsBar(0, { -1, 0 }, s.c_str());
		ImGui::NextColumn();
	}
	{
		string s;
		s += ToString(turret_c.range, 0);
		ImGui::PointsBar(0, { -1, 0 }, s.c_str());
		ImGui::NextColumn();
	}

#define MACRO_DRAW_PER string s;\
	static float max_perc = 1;\
	if (is_first) max_perc = perc;\
		s += ToString(perc / max_perc * 100, 0);\
		ImGui::PointsBar(perc / max_perc, { -1, 0 }, s.c_str());\
		ImGui::NextColumn();

	{
		float perc = energy_c.en_max / struct_building_c.cost_full;
		MACRO_DRAW_PER;
	}
	{
		float perc = hp_c.hp_max / struct_building_c.cost_full;
		MACRO_DRAW_PER;
	}

	{
		float perc = dps / struct_building_c.cost_full;
		MACRO_DRAW_PER;
	}
	{
		float perc = dps / eps;
		MACRO_DRAW_PER;
	}
	{
		float perc = eps / energy_c.en_max;
		MACRO_DRAW_PER;
	}
	{
		float perc = turret_c.range / struct_building_c.cost_full;
		MACRO_DRAW_PER;
	}

	for (auto& up_name : struct_upgrades_c_desc.upgrades)
	{
		auto& up_bo = assets.GetBoxedObject(up_name);
		DrawBItem(up_bo, false);
	}

	ImGui::PopStyleColor();
}

void BalanceWindow()
{
	EASY_FUNCTION(profiler::colors::Dark);
	ImGui::Begin("Balance gui");

	ImGui::Columns(14, "Table balance", false);  // 3-ways, no border

	ImGui::PointsBar(0, { -1, 0 }, "Img");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "name");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "hp");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "en");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "cost");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "dps");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "eps");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "range");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "en/cost");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "hp/cost");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "dps/cost");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "dps/eps");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "eps/en");
	ImGui::NextColumn();
	ImGui::PointsBar(0, { -1, 0 }, "range/cost");
	ImGui::NextColumn();
	ImGui::Separator();

	for (auto& [bo_name, bo_id] : assets.ObjectsIdMap())
	{
		auto& bo = assets.GetBoxedObject(bo_id);
		if (!bo.IsStruct()) continue;
		if (!bo.Has<StructBuildingC>()) continue;
		if (bo.Get<StructBuildingC>().is_upgrade) continue;

		DrawBItem(bo);
		ImGui::Separator();
	}
	ImGui::Columns(1);


	ImGui::End();
}

void MemoryWindow()
{
	EASY_FUNCTION(profiler::colors::Dark);
	ImGui::Begin("Memory gui");

	static string memorydata;
	if (ImGui::Button("Collect"))
	{
		memorydata = MemoryWatcher::get().get_current();
		fstream f("memory.csv", ios_base::out);
		f << memorydata;
		f.close();
	}

	ImGui::Text("%s", memorydata.data());

	ImGui::End();
}

void PerformanceWindow()
{
	EASY_FUNCTION(profiler::colors::Dark);

	if constexpr (build_type == BuildType::Retail) return;
	if (!settings.is_show_debug_gui) return;

	ImGui::Begin("Debug gui");

	ImGui::Text("Render: %.3f ms", (float)core.info.render_time.count() / 1000000.0f);
	ImGui::Text("Update: %.3f ms (%.3f full)",
		(float)core.info.update_frame_time.count() / 1000000.0f,
		(float)core.info.updates_time.count() / 1000000.0f
	);

	static bool is_show_balance = false;
	ImGui::Text("is_show_balance:");
	ImGui::SameLine();
	ImGui::Checkbox("##is_show_balance:", &is_show_balance);

	static bool is_show_memory = false;
	ImGui::Text("is_show_memory:");
	ImGui::SameLine();
	ImGui::Checkbox("##is_show_memory:", &is_show_memory);


	if (ImGui::Button("Save setup"))
	{
		SaveSetup();
	}
	static char buf[100] = { 0 };

	ImGui::InputText("file:", buf, 100);
	if (ImGui::Button("Load setup"))
	{
		levels.LoadSetup(buf);
	}

	if (ImGui::Button("Clean game"))
	{
		core.CleanGame();
	}

	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::Text("Sleep: %.3f ms", (float)core.info.sleep_time.count() / 1000000.0f);
	ImGui::Text("Full: %.3f ms", (float)(core.info.updates_time + core.info.render_time + core.info.sleep_time).count() / 1000000.0f);
	ImGui::Text("Avg update: %.3f ms", (float)(core.info.avg_update_time).count() / 1000000.0f);
	ImGui::Text("Avg render: %.3f ms", (float)(core.info.avg_render_time).count() / 1000000.0f);
	ImGui::Text("Game frame: %i", (int)(core.GetGameFrame()));
	ImGui::End();

	if (is_show_balance)
		BalanceWindow();
	if (is_show_memory)
		MemoryWindow();
}

void EnergyGroupsWindow()
{
	EASY_FUNCTION(profiler::colors::Dark);

	if constexpr (build_type == BuildType::Retail) return;
	if (!settings.is_show_debug_gui) return;

	ImGui::Begin("Energy groups");

	for (auto& e_group : energy_groups.egs)
	{
		ImGui::Separator();
		ImGui::Text("Group");
		ImGui::Text("Gen: %s", ToString(e_group.energy_gen, 1).c_str());
		ImGui::Text("build: %s", e_group.info_builds.ToString().c_str());
		ImGui::Text("storage: %s", e_group.info_storage.ToString().c_str());
		ImGui::Text("consumer: %s", e_group.info_consumer.ToString().c_str());
	}

	ImGui::End();
}