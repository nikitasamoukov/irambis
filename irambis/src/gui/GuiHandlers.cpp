#include "GuiHandlers.h"
#include "../helpers/HelpersMouse.h"
#include "../helpers/Object.h"
#include "../components.h"
#include "../Player.h"
#include "../helpers/Query.h"

void GuiHandlerClickSelect(const EClick& ev)
{
	vec2 game_pos = ConvertMousePosToGame(ev.pos);

	player.selected_obj = GetObjectOnClick(game_pos);
}

void GuiHandlerClickBuild(const EClick& ev)
{
	ETryBuildStructure ev_build{ ev.pos, *player.selected_building_to_place };
	dispatcher.trigger<ETryBuildStructure>(ev_build);
	
	if (!(ev.mods & GLFW_MOD_SHIFT))
		player.selected_building_to_place = {};
}

void GuiGameHandlerClick(const EClick& ev)
{
	if (!holds_alternative<Player::MenuGameState>(player.menu)) return;
	if (player.selected_building_to_place)
		GuiHandlerClickBuild(ev);
	else
		GuiHandlerClickSelect(ev);
}

void RegisterGuiHandlers()
{
	dispatcher.sink<EClick>().connect<GuiGameHandlerClick>();
}
