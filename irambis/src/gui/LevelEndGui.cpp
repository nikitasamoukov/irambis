#include "LevelEndGui.h"
#include "../Player.h"

LevelEndGui level_end_gui;

void LevelEndGui::Draw()
{
	EASY_FUNCTION(profiler::colors::Dark);
	if (!_is_enable) return;

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(io.DisplaySize.x / 2, io.DisplaySize.y / 2);
	ImVec2 window_pos_pivot = ImVec2(0.5f, 0.5f);
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::Begin("Level end", nullptr, window_flags);

	string result_text = _is_win ? "Win" : "Lose";
	ImGui::Text("Level result: %s", result_text.c_str());
	if (ImGui::Button("Main menu", { -1, 0 }))
	{
		player.MenuSwitchToMain();
	}

	ImGui::End();
}

void LevelEndGui::Enable(bool is_win)
{
	if (!_is_enable)
	{
		_is_enable = true;
		_is_win = is_win;
	}
}

void LevelEndGui::Reset()
{
	_is_enable = false;
	_is_win = false;
}
