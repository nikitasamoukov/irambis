#pragma once
#include "../pch.h"

class LevelEndGui
{
public:
	void Draw();
	void Enable(bool is_win);
	void Reset();

private:
	bool _is_enable = false;
	bool _is_win = false;
};

extern LevelEndGui level_end_gui;