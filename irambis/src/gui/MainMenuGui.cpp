#include "MainMenuGui.h"
#include "../Player.h"
#include "../common/ImGuiExt.h"
#include "../Core.h"
#include "../Levels.h"
#include "SettingsGui.h"

void MainMenuGui()
{
	EASY_FUNCTION(profiler::colors::Dark);

	static bool is_show_settings = false;

	if (is_show_settings)
	{
		if (SettingsGui())
			is_show_settings = false;
		return;
	}

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(io.DisplaySize.x / 2, io.DisplaySize.y / 2);
	ImVec2 window_pos_pivot = ImVec2(0.5f, 0.5f);
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::Begin("Main menu", nullptr, window_flags);

	ImGui::Text("Levels:");

	int min_level = 0;
	if constexpr (build_type != BuildType::Retail)
		min_level = -1;

	for (int i = min_level; i <= 8; i++)
	{
		if (i > 0)
			ImGui::SameLine();

		if (ImGui::Button(to_string(i).c_str()))
		{
			player.menu = Player::MenuGameState();
			core.CleanGame();
			levels.LoadSetup(to_string(i) + ".xml");
			core.UpdateWorld();
		}
	}

	ImGui::AlignCenterButton("Settings");
	if (ImGui::Button("Settings"))
	{
		is_show_settings = true;
	}
	ImGui::AlignCenterButtonEnd("Settings");

	ImGui::AlignCenterButton("Quit");
	if (ImGui::Button("Quit"))
	{
		core.QuitGame();
	}
	ImGui::AlignCenterButtonEnd("Quit");

	ImGui::End();
}
