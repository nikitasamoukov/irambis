#include "PauseMenuGui.h"
#include "../Player.h"
#include "../common/ImGuiExt.h"
#include "../Settings.h"
#include "SettingsGui.h"

void PauseMenuGui()
{
	EASY_FUNCTION(profiler::colors::Dark);

	static bool is_show_settings = false;

	if (is_show_settings)
	{
		if (SettingsGui())
			is_show_settings = false;
		return;
	}

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(io.DisplaySize.x / 2, io.DisplaySize.y / 2);
	ImVec2 window_pos_pivot = ImVec2(0.5f, 0.5f);
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::SetNextWindowSize({ 190, 0 });
	ImGui::Begin("Pause menu", nullptr, window_flags);
	{
		float text_sizex = ImGui::CalcTextSize("Pause").x;
		ImGui::SetCursorPosX(ImGui::GetWindowSize().x / 2 - text_sizex / 2);
		ImGui::Text("Pause");
	}
	{
		vector<const char*> texts = {
			"W, A, S, D - move",
			"-, =, + - zoom",
			"Spacebar - pause",
			"U - upgrade",
			"I - second upgrade",
			"X - sell",
			"C - move camera to base",
			"Shift - place multiply" };

		for (auto& text : texts)
		{
			float text_sizex = ImGui::CalcTextSize(text).x;
			ImGui::SetCursorPosX(ImGui::GetWindowSize().x / 2 - text_sizex / 2);
			ImGui::Text("%s", text);
		}
	}

	ImGui::AlignCenterButton("Resume");
	if (ImGui::Button("Resume"))
	{
		player.MenuSwitchToGame();
	}
	ImGui::AlignCenterButtonEnd("Resume");

	ImGui::AlignCenterButton("Settings");
	if (ImGui::Button("Settings"))
	{
		is_show_settings = true;
	}
	ImGui::AlignCenterButtonEnd("Settings");

	ImGui::AlignCenterButton("To main menu");
	if (ImGui::Button("To main menu"))
	{
		player.MenuSwitchToMain();
	}
	ImGui::AlignCenterButtonEnd("To main menu");

	ImGui::End();
}
