#include "PlayerGui.h"
#include "../Player.h"
#include "../common/util.h"
#include "../common/ImGuiExt.h"
#include "../systems/SystemEnergy.h"
#include "../Settings.h"

struct EnergySummary
{
	f32 en = 0;
	f32 en_max = 0;
	void AddInfo(StructsEnergyInfo const& info)
	{
		en += info.en;
		en_max += info.en_max;
	}
};

EnergySummary ComputeEnergySummary()
{
	EnergySummary res;

	res = std::accumulate(energy_groups.egs.begin(), energy_groups.egs.end(), EnergySummary(), [](EnergySummary a, EnergyGroupInfo const& b) {
		a.AddInfo(b.info_storage);
		return a;
		});

	return res;
}

void PlayerGui()
{
	EASY_FUNCTION(profiler::colors::Dark);

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(io.DisplaySize.x, io.DisplaySize.y);
	ImVec2 window_pos_pivot = ImVec2(1.0f, 1.0f);
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::Begin("Player status", nullptr, window_flags);

	ImGui::Text("Money: %s", ToString(player.money, 0).c_str());
	{
		auto info = ComputeEnergySummary();
		string text;
		if (settings.bar_size > 150)
			text = "Energy: " + ToString(info.en, 0) + "/" + ToString(info.en_max, 0);
		else
			text = ToString(info.en, 0) + "/" + ToString(info.en_max, 0);

		ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(color_energy_bar_gui));
		ImGui::PointsBar(info.en / max(info.en_max, 0.001f), { settings.bar_size, 0 }, text.c_str());
		ImGui::PopStyleColor(1);
	}

	ImGui::End();
}
