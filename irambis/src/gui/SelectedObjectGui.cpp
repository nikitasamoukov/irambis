#include "SelectedObjectGui.h"
#include "../Player.h"
#include "../Assets.h"
#include "../components.h"
#include "../common/util.h"
#include "../common/ImGuiExt.h"
#include "../registry.h"
#include "../Inputs.h"
#include "../Events.h"
#include "../Settings.h"
#include "../systems/SystemKill.h"

void ObjectInfoGui(Object object)
{
	EASY_FUNCTION(profiler::colors::Dark);

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(io.DisplaySize.x - 0, 0);
	ImVec2 window_pos_pivot = ImVec2(1.0f, 0.0f);
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::Begin(("Object info:" + assets.GetName(object.GetType())).c_str(), nullptr, window_flags);

	ImGui::Text("Name: %s", assets.GetName(object.GetType()).c_str());

	const BoxedObject* bo = nullptr; // HACK objects with no type???
	if (registry.all_of<TypeC>(object.entity))
	{
		bo = &assets.GetBoxedObject(object.GetType());
	}

	if (registry.all_of<HpC>(object.entity))
	{
		auto& hp_c = registry.get<HpC>(object.entity);
		string text = "Hp: " + ToString(hp_c.hp, 0) + "/" + ToString(hp_c.hp_max, 0);
		ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(color_hp_bar_gui));
		ImGui::PointsBar(hp_c.hp / max(hp_c.hp_max, 0.001f), { settings.bar_size, 0 }, text.c_str());

		ImGui::PopStyleColor(1);
	}

	if (registry.all_of<EnergyC>(object.entity))
	{
		auto& energy_c = registry.get<EnergyC>(object.entity);
		string text = "Energy: " + ToString(energy_c.en, 0) + "/" + ToString(energy_c.en_max, 0);
		ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(color_energy_bar_gui));
		ImGui::PointsBar(energy_c.en / max(energy_c.en_max, 0.001f), { settings.bar_size, 0 }, text.c_str());
		ImGui::PopStyleColor(1);
	}

	if (registry.all_of<StructBuildingC>(object.entity))
	{
		auto& struct_building_c = registry.get<StructBuildingC>(object.entity);
		string text = "Build: " + ToString(struct_building_c.build_progress * 100, 0) + "%";
		ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(color_build_bar_gui));
		ImGui::PointsBar(struct_building_c.build_progress, { settings.bar_size, 0 }, text.c_str());
		ImGui::PopStyleColor(1);
	}

	if (bo && bo->Has<EnemyCDesc>())
	{
		auto& enemy_c_desc = bo->Get<EnemyCDesc>();
		ImGui::Text("Bounty: %s",
			ToString(enemy_c_desc.bounty, 0).c_str()
		);
	}

	if (bo && bo->Has<EnemyC>())
	{
		auto& c = bo->Get<EnemyC>();
		ImGui::Text("Range: %s",
			ToString(c.range, 0).c_str()
		);
		ImGui::Text("Detect range: %s",
			ToString(c.agro_range, 0).c_str()
		);
	}

	if (bo && bo->IsStruct())
	{
		string s = bo->InfoTextObject();
		if (!s.empty())
			ImGui::Text("%s", s.c_str());
	}

	// -----------------------------------DEBUG--------------------------------------------------
	if constexpr (build_type != BuildType::Retail) if (settings.is_show_debug_gui)
	{
		if (registry.all_of<EnergyC>(object.entity))
		{
			auto& energy_c = registry.get<EnergyC>(object.entity);

			if (energy_c.gen > 0)
				ImGui::Text("Energy generation: %s", ToString(energy_c.gen, 1).c_str());
			ImGui::Text("is_consumer: %s", to_string(energy_c.is_consumer).c_str());
		}
		if (registry.all_of<EnemyTargetC>(object.entity))
		{
			auto& enemy_target_c = registry.get<EnemyTargetC>(object.entity);
			ImGui::Text("EnemyTargetC: %s, %s",
				ToString(enemy_target_c.target_pos.x, 1).c_str(),
				ToString(enemy_target_c.target_pos.y, 1).c_str()
			);
		}

		if (registry.all_of<EnemyAttackNowCTag>(object.entity))
		{
			ImGui::Text("EnemyAttackNowCTag");
		}

		if (registry.all_of<StructC>(object.entity))
		{
			auto& struct_c = registry.get<StructC>(object.entity);
			ImGui::Text("StructC GroupId: %s", to_string(struct_c.id.id).c_str());
			ImGui::Text("Energy type: %s", EnergyTypeToString(struct_c.et).c_str());
		}

		if (registry.all_of<ConnectIdC>(object.entity))
		{
			auto& connect_id_c = registry.get<ConnectIdC>(object.entity);
			ImGui::Text("ConnectIdC: is_one_connect: %s", to_string(connect_id_c.is_only_one_connect).c_str());
		}

		if (registry.all_of<ConnectPosC>(object.entity))
		{
			ImGui::Text("ConnectPosC");
		}

		if (registry.all_of<TurretC>(object.entity))
		{
			auto& turret_c = registry.get<TurretC>(object.entity);
			ImGui::Text("TurretC: range:%s", to_string(turret_c.range).c_str());
		}

		if (registry.all_of<TurretLaserC>(object.entity))
		{
			auto& turret_laser_c = registry.get<TurretLaserC>(object.entity);
			ImGui::Text("TurretLaserC: dps: %s", ToString(turret_laser_c.dps, 1).c_str());
			ImGui::Text("en_per_second: %s", ToString(turret_laser_c.en_per_second, 1).c_str());
		}

		if (registry.all_of<TurretHighLaserC>(object.entity))
		{
			auto& turret_high_laser_c = registry.get<TurretHighLaserC>(object.entity);
			ImGui::Text("TurretHighLaserC: dps: %s", ToString(turret_high_laser_c.dps, 1).c_str());
			ImGui::Text("en_per_second: %s", ToString(turret_high_laser_c.en_per_second, 1).c_str());
			ImGui::Text("charge_per_frame: %s", ToString(turret_high_laser_c.charge_per_frame, 3).c_str());
			ImGui::Text("charge: %s", ToString(turret_high_laser_c.charge, 2).c_str());
		}
	}

	ImGui::End();
}

void ObjectUpgradeGui(Object object)
{
	EASY_FUNCTION(profiler::colors::Dark);

	auto& bo = assets.GetBoxedObject(object.GetType());
	if (!bo.IsStruct()) return;

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(io.DisplaySize.x / 2.0f, 0);
	ImVec2 window_pos_pivot = ImVec2(0.5f, 0.0f);

	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::Begin(("Object upgrade" + assets.GetName(object.GetType())).c_str(), nullptr, window_flags);

	if (inputs.keys[GLFW_KEY_X].hit)
	{
		registry.emplace_or_replace<KillCTag>(object.entity);
	}
	// remove button
	{
		Texture tex = assets.GetTexture("remove_icon");
		GLuint tex_id = tex.GetId();
		f32 uv_sz = (10 + 2) / f32(tex.GetSize().x);

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.4f, 0.1f, 0.1f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.6f, 0.2f, 0.2f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.2f, 0.2f, 1.0f });

		if (ImGui::ImageButtonText(0, { (void*)tex_id }, ToString(bo.CostSell(object), 0).c_str(), { settings.buttons_size, settings.buttons_size }, { 0.5f - uv_sz, 0.5f - uv_sz }, { 0.5f + uv_sz, 0.5f + uv_sz }))
		{
			registry.emplace_or_replace<KillCTag>(object.entity);
		}
		ImGui::PopStyleColor(3);

		if (ImGui::IsItemHovered())
		{
			ImGui::BeginTooltip();
			ImGui::Text("Destroy structure\nReward: %s", ToString(bo.CostSell(object), 0).c_str());
			ImGui::EndTooltip();
		}
	}

	if (!registry.all_of<StructBuildingC>(object.entity) && bo.Has<StructUpgradesCDesc>())
	{
		auto& struct_upgrades_c_desc = bo.Get<StructUpgradesCDesc>();

		if (inputs.keys[GLFW_KEY_U].hit && !struct_upgrades_c_desc.upgrades.empty())
		{
			auto up_struct_name = struct_upgrades_c_desc.upgrades[0];
			ETryUpgradeStructure ev{ object, up_struct_name };
			dispatcher.trigger(ev);
		}

		if (inputs.keys[GLFW_KEY_I].hit && struct_upgrades_c_desc.upgrades.size() >= 2)
		{
			auto up_struct_name = struct_upgrades_c_desc.upgrades[1];
			ETryUpgradeStructure ev{ object, up_struct_name };
			dispatcher.trigger(ev);
		}

		for (auto& up_struct_name : struct_upgrades_c_desc.upgrades)
		{
			auto& up_bo = assets.GetBoxedObject(up_struct_name);

			ImGui::SameLine();

			Texture tex = assets.GetTexture(up_bo.Get<RenderC>().id);
			GLuint tex_id = tex.GetId();
			f32 uv_sz = (up_bo.Size() + 2) / f32(tex.GetSize().x);

			if (player.money >= up_bo.Cost())
			{
				ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.1f, 0.4f, 0.1f, 1.0f });
				ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.6f, 0.2f, 1.0f });
				ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.2f, 0.8f, 0.2f, 1.0f });
			}
			else
			{
				ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.3f, 0.3f, 0.3f, 1.0f });
				ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.4f, 0.4f, 0.4f, 1.0f });
				ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.6f, 0.6f, 0.6f, 1.0f });
			}

			vector<ImTextureID> textures;
			textures.push_back((ImTextureID)tex_id);
			if (bo.Has<RenderTurretC>())
			{
				auto& tex_turr = assets.GetTexture(up_bo.Get<RenderTurretC>().id);
				textures.push_back((ImTextureID)tex_turr.GetId());
			}

			if (ImGui::ImageButtonText((int)up_bo.Id().id, textures, ToString(up_bo.Cost(), 0).c_str(), { settings.buttons_size, settings.buttons_size }, { 0.5f - uv_sz, 0.5f - uv_sz }, { 0.5f + uv_sz, 0.5f + uv_sz }))
			{
				ETryUpgradeStructure ev{ object, up_struct_name };
				dispatcher.trigger(ev);
			}
			ImGui::PopStyleColor(3);

			if (ImGui::IsItemHovered())
			{
				ImGui::BeginTooltip();
				ImGui::Text("%s", up_bo.InfoTextBuild().c_str());
				ImGui::EndTooltip();
			}
		}
	}

	if (registry.all_of<KillCTag>(object.entity)) // HACK force kill
	{
		player.money += bo.CostSell(object);
		SystemKillStructKill(object.entity);
	}

	ImGui::End();
}

void SelectedObjectGui(Object object)
{
	if (object.IsNull())
		return;

	ObjectInfoGui(object);
	ObjectUpgradeGui(object);
}
