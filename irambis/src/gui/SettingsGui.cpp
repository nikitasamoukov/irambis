#include "SettingsGui.h"
#include "../common/ImGuiExt.h"
#include "../Settings.h"

bool SettingsGui()
{
	EASY_FUNCTION(profiler::colors::Dark);

	bool is_exit_settings = false;

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(io.DisplaySize.x / 2, io.DisplaySize.y / 2);
	ImVec2 window_pos_pivot = ImVec2(0.5f, 0.5f);
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::Begin("Settings gui", nullptr, window_flags);

	ImGui::Text("Use perfect zoom:");
	ImGui::SameLine();
	ImGui::Checkbox("##settings.is_perfect_zoom", &settings.is_perfect_zoom);

	ImGui::Text("Fullscreen:");
	ImGui::SameLine();
	ImGui::Checkbox("##settings.is_fullscreen", &settings.is_fullscreen);

	if constexpr (build_type != BuildType::Retail)
	{
		ImGui::Text("Debug gui:");
		ImGui::SameLine();
		ImGui::Checkbox("##settings.is_show_debug_gui", &settings.is_show_debug_gui);
	}

	ImGui::AlignCenterButton("Back");
	if (ImGui::Button("Back"))
	{
		is_exit_settings = true;
	}
	ImGui::AlignCenterButtonEnd("Back");

	ImGui::End();

	return is_exit_settings;
}
