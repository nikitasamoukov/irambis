#include "TimeWarpGui.h"
#include "../Settings.h"
#include "../Inputs.h"

void TimeWarpGui()
{
	EASY_FUNCTION(profiler::colors::Dark);
	
	if (inputs.keys[GLFW_KEY_SPACE].hit) settings.OnSpacebar();
	
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImVec2 window_pos = ImVec2(0, 0);
	ImVec2 window_pos_pivot = ImVec2(0.0f, 0.0f);

	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::Begin("Time warp gui", nullptr, window_flags);

	auto base_button_color = ImGui::GetStyleColorVec4(ImGuiCol_Button);
	ImVec4 pressed_button_color{ 0.4f, 0.6f, 0.8f, 1.0f };

	ImGui::PushStyleColor(ImGuiCol_Button, settings.time_speed == Settings::TimeSpeed::Pause ? pressed_button_color : base_button_color);
	if (ImGui::Button("Pause"))
	{
		settings.SetTimeSpeed(Settings::TimeSpeed::Pause);
	}
	ImGui::PopStyleColor(1);

	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, settings.time_speed == Settings::TimeSpeed::Slow ? pressed_button_color : base_button_color);
	if (ImGui::Button("0.5"))
	{
		settings.SetTimeSpeed(Settings::TimeSpeed::Slow);
	}
	ImGui::PopStyleColor(1);

	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, settings.time_speed == Settings::TimeSpeed::Normal ? pressed_button_color : base_button_color);
	if (ImGui::Button("1"))
	{
		settings.SetTimeSpeed(Settings::TimeSpeed::Normal);
	}
	ImGui::PopStyleColor(1);

	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, settings.time_speed == Settings::TimeSpeed::Fast ? pressed_button_color : base_button_color);
	if (ImGui::Button("2"))
	{
		settings.SetTimeSpeed(Settings::TimeSpeed::Fast);
	}
	ImGui::PopStyleColor(1);

	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, settings.time_speed == Settings::TimeSpeed::Max ? pressed_button_color : base_button_color);
	if (ImGui::Button("20"))
	{
		settings.SetTimeSpeed(Settings::TimeSpeed::Max);
	}
	ImGui::PopStyleColor(1);

	ImGui::End();
}
