#include "WavesGui.h"
#include "../Assets.h"
#include "../components.h"
#include "../Levels.h"

WavesGui waves_gui;

void WavesGui::AddWave(BoxedObjectId bo_id, int count)
{
	_waves.push_back({ .bo_id = bo_id, .count = count });
}

void WavesGui::Draw()
{
	EASY_FUNCTION(profiler::colors::Dark);
	if (_waves.empty() && !levels.IsNoWaves()) return;

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(0, io.DisplaySize.y / 2);
	ImVec2 window_pos_pivot = ImVec2(0.0f, 1.0f);
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);

	ImGui::Begin("Waves gui", nullptr, window_flags);
	for (auto& wave : _waves)
	{
		string const& name = assets.GetName(wave.bo_id);

		auto& bo = assets.GetBoxedObject(wave.bo_id);
		if (bo.Has<RenderC>())
		{
			Texture tex = assets.GetTexture(bo.Get<RenderC>().id);
			GLuint tex_id = tex.GetId();
			f32 uv_sz = (bo.Size() + 2) / f32(tex.GetSize().x);
			ImGui::Image((ImTextureID)tex_id, { 24, 24 }, { 0.5f - uv_sz, 0.5f - uv_sz }, { 0.5f + uv_sz, 0.5f + uv_sz });
			ImGui::SameLine();
		}
		ImGui::Text("%s %s", to_string(wave.count).c_str(), name.c_str());
	}

	if (_waves.empty() && levels.IsNoWaves())
		ImGui::Text("End of waves");

	ImGui::End();
}

void WavesGui::Update()
{
	for (auto& wave : _waves)
	{
		wave.lifetime--;
	}
	if (!_waves.empty() && _waves.front().lifetime <= 0)
		_waves.erase(_waves.begin());
}
