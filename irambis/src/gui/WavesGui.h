#pragma once
#include "../pch.h"
#include "../BoxedObject.h"
#include "../common/util.h"

class WavesGui
{
public:
	void AddWave(BoxedObjectId bo_id, int count);
	void Draw();
	void Update();
	void Clear() { _waves.clear(); }
private:
	struct GWave
	{
		BoxedObjectId bo_id;
		int count = 0;
		Frames lifetime = (Frames)(frames_per_second * 10);
	};

	vector<GWave> _waves;
};

extern WavesGui waves_gui;