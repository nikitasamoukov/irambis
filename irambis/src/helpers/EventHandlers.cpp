#include "EventHandlers.h"
#include "../Events.h"
#include "../registry.h"
#include "../Assets.h"
#include "../Player.h"
#include "HelpersMouse.h"
#include "HelpersStruct.h"

// HACK macros
#define HANDLER_DEF(event_name) void Handler ## event_name(event_name const& ev)
#define HANDLER_REG(event_name) dispatcher.sink<event_name>().connect<Handler ## event_name>()

HANDLER_DEF(EShoot)
{
	Object obj = CreateObjectFull(ev.id);
	obj.SetTrs(ev.trs);
	if (registry.all_of<BulletC>(obj.entity))
	{
		auto& bullet_c = registry.get<BulletC>(obj.entity);
		bullet_c.lifetime = ev.range * frames_per_second / max(bullet_c.speed, 0.001f);
	}
}

HANDLER_DEF(ESpawnEnemy)
{
	Object obj = SpawnEnemy(ev.id, ev.trs);
	if (registry.all_of<PhysC>(obj.entity))
	{
		auto& phys_c = registry.get<PhysC>(obj.entity);
		phys_c.vel = vec2(cos(ev.trs.angle) * ev.speed, sin(ev.trs.angle) * ev.speed);
	}
}

HANDLER_DEF(ETryUpgradeStructure)
{
	Object obj = ev.obj;
	if (obj.IsNull()) return;
	auto& bo = assets.GetBoxedObject(obj.GetType());
	if (!bo.Has<StructUpgradesCDesc>()) return;
	if (!bo.IsStruct()) return;
	auto& upgrades = bo.Get<StructUpgradesCDesc>().upgrades;
	auto it = find(upgrades.begin(), upgrades.end(), ev.upgrade_name);
	if (it == upgrades.end()) return;

	auto& up_bo = assets.GetBoxedObject(ev.upgrade_name);

	if (player.money >= up_bo.Cost())
	{
		player.money -= up_bo.Cost();

		bo.UpgradeTo(up_bo, obj.entity);
	}
}

HANDLER_DEF(ETryBuildStructure)
{
	auto& bo = assets.GetBoxedObject(ev.id);

	if (player.money < bo.Cost()) return;
	vec2 strust_pos = ConvertMousePosToStructPos(ev.pos);

	Object obj;

	if (is_sandbox)
		obj = PlaceStructFull(strust_pos, ev.id);
	else
		obj = PlaceStruct(strust_pos, ev.id);

	if (!obj.IsNull())
	{
		player.money -= bo.Cost();
		player.selected_obj = obj;
	}
}

void RegisterHandlers()
{
	HANDLER_REG(EShoot);
	HANDLER_REG(ESpawnEnemy);
	HANDLER_REG(ETryUpgradeStructure);
	HANDLER_REG(ETryBuildStructure);
}
