#include "HelpersEnemy.h"
#include "../registry.h"
#include "../components.h"

void EnemyRemoveTarget(Object obj)
{
	registry.remove_if_exists<EnemyTargetC>(obj.entity);
	registry.remove_if_exists<EnemyAttackNowCTag>(obj.entity);
}
