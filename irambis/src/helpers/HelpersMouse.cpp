#include "HelpersMouse.h"
#include "../Inputs.h"
#include "../Camera.h"

vec2 ConvertMousePosToGame(vec2 pos_screen)
{
	return (pos_screen - vec2(inputs.screen.size) / 2.0f)
		* vec2(1, -1)
		* (2.0f / max(inputs.screen.size.y, 1))
		* camera.pos.zoom + camera.pos.pos;
}

vec2 ConvertMousePosToStructPos(vec2 pos_screen)
{
	vec2 pos_float = ConvertMousePosToGame(pos_screen);
	pos_float += 0.5;
	pos_float = floor(pos_float);
	return pos_float;
}

vec2 ConvertMousePosToGameReq(vec2 pos_screen) // HACK 100% accuray zoom
{
	return (pos_screen - vec2(inputs.screen.size) / 2.0f)
		* vec2(1, -1)
		* (2.0f / max(inputs.screen.size.y, 1))
		* camera.required_pos.zoom + camera.required_pos.pos;
}

bool IsMouseOnGui()
{
	ImGuiIO& io = ImGui::GetIO();
	return io.WantCaptureMouse;
}

bool IsKeyboardOnGui()
{
	ImGuiIO& io = ImGui::GetIO();
	return io.WantCaptureKeyboard;
}