#pragma once
#include "../pch.h"

vec2 ConvertMousePosToGame(vec2 pos_screen);
vec2 ConvertMousePosToStructPos(vec2 pos_screen);

vec2 ConvertMousePosToGameReq(vec2 pos_screen);

bool IsMouseOnGui();
bool IsKeyboardOnGui();
