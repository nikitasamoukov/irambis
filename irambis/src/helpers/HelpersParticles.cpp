#include "HelpersParticles.h"
#include "../BoxedObject.h"
#include "../components.h"
#include "../Assets.h"
#include "../registry.h"

entt::entity SpawnParticle(TrsC const& trs, RenderParticleC const& render_particle_c)
{
	auto entity = registry.create();
	registry.emplace<TrsC>(entity, trs);
	registry.emplace<RenderParticleC>(entity, render_particle_c);
	registry.emplace<ParticleLifetimeC>(entity, ParticleLifetimeC{ 1, 1.0f / (1.5 * frames_per_second) });
	//registry.emplace<RenderC>(entity, RenderC{ .id = assets.GetTextureId("particle") });

	return entity;
}

void SpawnTDP(TrsC trs, DestructionParticles const& dp, float speed_anim, vec2 base_speed)
{
	speed_anim = max(0.0001f, speed_anim);
	// HACK create new TrsC because old is UB after create new TrsC
	static mt19937 rg{ 1 };
	mat3 mat(1);
	mat = glm::translate(mat, trs.pos);
	mat = glm::rotate(mat, trs.angle);

	for (auto p_info : dp.particles)
	{
		f32 rand_angle = glm::radians((int(rg() % 2001) - 1000) / 1000.0f * 10);
		f32 rand_vel = (int(rg() % 401) + 800) / 1000.0f;
		f32 rand_decay = (int(rg() % 601) + 600) / 1000.0f;

		TrsC trs_p{ .pos = mat * vec3(p_info.pos, 1), .angle = trs.angle + rand_angle };
		auto entity = SpawnParticle(trs_p, p_info.render_particle_c);

		PhysC phys_c;
		phys_c.vel = p_info.pos * (0.8f * seconds_in_frame / 1.5f) * rand_vel * speed_anim;
		phys_c.vel = mat * vec3(phys_c.vel, 0);
		phys_c.vel += base_speed;

		registry.emplace<PhysC>(entity, phys_c);

		registry.get<ParticleLifetimeC>(entity).frame_decay *= rand_decay * speed_anim;
	}
}

void SpawnParticlesOnObjectDead(TrsC const& trs, TextureId tex_id, float speed_anim, vec2 base_speed)
{
	if (assets.HasTextureDestruction(tex_id))
	{
		auto& td = assets.GetTextureDestruction(tex_id);
		SpawnTDP(TrsC(trs), td, speed_anim, base_speed);
	}
}
