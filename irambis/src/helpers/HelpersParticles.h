#pragma once
#include "../pch.h"
#include "../common/IdTypes.h"

struct TrsC;
class BoxedObject;

void SpawnParticlesOnObjectDead(TrsC const& trs, TextureId tex_id, float speed_anim = 1, vec2 base_speed = vec2{ 0, 0 });
