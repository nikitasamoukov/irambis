#include "HelpersStruct.h"
#include "Query.h"
#include "../Player.h"
#include "../components.h"
#include "../Assets.h"
#include "../registry.h"

void ConnectStruct(Object object)
{
	auto possible_connections = GetPossibleConnections(object);

	sort(possible_connections.begin(), possible_connections.end(), [](const auto& a, const auto& b) {return a.dist < b.dist; });

	for (auto& possible_connection : possible_connections)
	{
		ConnectStructs(object, possible_connection.obj);
	}
}

Object PlaceStructFull(vec2 struct_pos, BoxedObjectId id, bool is_connect)
{
	if (!CanPlaceObject(struct_pos, id))
		return {};
	Object object = CreateObjectFull(id);
	object.SetTrs({ struct_pos, 0 });

	if (is_connect)
		ConnectStruct(object);
	AddStructToGrid(object, struct_pos);

	return object;
}

// Create struct building 
Object PlaceStruct(vec2 struct_pos, BoxedObjectId id)
{
	if (!CanPlaceObject(struct_pos, id))
		return {};

	Object object = CreateObjectBuild(*player.selected_building_to_place);
	object.SetTrs({ struct_pos, 0 });

	ConnectStruct(object);
	AddStructToGrid(object, struct_pos);

	return object;
}

void StructPerformBuildEnd(Object obj)
{
	auto bo_id = obj.GetType();
	auto& bo = assets.GetBoxedObject(bo_id);
	bo.PlaceToPartReady(obj.entity);
	registry.remove_if_exists<StructBuildingC>(obj.entity);
	if (registry.all_of<EnergySaveWhenUpgradeC>(obj.entity) && registry.all_of<EnergyC>(obj.entity))
	{
		auto& energy_c = registry.get<EnergyC>(obj.entity);
		f32 en_saved = registry.get<EnergySaveWhenUpgradeC>(obj.entity).en;
		energy_c.en = clamp(energy_c.en + en_saved, 0.0f, energy_c.en_max);
	}
}
