#pragma once
#include "../pch.h"
#include "Object.h"

void ConnectStruct(Object object);

Object PlaceStructFull(vec2 struct_pos, BoxedObjectId id, bool is_connect = true);
Object PlaceStruct(vec2 struct_pos, BoxedObjectId id);
void StructPerformBuildEnd(Object obj);