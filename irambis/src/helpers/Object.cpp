#include "Object.h"
#include "../registry.h"
#include "../components.h"
#include "../Assets.h"
#include "Query.h"

void Object::SetTrs(const TrsC& trs)
{
	if (IsNull()) return;
	if (!registry.all_of<TrsC>(entity)) return;

	auto& trs_c = registry.get<TrsC>(entity);

	trs_c = trs;
}

TrsC& Object::GetTrs()
{
	Assert(!IsNull());
	Assert(registry.all_of<TrsC>(entity));

	return registry.get<TrsC>(entity);
}

void Object::SetTrsWithRand(const TrsC& trs, i32 size, mt19937& rg)
{
	Assert(size >= 0);

	auto trs_rand = trs;
	trs_rand.pos += vec2{ i32(rg() % (size * 2 + 1)) - size, i32(rg() % (size * 2 + 1)) - size };
	trs_rand.angle = glm::radians(f32(rg() % 360));

	SetTrs(trs_rand);
}

void Object::Damage(const DmgC& dmg)
{
	if (IsNull()) return;
	if (!registry.all_of<HpC>(entity)) return;

	auto& hp_c = registry.get<HpC>(entity);
	hp_c.hp -= dmg.dmg;

	if (hp_c.hp <= 0)
	{
		hp_c.hp = 0;
		registry.emplace_or_replace<KillCTag>(entity);
	}
}

bool Object::IsNull()
{
	if (!registry.valid(entity))
	{
		entity = entt::null;
		return true;
	}
	return false;
}

bool Object::IsNull() const
{
	return !registry.valid(entity);
}

void Object::RemoveAllConnects()
{
	if (IsNull()) return;
	if (!registry.all_of<ConnectIdC>(entity)) return;

	auto& connect_id_c = registry.get<ConnectIdC>(entity);

	for (auto& connect_obj : connect_id_c.connects)
	{
		if (connect_obj.IsNull()) continue;

		auto& other_obj_connect_id_c = registry.get<ConnectIdC>(connect_obj.entity);
		auto& other_obj_connect_pos_c = registry.get<ConnectPosC>(connect_obj.entity);
		auto it = std::find(other_obj_connect_id_c.connects.begin(), other_obj_connect_id_c.connects.end(), *this);
		Assert(it != other_obj_connect_id_c.connects.end());

		size_t index = std::distance(other_obj_connect_id_c.connects.begin(), it);
		it->entity = entt::null;
		other_obj_connect_pos_c.connects[index] = {};

	}
}

BoxedObjectId Object::GetType()
{
	if (!registry.valid(entity))
	{
		entity = entt::null;
		return { (u64)-1 };
	}
	if (!registry.all_of<TypeC>(entity))
		return { (u64)-1 };
	return registry.get<TypeC>(entity).id;
}

Object CreateObjectBuild(BoxedObjectId id)
{
	Object obj = { assets.GetBoxedObject(id).CreateBuildOnly() };
	if (registry.all_of<HpC>(obj.entity) && registry.all_of<StructBuildingC>(obj.entity))
	{
		auto& hp_c = registry.get<HpC>(obj.entity);
		auto& struct_building_c = registry.get<StructBuildingC>(obj.entity);

		hp_c.hp *= 0.01f;
		struct_building_c.hp_build = hp_c.hp_max;
	}
	return obj;
}

Object CreateObjectFull(BoxedObjectId id)
{
	auto obj = assets.GetBoxedObject(id).CreateFull();
	return { obj };
}

bool ConnectStructs(Object o1, Object o2)
{
	if (o1.IsNull() | o2.IsNull()) return false;
	if (o1.entity == o2.entity) return false;
	if (!CanCreateConnect(o1, o2)) return false;
	if (!registry.all_of<StructC>(o1.entity) | !registry.all_of<StructC>(o2.entity)) return false;

	auto& o1_connects_pos_c = registry.get<ConnectPosC>(o1.entity); // HACK really spagetti
	auto& o1_connects_id_c = registry.get<ConnectIdC>(o1.entity);

	auto& o2_connects_pos_c = registry.get<ConnectPosC>(o2.entity);
	auto& o2_connects_id_c = registry.get<ConnectIdC>(o2.entity);

	u64 o1_empty_socket = o1_connects_id_c.FindEmpty();
	if (o1_empty_socket == o1_connects_id_c.connects.size()) return false;
	u64 o2_empty_socket = o2_connects_id_c.FindEmpty();
	if (o2_empty_socket == o2_connects_id_c.connects.size()) return false;

	auto& o1_trs_c = registry.get<TrsC>(o1.entity);
	auto& o2_trs_c = registry.get<TrsC>(o2.entity);

	vec2i diff = o2_trs_c.pos - o1_trs_c.pos;

	o1_connects_pos_c.connects[o1_empty_socket].x = diff.x;
	o1_connects_pos_c.connects[o1_empty_socket].y = diff.y;
	o1_connects_id_c.connects[o1_empty_socket] = o2;

	o2_connects_pos_c.connects[o2_empty_socket].x = -diff.x;
	o2_connects_pos_c.connects[o2_empty_socket].y = -diff.y;
	o2_connects_id_c.connects[o2_empty_socket] = o1;

	return true;
}

void SpawnEnemy(BoxedObjectId id, vec2 pos, float rand_range, u64 count)
{
	mt19937 rg(1);
	for (u64 i = 0; i < count; i++)
	{
		auto obj = CreateObjectFull(id);
		obj.SetTrsWithRand({ pos, 0 }, rand_range, rg);
		AddEnemyToGrid(obj, obj.GetTrs().pos);
	}
}

Object SpawnEnemy(BoxedObjectId id, const TrsC& trs)
{
	auto obj = CreateObjectFull(id);
	obj.SetTrs(trs);
	AddEnemyToGrid(obj, obj.GetTrs().pos);
	return obj;
}
