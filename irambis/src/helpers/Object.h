#pragma once
#include "../pch.h"
#include "../common/IdTypes.h"

struct DmgC;
struct TrsC;

// wrapper to entt::entity. can be null.
struct Object
{
	entt::entity entity = entt::null;

	void SetTrs(const TrsC& trs);
	TrsC& GetTrs();
	void SetTrsWithRand(const TrsC& trs, i32 size, mt19937& rg);
	void Damage(const DmgC& dmg);
	bool IsNull();
	bool IsNull() const;
	void RemoveAllConnects();
	BoxedObjectId GetType();

	auto operator<=>(const Object&)const = default;
};

namespace std
{
	template<>
	struct std::hash<Object>
	{
		std::size_t operator()(Object const& s) const noexcept
		{
			return std::hash<u64>()((u64)s.entity);
		}
	};
}

Object CreateObjectBuild(BoxedObjectId id);
Object CreateObjectFull(BoxedObjectId id);

bool ConnectStructs(Object o1, Object o2);

void SpawnEnemy(BoxedObjectId id, vec2 pos, float rand_range, u64 count);
Object SpawnEnemy(BoxedObjectId id, const TrsC& trs);
