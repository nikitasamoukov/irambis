#include "Query.h"
#include "../registry.h"
#include "../components.h"
#include "../Assets.h"
#include "../helpers/Object.h"

#include <glm/gtx/intersect.hpp>
#include "../Render.h"
#include "../Inputs.h"
#include "../Camera.h"


struct GridCell
{
	tsl::sparse_set<Object> objects;
};

tsl::sparse_map<vec2i, GridCell> grid_structs;
tsl::sparse_map<vec2i, GridCell> grid_enemies;

vec2i GridPosFromObjPos(vec2 pos)
{
	static_assert(grid_cell_size != 0);
	return floor(pos / grid_cell_size);
}

void AddStructToGrid(Object obj, vec2 pos)
{
	vec2i grid_pos = GridPosFromObjPos(pos);
	auto& cell_objects = grid_structs[grid_pos].objects;
	Assert(cell_objects.find(obj) == cell_objects.end());
	cell_objects.emplace(obj);
}

void RemStructFromGrid(Object obj, vec2 pos)
{
	vec2i grid_pos = GridPosFromObjPos(pos);
	Assert(grid_structs.find(grid_pos) != grid_structs.end());
	auto& cell_objects = grid_structs[grid_pos].objects;
	cell_objects.erase(obj);
	if (cell_objects.empty())
	{
		grid_structs.erase(grid_pos);
	}
}

bool IsDiffGridCell(vec2 pos1, vec2 pos2)
{
	return GridPosFromObjPos(pos1) != GridPosFromObjPos(pos2);
}

void AddEnemyToGrid(Object obj, vec2 pos)
{
	vec2i grid_pos = GridPosFromObjPos(pos);
	auto& cell_objects = grid_enemies[grid_pos].objects;
	Assert(cell_objects.find(obj) == cell_objects.end());
	cell_objects.emplace(obj);
}

void RemEnemyFromGrid(Object obj, vec2 pos)
{
	vec2i grid_pos = GridPosFromObjPos(pos);
	Assert(grid_enemies.find(grid_pos) != grid_enemies.end());
	auto& cell_objects = grid_enemies[grid_pos].objects;
	cell_objects.erase(obj);
	if (cell_objects.empty())
	{
		grid_enemies.erase(grid_pos);
	}
}

void ProcessEnemyMove(Object obj, vec2 pos1, vec2 pos2)
{
	if (!IsDiffGridCell(pos1, pos2)) return;
	RemEnemyFromGrid(obj, pos1);
	AddEnemyToGrid(obj, pos2);
}

void ClearGrids()
{
	grid_enemies.clear();
	grid_structs.clear();
}

Object GetObjectOnClick(vec2 pos)
{
	Object res = { entt::null };
	f32 res_dist = 99999999999;

	f32 add_size = 10 * (camera.pos.zoom / (float)max(1, inputs.screen.size.y) * 2);

	auto view = registry.view<TrsC, PhysC>();
	for (auto entity : view)
	{
		auto& trs_c = view.get<TrsC>(entity);
		auto& phys_c = view.get<PhysC>(entity);

		f32 dist = glm::distance(pos, trs_c.pos);
		if (dist <= phys_c.r + add_size && dist < res_dist)
		{
			res = { entity };
			res_dist = dist;
		}
	}

	return res;
	// TODO QuadMap
}

bool IsIntersectLineCircle(vec2 line1, vec2 line2, vec2 pos, float r)
{
	vec2 d = line1 - line2;
	vec2 f = line2 - pos;

	float a = dot(d, d);
	float b = 2 * dot(f, d);
	float c = dot(f, f) - r * r;

	float discriminant = b * b - 4 * a * c;
	if (discriminant < 0)
		return false;

	discriminant = sqrt(discriminant);

	float t1 = (-b - discriminant) / (2 * a);
	float t2 = (-b + discriminant) / (2 * a);

	if (t1 >= 0 && t1 <= 1)
	{
		return true;
	}

	if (t2 >= 0 && t2 <= 1)
	{
		return true;
	}
	return false;
}

bool CanPlaceObject(vec2 pos, BoxedObjectId id)
{
	auto& bo = assets.GetBoxedObject(id);

	auto view = registry.view<TrsC, PhysC, StructC>();
	for (auto entity : view)
	{
		auto& trs_c = view.get<TrsC>(entity);
		auto& phys_c = view.get<PhysC>(entity);

		float dist = glm::distance(pos, trs_c.pos);

		if (dist < phys_c.r + bo.Size() + struct_spacing)
		{
			return false;
		}

		if (dist < connect_dist && registry.all_of<ConnectPosC>(entity))
		{
			auto& connect_pos_c = registry.get<ConnectPosC>(entity);

			for (auto& connect_pos : connect_pos_c.connects)
			{
				if (connect_pos.IsValid()) // HACK need connects counter
				{
					vec2 line1 = trs_c.pos;
					vec2 line2 = trs_c.pos + vec2{ connect_pos.x, connect_pos.y };
					if (IsIntersectLineCircle(
						line1, line2,
						pos, bo.Size() + struct_spacing + connect_width
					))
						return false;
				}
			}
		}
	}

	return true;
}

bool CanCreateConnect(Object o1, Object o2)
{
	vec2 line1 = o1.GetTrs().pos;
	vec2 line2 = o2.GetTrs().pos;

	auto view = registry.view<TrsC, PhysC, StructC>();
	for (auto entity : view)
	{
		if ((entity == o1.entity) | (entity == o2.entity))
			continue;

		auto& trs_c = view.get<TrsC>(entity);
		auto& phys_c = view.get<PhysC>(entity);

		if (IsIntersectLineCircle(
			line1, line2,
			trs_c.pos, phys_c.r + struct_spacing + connect_width
		))
			return false;
	}

	return true;
}

vector<PossibleConnectStr> GetPossibleConnections(Object obj)
{
	vector<PossibleConnectStr> res;

	const auto& obj_trs = obj.GetTrs();

	bool obj_is_only_one_connect = registry.get<ConnectIdC>(obj.entity).is_only_one_connect;

	auto view = registry.view<TrsC, StructC>();
	for (auto entity : view)
	{
		if (entity == obj.entity)
			continue;
		auto& trs_c = view.get<TrsC>(entity);
		float dist = distance(trs_c.pos, obj_trs.pos);
		if (dist < connect_dist)
		{
			if (CanCreateConnect(obj, { entity }))
			{
				auto& connect_id_c = registry.get<ConnectIdC>(entity);
				if (!(obj_is_only_one_connect && connect_id_c.is_only_one_connect) &&
					(connect_id_c.FindEmpty() != connect_id_c.connects.size()))
					res.push_back({ Object{ entity }, dist });
			}
		}
	}

	sort(res.begin(), res.end(), [](const auto& a, const auto& b) {return a.dist < b.dist; });
	if (obj_is_only_one_connect)
	{
		res.resize(min<u64>(1, res.size()));
	}
	else
	{
		res.resize(min<u64>(max_connects, res.size()));
	}

	return res;
}

vector<PossibleConnectStr> GetPossibleConnectionsPredict(vec2 pos, BoxedObjectId id)
{
	auto tmp_obj = CreateObjectFull(id); // HACK temp object
	tmp_obj.SetTrs({ pos, 0 });

	auto res = GetPossibleConnections(tmp_obj);

	registry.destroy(tmp_obj.entity);

	return res;
}

void QueryEnemies(vec2 pos, f32 range, function<void(Object obj)> func)
{
	Assert(range >= 0);
	vec2i grid_pos_min = GridPosFromObjPos(pos - vec2(range));
	vec2i grid_pos_max = GridPosFromObjPos(pos + vec2(range));

	for (i32 x = grid_pos_min.x; x <= grid_pos_max.x; x++)
		for (i32 y = grid_pos_min.y; y <= grid_pos_max.y; y++)
		{
			auto it = grid_enemies.find(vec2i(x, y));
			if (it != grid_enemies.end())
			{
				for (auto obj : it->second.objects)
				{
					func(obj);
				}
			}
		}
}

void QueryStructs(vec2 pos, f32 range, function<void(Object obj)> func)
{
	Assert(range >= 0);
	vec2i grid_pos_min = GridPosFromObjPos(pos - vec2(range));
	vec2i grid_pos_max = GridPosFromObjPos(pos + vec2(range));

	for (i32 x = grid_pos_min.x; x <= grid_pos_max.x; x++)
		for (i32 y = grid_pos_min.y; y <= grid_pos_max.y; y++)
		{
			auto it = grid_structs.find(vec2i(x, y));
			if (it != grid_structs.end())
			{
				for (auto obj : it->second.objects)
				{
					func(obj);
				}
			}
		}
}

void TargetToNearStructure(Object obj)
{
	if (obj.IsNull()) return;
	if (!registry.all_of<EnemyC>(obj.entity)) return;
	if (!registry.all_of<TrsC>(obj.entity)) return;

	auto& enemy_c = registry.get<EnemyC>(obj.entity);
	TrsC my_pos = registry.get<TrsC>(obj.entity);

	float best_target_dist = 99999999.9f; // HACK code duplicate
	entt::entity best_target_entity = entt::null;
	vec2 best_target_pos;

	QueryStructs(my_pos.pos, enemy_c.agro_range, [&](Object struct_obj) {
		auto& trs_c = struct_obj.GetTrs();

		//render.AddLaserToDraw({ my_pos.pos, trs_c.pos, vec3(1, 1, 1), 1 });

		if (glm::distance(trs_c.pos, my_pos.pos) < best_target_dist)
		{
			best_target_pos = trs_c.pos;
			best_target_dist = glm::distance(trs_c.pos, my_pos.pos);
			best_target_entity = struct_obj.entity;
		}
		});

	if (best_target_entity != entt::null && best_target_dist <= enemy_c.agro_range)
	{
		EnemyTargetC enemy_target_c{ {best_target_entity}, best_target_pos };
		registry.emplace_or_replace<EnemyTargetC>(obj.entity, enemy_target_c);
		registry.remove_if_exists<EnemyAttackNowCTag>(obj.entity);
		if (best_target_dist <= enemy_c.range)
		{
			registry.emplace_or_replace<EnemyAttackNowCTag>(obj.entity);
		}
		else
		{
			registry.remove_if_exists<EnemyAttackNowCTag>(obj.entity);
		}
	}
}

void TargetToEnemyInRange(Object obj, float range, bool is_high_hp_aim)
{
	if (!registry.valid(obj.entity)) return;
	if (!registry.all_of<TrsC>(obj.entity)) return;

	TrsC my_pos = registry.get<TrsC>(obj.entity);

	float best_target_score = -99999999.9f;
	entt::entity best_target_entity = entt::null;

	QueryEnemies(my_pos.pos, range, [&](Object enemy_obj) {
		auto& trs_c = enemy_obj.GetTrs();
		f32 dist = glm::distance(trs_c.pos, my_pos.pos);
		f32 target_score = -dist;

		if (is_high_hp_aim)
		{
			if (!registry.all_of<HpC>(enemy_obj.entity))
			{
				target_score = -999999;
			}
			else
			{
				target_score = registry.get<HpC>(enemy_obj.entity).hp;
			}
		}

		if (target_score > best_target_score && dist <= range)
		{
			best_target_score = target_score;
			best_target_entity = enemy_obj.entity;
		}
		});

	if (best_target_entity != entt::null)
	{
		TurretTargetC enemy_target_c{ best_target_entity };
		registry.emplace_or_replace<TurretTargetC>(obj.entity, enemy_target_c);
	}
}


