#pragma once
#include "../pch.h"
#include "Object.h"

Object GetObjectOnClick(vec2 pos);
bool CanPlaceObject(vec2 pos, BoxedObjectId id);

bool CanCreateConnect(Object o1, Object o2);

struct PossibleConnectStr
{
	Object obj;
	f32 dist = 0;
};

vector<PossibleConnectStr> GetPossibleConnections(Object obj);
vector<PossibleConnectStr> GetPossibleConnectionsPredict(vec2 pos, BoxedObjectId id);

void QueryEnemies(vec2 pos, f32 range, function<void(Object obj)> func);
void QueryStructs(vec2 pos, f32 range, function<void(Object obj)> func);

void TargetToNearStructure(Object obj);
void TargetToEnemyInRange(Object obj, float range, bool is_high_hp_aim = false);

vec2i GridPosFromObjPos(vec2 pos);
void AddStructToGrid(Object obj, vec2 pos);
void RemStructFromGrid(Object obj, vec2 pos);
bool IsDiffGridCell(vec2 pos1, vec2 pos2);

void AddEnemyToGrid(Object obj, vec2 pos);
void RemEnemyFromGrid(Object obj, vec2 pos);
void ProcessEnemyMove(Object obj, vec2 pos1, vec2 pos2);

void ClearGrids();