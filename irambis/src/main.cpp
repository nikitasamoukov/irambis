﻿#include "pch.h"
#include "Core.h"
#include "registry.h"
#include "common/MemoryWatcher.h"

int main()
{
	if constexpr (build_type != BuildType::Retail) {
		EASY_PROFILER_ENABLE;
		profiler::startListen();
		EASY_MAIN_THREAD;
		MemoryWatcher::get().init();
	}

	core.Run();

	return 0;
}


/*

Global vars:

registry
dispatcher
inputs
assets
camera
player
render
levels
core

 */