#include "pch.h"

void AssertFailureImpl(const char* file, int line, const char* function, const string& msg)
{
	string s = msg;
	while (s.length() < 70)
	{
		s += ' ';
	}
	s = s + file + " (line " + std::to_string(line) + ")";
	while (s.length() < 90)
	{
		s += ' ';
	}
	s = s + file;
	std::puts(msg.c_str());
	__debugbreak();
	std::terminate();
}