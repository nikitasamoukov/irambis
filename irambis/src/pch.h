#pragma once

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)  

#define NOGDI
#define NOMINMAX
#ifndef _DEBUG
#define NDEBUG 1
#endif

#define WIN32_LEAN_AND_MEAN
#include <windows.h>


#define GLFW_INCLUDE_NONE

#define GLEW_STATIC
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <stb/stb_image.h>

#ifndef RETAIL
#define USING_EASY_PROFILER
#endif
#include "easy/profiler.h"

#pragma comment (lib, "glfw3.lib")
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glu32.lib")
#pragma comment (lib, "pugixml.lib")
#ifndef RETAIL
#pragma comment (lib, "easy_profiler.lib")
#endif



#include <iostream>
#include <vector>
#include <filesystem>
#include <chrono>
#include <thread>
#include <any>
#include <random>
#include <optional>
#include <sstream>
#include <numeric>
#include <variant>
#include <queue>

#define GLM_FORCE_CTOR_INIT
#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include <glm/gtx/fast_trigonometry.hpp>

#include <easy/profiler.h>

#include <entt/entt.hpp>

#include <pugixml/pugixml.hpp>

#include <tsl/sparse_map.h>
#include <tsl/sparse_set.h>


namespace fs = std::filesystem;
using namespace std;
using pugi::xml_node;
using pugi::xml_attribute;
using pugi::xml_document;


using f32 = float;

using u8 = uint8_t;
using i8 = int8_t;

using u16 = uint16_t;
using i16 = int16_t;

using u32 = uint32_t;
using i32 = int32_t;

using u64 = uint64_t;
using i64 = int64_t;



using glm::vec4;
using glm::vec3;
using glm::vec2;
using glm::mat3;
using glm::mat4;
using glm::quat;
using vec2i = glm::vec<2, int>;
using vec2d = glm::vec<2, double>;

namespace std
{
	template<>
	struct std::hash<vec2i>
	{
		std::size_t operator()(vec2i const& s) const noexcept
		{
			return std::hash<u64>()((u64)s.x + (u64)s.y * (u64)1234);
		}
	};
}



void AssertFailureImpl(const char* file, int line, const char* function, const std::string& msg);
#define Assert(ex)  (void)(                                                   \
            (!!(ex)) ||                                                       \
            (AssertFailureImpl(__FILENAME__, __LINE__, __FUNCTION__, #ex), 0) \
        )
#define AssertFail()  (void)(                                                            \
                                                                              \
            (AssertFailureImpl(__FILENAME__, __LINE__, __FUNCTION__, "<no message>"), 0) \
        )
#define AssertMsg(ex, msg)  (void)(                                             \
            (!!(ex)) ||                                                         \
            (AssertFailureImpl(__FILENAME__, __LINE__, __FUNCTION__, (msg)), 0) \
        )
#define AssertMsgFail(msg)  (void)(                                             \
                                                                     \
            (AssertFailureImpl(__FILENAME__, __LINE__, __FUNCTION__, (msg)), 0) \
        )

#define NO_COPY_NO_MOVE_CLASS_DEF(class_name) \
	class_name(const class_name&) = delete;\
	class_name(class_name&&) = delete;\
	class_name& operator =(const class_name&) = delete;\
	class_name& operator =(class_name&&) = delete

#include "ImGui/imgui.h"

template<typename T, typename TAG_T>
struct Id
{
	T id = { T(-1) };
	auto operator<=>(const Id&) const = default;
};

namespace std
{
	template<typename T, typename TAG_T>
	struct std::hash<Id<T, TAG_T>>
	{
		std::size_t operator()(Id<T, TAG_T> const& s) const noexcept
		{
			return std::hash<T>()(s.id);
		}
	};
}

#include "game_config.h"

#undef assert
#define assert Assert