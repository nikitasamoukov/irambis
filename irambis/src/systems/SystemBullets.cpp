#include "SystemBullets.h"
#include "../registry.h"
#include "../components.h"
#include "../common/util.h"
#include "../helpers/HelpersParticles.h"

void SystemBullets()
{
	EASY_FUNCTION(profiler::colors::Dark);

	for (auto entity : registry.view<BulletC>())
	{
		auto& bullet_c = registry.get<BulletC>(entity);
		bullet_c.lifetime -= 1;

		if (bullet_c.lifetime <= 0)
		{
			if (registry.all_of<TrsC>(entity) && registry.all_of<RenderC>(entity) && registry.all_of<PhysC>(entity))
				SpawnParticlesOnObjectDead(registry.get<TrsC>(entity), registry.get<RenderC>(entity).id, 8, registry.get<PhysC>(entity).vel);
			registry.destroy(entity);
		}
	}
}
