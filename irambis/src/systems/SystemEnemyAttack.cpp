#include "SystemEnemyAttack.h"
#include "../registry.h"
#include "../components.h"
#include "../Render.h"
#include "../helpers/Query.h"
#include "../Events.h"
#include "../helpers/HelpersEnemy.h"

void DoShoot(EnemyC& enemy_c, EnemyShootingC& enemy_shooting_c, TrsC& trs_c, EnemyTargetC& enemy_target_c)
{
	auto& target_trs = enemy_target_c.target.GetTrs();

	EShoot ev{ .range = enemy_c.range, .trs = trs_c, .id = enemy_shooting_c.shoot_obj_id };
	dispatcher.enqueue(ev);
}

void SystemEnemyTargeting()
{
	EASY_FUNCTION(profiler::colors::DarkRed);
	for (auto entity : registry.view<EnemyC>())
	{
		TargetToNearStructure(Object{ entity });
	}
}

void SystemEnemyDoShoots()
{
	EASY_FUNCTION(profiler::colors::DarkRed);

	for (auto entity : registry.view<EnemyC, EnemyShootingC, EnemyTargetC, EnemyAttackNowCTag, TrsC>())
	{
		auto&& [enemy_c, enemy_shooting_c, enemy_target_c, trs_c] = registry.get<EnemyC, EnemyShootingC, EnemyTargetC, TrsC>(entity);
		if (enemy_shooting_c.cooldown < 1)
		{
			enemy_shooting_c.cooldown += enemy_shooting_c.shoot_cooldown;
			DoShoot(enemy_c, enemy_shooting_c, trs_c, enemy_target_c);
		}
	}
}

void SystemEnemyCooldown()
{
	EASY_FUNCTION(profiler::colors::DarkRed);

	for (auto entity : registry.view<EnemyShootingC>())
	{
		auto& enemy_shooting_c = registry.get<EnemyShootingC>(entity);

		enemy_shooting_c.cooldown = clamp<FramesF>(enemy_shooting_c.cooldown - 1, 0, enemy_shooting_c.shoot_cooldown);
	}
}

void DoLaserShoot(EnemyC& enemy_c, EnemyLaserC& enemy_laser_c, TrsC& trs_c, EnemyTargetC& enemy_target_c, vec2& structure_pos)
{
	render.AddLaserToDraw({ trs_c.pos,structure_pos, enemy_laser_c.color, enemy_laser_c.width });

	DmgC dmg{ .dmg = enemy_laser_c.dps * seconds_in_frame };

	enemy_target_c.target.Damage(dmg);
}

void SystemEnemyDoLaser()
{
	for (auto entity : registry.view<EnemyC, EnemyLaserC, EnemyTargetC, EnemyAttackNowCTag, TrsC>())
	{
		auto&& [enemy_c, enemy_laser_c, enemy_target_c, trs_c] = registry.get<EnemyC, EnemyLaserC, EnemyTargetC, TrsC>(entity);

		DoLaserShoot(enemy_c, enemy_laser_c, trs_c, enemy_target_c, enemy_target_c.target_pos);
	}
}

void DoEnemySplit(EnemySplitC& enemy_split_c, TrsC& trs_c)
{
	float angle_step = glm::radians(360.0f / enemy_split_c.count);
	for (int i = 0; i < enemy_split_c.count; i++)
	{
		TrsC trs = trs_c;
		trs.angle += (float)i * angle_step;

		ESpawnEnemy ev{
			.id = enemy_split_c.split_obj_id,
			.trs = trs,
			.speed = enemy_split_c.speed * seconds_in_frame,
		};

		dispatcher.enqueue(ev);
	}
}

void SystemEnemyDoSplit()
{
	for (auto entity : registry.view<EnemyC, EnemySplitC, EnemyTargetC, EnemyAttackNowCTag, TrsC>())
	{
		auto&& [enemy_c, enemy_split_c, enemy_target_c, trs_c] = registry.get<EnemyC, EnemySplitC, EnemyTargetC, TrsC>(entity);

		DoEnemySplit(enemy_split_c, trs_c);

		registry.emplace_or_replace<KillCTag>(entity);
		registry.emplace_or_replace<KillNoBountyCTag>(entity);
	}
}

void SystemEnemyAttack()
{
	SystemEnemyTargeting();

	SystemEnemyDoShoots();
	SystemEnemyCooldown();

	SystemEnemyDoLaser();

	SystemEnemyDoSplit();
}
