#include "SystemEnemyTargetFollow.h"
#include "../components.h"
#include "../registry.h"
#include "../helpers/HelpersEnemy.h"
#include "../GlobalTargets.h"

void SystemEnemyTargetFollowBase()
{
	EASY_FUNCTION(profiler::colors::DarkRed);

	auto func = [](entt::entity entity, TrsC& trs_c, EnemyC& enemy_c, EnemyTargetC& enemy_target_c) {
		vec2 pos = enemy_target_c.target_pos - trs_c.pos;
		trs_c.angle = RotateToAngle(trs_c.angle, atan2(pos.y, pos.x), enemy_c.rotate_speed * seconds_in_frame);

		if (enemy_c.range * enemy_c.range > dot(pos, pos))
		{
			registry.emplace_or_replace<EnemyAttackNowCTag>(entity);
		}
		else
		{
			registry.remove_if_exists<EnemyAttackNowCTag>(entity);
		}
		if (enemy_target_c.target.IsNull())
		{
			EnemyRemoveTarget({ entity });
		}
	};
	registry.view<TrsC, EnemyC, EnemyTargetC>().each(func);
}

void SystemEnemyTargetFollowGt()
{
	EASY_FUNCTION(profiler::colors::DarkRed);

	if (gt.targets.empty()) return;

	auto func = [](entt::entity entity, TrsC& trs_c, EnemyC& enemy_c) {
		auto it = min_element(gt.targets.begin(), gt.targets.end(), [trs_c](vec2 const& a, vec2 const& b) {
			return dot(trs_c.pos - a, trs_c.pos - a) < dot(trs_c.pos - b, trs_c.pos - b);
			});

		vec2 global_target_pos = *it;
		vec2 pos = global_target_pos - trs_c.pos;

		trs_c.angle = RotateToAngle(trs_c.angle, atan2(pos.y, pos.x), enemy_c.rotate_speed * seconds_in_frame);
	};
	registry.view< TrsC, EnemyC >(entt::exclude_t<EnemyTargetC>()).each(func);
}

void SystemEnemyTargetFollow()
{
	EASY_FUNCTION(profiler::colors::DarkRed);

	SystemEnemyTargetFollowBase();
	SystemEnemyTargetFollowGt();
}
