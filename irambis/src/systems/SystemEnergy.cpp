#include "SystemEnergy.h"
#include "../registry.h"
#include "../components.h"
#include "../common/util.h"
#include "../helpers/HelpersStruct.h"

EnergyGroups energy_groups;

void FillGroup(EnergyGroupInfo& e_group, entt::entity start_entity, ConnectGroupId cg_id)
{
	vector<entt::entity> to_expand;
	to_expand.push_back(start_entity);

	auto& start_struct_c = registry.get<StructC>(start_entity);
	start_struct_c.id = cg_id;

	for (u64 i = 0; i < to_expand.size(); i++)
	{
		entt::entity expand_entity = to_expand[i];
		auto& struct_c = registry.get<StructC>(expand_entity);
		struct_c.id = cg_id;
		auto& connect_id_c = registry.get<ConnectIdC>(expand_entity);
		bool is_build = registry.all_of<StructBuildingC>(expand_entity);

		for (auto obj : connect_id_c.connects)
		{
			if (obj.IsNull())
				continue;
			ConnectGroupId& id_child = registry.get<StructC>(obj.entity).id;
			bool is_child_build = registry.all_of<StructBuildingC>(obj.entity);

			if (id_child != cg_id && (!is_build | !is_child_build))
			{
				id_child = cg_id;
				to_expand.push_back(obj.entity);
			}
		}
	}
}

void SystemEnergyClearGroupId()
{
	for (auto entity : registry.view<StructC>())
	{
		auto& struct_c = registry.get<StructC>(entity);
		struct_c.id.id = 0;
	}
}

void SystemEnergyComputeEgId(EnergyGroups& e_groups)
{
	for (auto entity : registry.view<StructC>())
	{
		auto& struct_c = registry.get<StructC>(entity);
		if (struct_c.id.id == 0)
		{
			e_groups.egs.emplace_back();
			auto& e_group = e_groups.egs.back();
			FillGroup(e_group, entity, { e_groups.egs.size() - 1 });
		}
	}
}

void ComputeGroupsInfo(EnergyGroups& e_groups)
{
	for (auto entity : registry.view<StructC>())
	{
		auto& struct_c = registry.get<StructC>(entity);
		struct_c.et = EnergyType::Nothing;
	}

	for (auto entity : registry.view<StructC, EnergyC>())
	{
		auto&& [struct_c, energy_c] = registry.get<StructC, EnergyC>(entity);

		if (registry.all_of<StructBuildingC>(entity))
			continue;

		if (energy_c.en_max == 0)
		{
			struct_c.et = EnergyType::Nothing;
			continue;
		}

		if (energy_c.is_consumer)
			struct_c.et = EnergyType::Consumer;
		else
			struct_c.et = EnergyType::Storage;

		auto& e_group = e_groups.egs[struct_c.id.id];
		e_group.energy_gen += energy_c.gen;
		switch (struct_c.et)
		{
		case EnergyType::Storage: e_group.info_storage.AddStructInfo(energy_c); break;
		case EnergyType::Consumer: e_group.info_consumer.AddStructInfo(energy_c); break;
		default: break;
		}
	}

	for (auto entity : registry.view<StructC, StructBuildingC>())
	{
		auto&& [struct_c, struct_building_c] = registry.get<StructC, StructBuildingC>(entity);

		auto& e_group = e_groups.egs[struct_c.id.id];

		struct_c.et = EnergyType::Build;
		e_group.info_builds.AddStructInfo(struct_building_c);
	}
}

void StructsEnergyInfo::AddStructInfo(const EnergyC& energy_c)
{
	en += energy_c.en;
	en_max += energy_c.en_max;
}

void StructsEnergyInfo::AddStructInfo(const StructBuildingC& energy_c)
{
	en_max += energy_c.en_max;
}

string StructsEnergyInfo::ToString()
{
	return "En: " + ::ToString(en, 1) + "/" + ::ToString(en_max, 1) + " delta percent: " + ::ToString(en_percentage_delta, 6);
}

float StructsEnergyInfo::CurrentNeed()
{
	Assert(en_percentage_delta >= 0);
	return (en_max - en) * (1 - en_percentage_delta);
}

void StructsEnergyInfo::Add(float& energy)
{
	if (energy > CurrentNeed())
	{
		energy -= CurrentNeed();
		en_percentage_delta = 1;
	}
	else
	{
		if (en_max - en > energy_min_delta)
			en_percentage_delta = std::clamp<float>(en_percentage_delta + energy / (en_max - en), 0, 1);
		energy = 0;
	}
}

void StructsEnergyInfo::Rem(float& energy)
{
	if (en < energy)
	{
		energy -= en;
		en_percentage_delta = -1;
	}
	else
	{
		if (en > energy_min_delta)
			en_percentage_delta = -std::clamp<float>(energy / en, 0, 1);
		else
			en_percentage_delta = -1;
		energy = 0;
	}
}

void GroupsProcessUpd(EnergyGroups& e_groups)
{
	EASY_FUNCTION(profiler::colors::DarkBlue);
	for (auto& eg : e_groups.egs)
	{
		float energy_income = eg.energy_gen * seconds_in_frame;
		float energy_in_store = eg.info_storage.en;

		eg.info_builds.Add(energy_income);
		if (energy_income == 0) // HACK float zero compare
		{
			eg.info_builds.Add(energy_in_store);
		}
		else
		{
			eg.info_consumer.Add(energy_income);
		}

		if (energy_income == 0)
		{
			eg.info_consumer.Add(energy_in_store);

			float energy_to_rem = eg.info_storage.en - energy_in_store;

			eg.info_storage.Rem(energy_to_rem);

			Assert(energy_to_rem == 0);
		}
		else
		{
			eg.info_storage.Add(energy_income);
		}
	}

	for (auto entity : registry.view<StructC, EnergyC>())
	{
		auto&& [struct_c, energy_c] = registry.get<StructC, EnergyC>(entity);

		if (struct_c.id.id == 0)
			continue;

		const auto& eg = e_groups.egs[struct_c.id.id];

		switch (struct_c.et)
		{
		case EnergyType::Storage:
			if (eg.info_storage.en_percentage_delta > 0)
			{
				energy_c.en += (energy_c.en_max - energy_c.en) * eg.info_storage.en_percentage_delta;
			}
			else
			{
				energy_c.en = energy_c.en * (1 + eg.info_storage.en_percentage_delta);
			}
			break;
		case EnergyType::Consumer:
			energy_c.en += (energy_c.en_max - energy_c.en) * eg.info_consumer.en_percentage_delta;
			break;
		case EnergyType::Build: AssertMsgFail("Unreachable code"); break;
		case EnergyType::Nothing: break;
		default: AssertMsgFail("Unreachable code"); break;
		}
	}

	for (auto entity : registry.view<StructC, StructBuildingC>())
	{
		auto&& [struct_c, struct_building_c] = registry.get<StructC, StructBuildingC>(entity);
		const auto& eg = e_groups.egs[struct_c.id.id];

		Assert(eg.info_builds.en_percentage_delta >= 0);
		f32 percent_of_build = struct_building_c.ProgressIncrease(eg.info_builds.en_percentage_delta);

		if (registry.all_of<HpC>(entity))
		{
			auto& hp_c = registry.get<HpC>(entity);

			hp_c.hp = clamp(hp_c.hp + struct_building_c.hp_build * percent_of_build, 0.0f, hp_c.hp_max);
		}

		if (struct_building_c.build_progress >= 1)
			StructPerformBuildEnd({ entity });
	}
}

void SystemEnergy()
{
	EASY_FUNCTION(profiler::colors::DarkBlue);
	EnergyGroups e_groups;
	e_groups.egs.resize(1);

	SystemEnergyClearGroupId();
	SystemEnergyComputeEgId(e_groups);
	ComputeGroupsInfo(e_groups);

	GroupsProcessUpd(e_groups);

	energy_groups = e_groups;
}
