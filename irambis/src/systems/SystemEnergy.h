#pragma once
#include "../pch.h"

struct StructBuildingC;
struct EnergyC;

struct StructsEnergyInfo
{
	float en_percentage_delta = 0;
	float en = 0;
	float en_max = 0;
	void AddStructInfo(const EnergyC& energy_c);
	void AddStructInfo(const StructBuildingC& energy_c);
	string ToString();

	float CurrentNeed();
	void Add(float& energy);
	void Rem(float& energy);
};

struct EnergyGroupInfo
{
	float energy_gen = 0;
	
	StructsEnergyInfo info_builds;
	StructsEnergyInfo info_storage;
	StructsEnergyInfo info_consumer;
};

struct EnergyGroups
{
	vector<EnergyGroupInfo> egs;
};

extern EnergyGroups energy_groups;

void SystemEnergy();
