#include "SystemKill.h"
#include "../registry.h"
#include "../components.h"
#include "../GlobalTargets.h"
#include "SystemEnergy.h"

struct GroupTargetInfo
{
	f32 gen = 0;
	vec2 pos;
};

struct GroupTarget
{
	vec2 pos;
	ConnectGroupId id;

	struct StructNearAvg
	{
		vec2 pos;
		f32 dist = 0;
	};

	optional<StructNearAvg> best_solution;

};

vector<GroupTarget> ComputeGroupTargets() // HACK all this function is one big hack
{
	EASY_FUNCTION(profiler::colors::DarkRed);

	vector<GroupTarget> group_targets;
	if (energy_groups.egs.empty())
		return group_targets;

	vector<GroupTargetInfo> group_ti(energy_groups.egs.size());

	for (auto entity : registry.view<StructC, TrsC, EnergyC>())
	{
		auto& struct_c = registry.get<StructC>(entity);
		auto& trs_c = registry.get<TrsC>(entity);
		auto& energy_c = registry.get<EnergyC>(entity);

		auto& ti = group_ti[struct_c.id.id];
		ti.gen += energy_c.gen;
		ti.pos += energy_c.gen * trs_c.pos;
	}

	for (auto& ti : group_ti)
	{
		if (ti.gen > 0)
		{
			ti.pos /= ti.gen;
		}
	}

	auto it = std::max_element(group_ti.begin(), group_ti.end(), [](GroupTargetInfo const& lhs, GroupTargetInfo const& rhs) {
		return lhs.gen < rhs.gen;
		});

	if (it->gen == 0)
		return group_targets;

	ConnectGroupId first_target_id{ (u64)distance(group_ti.begin(),it) };
	f32 first_target_gen = it->gen;

	group_targets.push_back({ it->pos,first_target_id,{} });

	it->gen = -1;
	auto it_st = std::max_element(group_ti.begin(), group_ti.end(), [](GroupTargetInfo const& lhs, GroupTargetInfo const& rhs) {
		return lhs.gen < rhs.gen;
		});

	ConnectGroupId second_target_id{ (u64)distance(group_ti.begin(),it_st) };
	f32 second_target_gen = it_st->gen;

	if (second_target_id == first_target_id || it_st->gen == 0 || second_target_gen < 0.5 * first_target_gen)
		return group_targets;

	group_targets.push_back({ it_st->pos, second_target_id,{} });

	return group_targets;
}

void SystemGlobalTargets()
{
	EASY_FUNCTION(profiler::colors::DarkRed);

	auto target_groups = ComputeGroupTargets();

	for (auto entity : registry.view<StructC, TrsC, EnergyC>())
	{
		auto& struct_c = registry.get<StructC>(entity);
		auto& trs_c = registry.get<TrsC>(entity);
		auto& energy_c = registry.get<EnergyC>(entity);

		if (energy_c.gen > 0)
		{
			auto group_id = struct_c.id;
			auto it = find_if(target_groups.begin(), target_groups.end(), [group_id](GroupTarget const& e) {
				return e.id == group_id;
				});

			if (it != target_groups.end())
			{
				f32 dist_to_avg = distance(trs_c.pos, it->pos);
				if (!it->best_solution)
				{
					it->best_solution = GroupTarget::StructNearAvg{ trs_c.pos, dist_to_avg };
				}
				else
				{
					if (it->best_solution->dist > dist_to_avg)
						it->best_solution = GroupTarget::StructNearAvg{ trs_c.pos, dist_to_avg };
				}
			}
		}
	}

	gt.targets.resize(0);
	for (auto& tg : target_groups)
	{
		if (tg.best_solution)
		{
			gt.targets.push_back(tg.best_solution->pos);
		}
	}
	
	if (gt.targets.empty())
	{
		for (auto entity : registry.view<StructC, TrsC>(entt::exclude<StructBuildingC>))
		{
			gt.targets.push_back(registry.get<TrsC>(entity).pos);
			break;
		}
	}
	
	if (gt.targets.empty())
	{
		for (auto entity : registry.view<StructC, TrsC>())
		{
			gt.targets.push_back(registry.get<TrsC>(entity).pos);
			break;
		}
	}
}

