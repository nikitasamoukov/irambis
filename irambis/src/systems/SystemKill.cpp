#include "SystemKill.h"
#include "../registry.h"
#include "../components.h"
#include "../helpers/Query.h"
#include "../Assets.h"
#include "../Player.h"
#include "../helpers/HelpersParticles.h"

void SystemKillStructKill(entt::entity entity)
{
	Object obj{ entity };
	obj.RemoveAllConnects();

	RemStructFromGrid(obj, obj.GetTrs().pos);
	registry.destroy(entity);
}

void SystemKillAny()
{
	EASY_FUNCTION(profiler::colors::Dark);

	for (entt::entity entity : registry.view<KillCTag>())
	{
		if (registry.all_of<EnemyC>(entity))
		{
			Object obj{ entity };

			auto& bo = assets.GetBoxedObject(obj.GetType());
			if (bo.Has<EnemyCDesc>())
			{
				player.money += bo.Get<EnemyCDesc>().bounty;
			}

			RemEnemyFromGrid(obj, obj.GetTrs().pos);
			if (registry.all_of<TrsC>(obj.entity) && registry.all_of<RenderC>(obj.entity) && registry.all_of<PhysC>(entity))
				SpawnParticlesOnObjectDead(obj.GetTrs(), registry.get<RenderC>(obj.entity).id, 1, registry.get<PhysC>(entity).vel);
			registry.destroy(entity);
			continue;
		}
		if (registry.all_of<BulletC>(entity))
		{
			if (registry.all_of<TrsC>(entity) && registry.all_of<RenderC>(entity))
				SpawnParticlesOnObjectDead(registry.get<TrsC>(entity), registry.get<RenderC>(entity).id, 8);
			registry.destroy(entity);
			continue;
		}
		if (registry.all_of<StructC>(entity))
		{
			if (registry.all_of<TrsC>(entity) && registry.all_of<RenderC>(entity))
				SpawnParticlesOnObjectDead(registry.get<TrsC>(entity), registry.get<RenderC>(entity).id);
			SystemKillStructKill(entity);
			continue;
		}
		AssertMsgFail("Not implemented");
	}
}

void SystemKillNoBounty()
{
	EASY_FUNCTION(profiler::colors::Dark);

	for (entt::entity entity : registry.view<KillCTag, EnemyC, KillNoBountyCTag>())
	{
		Object obj{ entity };

		RemEnemyFromGrid(obj, obj.GetTrs().pos);
		registry.destroy(entity);
	}
}

void SystemKill()
{
	EASY_FUNCTION(profiler::colors::Dark);

	SystemKillNoBounty();
	SystemKillAny();
}

