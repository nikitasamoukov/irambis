#include "SystemLevelEnd.h"
#include "../registry.h"
#include "../components.h"
#include "../gui/LevelEndGui.h"
#include "../Levels.h"

bool HaveSomeStructures()
{
	for (auto entity : registry.view<StructC>(entt::exclude<StructBuildingC>))
	{
		return true;
	}
	return false;
}

bool HaveSomeEnemies()
{
	for (auto entity : registry.view<EnemyC>())
	{
		return true;
	}
	return false;
}

void SystemLevelEnd()
{
	EASY_FUNCTION(profiler::colors::Dark);

	if (HaveSomeStructures())
	{
		if (!HaveSomeEnemies() && levels.IsNoWaves())
			level_end_gui.Enable(true);
	}
	else
		level_end_gui.Enable(false);
}
