#include "SystemParticles.h"
#include "../registry.h"
#include "../components.h"

void SystemParticles()
{
	EASY_FUNCTION(profiler::colors::Dark);

	for (entt::entity entity : registry.view<ParticleLifetimeC>())
	{
		auto& particle_lifetime_c = registry.get<ParticleLifetimeC>(entity);

		particle_lifetime_c.life -= particle_lifetime_c.frame_decay;
		if (particle_lifetime_c.life <= 0)
		{
			registry.destroy(entity);
		}
	}
}
