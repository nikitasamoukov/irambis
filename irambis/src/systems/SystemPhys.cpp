#include "SystemPhys.h"
#include "../registry.h"
#include "../components.h"
#include <glm/gtx/fast_trigonometry.hpp>
#include "../helpers/Query.h"

void SystemPhysMoveEnemy()
{
	EASY_FUNCTION(profiler::colors::DarkRed);
	/*
	float v = 0;
	while (1)
	{
		v *= 0.95f;
		v += 1;
	}*/

	auto func = [](auto entity, TrsC& trs_c, PhysC& phys_c, EnemyC& enemy_c) {
		if (phys_c.is_static)
			return;

		vec2 old_pos = trs_c.pos;
		vec2 new_pos = trs_c.pos + phys_c.vel;

		ProcessEnemyMove({ entity }, old_pos, new_pos);

		trs_c.pos = new_pos;
		phys_c.vel *= 0.95f;

		if (!registry.all_of<EnemyAttackNowCTag>(entity))
			phys_c.vel += vec2(glm::fastCos(trs_c.angle), glm::fastSin(trs_c.angle)) * enemy_c.speed * speed_to_accel_scale;
	};

	registry.view<TrsC, PhysC, EnemyC>().each(func);
}

void SystemPhysMoveBullets()
{
	EASY_FUNCTION(profiler::colors::Dark);
	auto func = [](auto entity, TrsC& trs_c, PhysC& phys_c, BulletC& bullet_c) {
		if (phys_c.is_static)
			return;

		trs_c.pos += phys_c.vel;

		phys_c.vel = vec2(glm::fastCos(trs_c.angle), glm::fastSin(trs_c.angle)) * bullet_c.speed * seconds_in_frame;
	};

	registry.view<TrsC, PhysC, BulletC>().each(func);
}

void SystemPhysMoveParticles()
{
	EASY_FUNCTION(profiler::colors::Dark);
	auto func = [](auto entity, TrsC& trs_c, PhysC& phys_c, ParticleLifetimeC&) {
		if (phys_c.is_static)
			return;

		trs_c.pos += phys_c.vel;
	};

	registry.view<TrsC, PhysC, ParticleLifetimeC>().each(func);
}

void BulletCollisionProcess(Object bullet_obj, Object damaged_obj)
{
	EASY_FUNCTION(profiler::colors::Dark);
	if (!registry.all_of<DmgC>(bullet_obj.entity)) return;
	DmgC& dmg_c = registry.get<DmgC>(bullet_obj.entity);
	damaged_obj.Damage(dmg_c);
	registry.emplace_or_replace<KillCTag>(bullet_obj.entity);
}

void SystemPhysCollisions()
{
	EASY_FUNCTION(profiler::colors::DarkRed);
	auto func = [](auto entity, TrsC& trs_c, PhysC& phys_c, BulletC& bullet_c) {
		if (bullet_c.faction == BulletC::Faction::Enemy)
		{
			Object collided_obj;

			QueryStructs(trs_c.pos, max_struct_radius + phys_c.r, [&](Object obj) {
				auto&& [struct_trs_c, struct_phys_c] = registry.get<TrsC, PhysC>(obj.entity);

				if (distance(struct_trs_c.pos, trs_c.pos) <= phys_c.r + struct_phys_c.r)
					collided_obj = obj;
				});

			if (!collided_obj.IsNull())
			{
				BulletCollisionProcess({ entity }, collided_obj);
			}
		}

		if (bullet_c.faction == BulletC::Faction::Player)
		{
			Object collided_obj;

			QueryEnemies(trs_c.pos, max_enemy_radius + phys_c.r, [&](Object obj) {
				auto&& [enemy_trs_c, enemy_phys_c] = registry.get<TrsC, PhysC>(obj.entity);

				if (distance(enemy_trs_c.pos, trs_c.pos) <= phys_c.r + enemy_phys_c.r)
					collided_obj = obj;
				});

			if (!collided_obj.IsNull())
			{
				BulletCollisionProcess({ entity }, collided_obj);
			}
		}
	};

	registry.view<TrsC, PhysC, BulletC>().each(func);
}

void SystemPhysEnemyRam()
{
	EASY_FUNCTION(profiler::colors::DarkRed);
	auto func = [](auto entity, TrsC& trs_c, PhysC& phys_c, DmgC& dmg_c) {

		Object collided_obj;

		QueryStructs(trs_c.pos, max_struct_radius + phys_c.r, [&](Object obj) {
			auto&& [struct_trs_c, struct_phys_c, struct_c] = registry.get<TrsC, PhysC, StructC>(obj.entity);

			if (distance(struct_trs_c.pos, trs_c.pos) <= phys_c.r + struct_phys_c.r)
				collided_obj = obj;
			});

		if (!collided_obj.IsNull())
		{
			BulletCollisionProcess({ entity }, collided_obj);
		}
	};

	registry.view<TrsC, PhysC, EnemyRamC, DmgC>().each(func);
}

void SystemPhys()
{
	EASY_FUNCTION(profiler::colors::Dark);

	SystemPhysMoveEnemy();
	SystemPhysMoveBullets();
	SystemPhysMoveParticles();
	SystemPhysCollisions();
	SystemPhysEnemyRam();
}

