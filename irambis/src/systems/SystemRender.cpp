#include "SystemRender.h"
#include "../registry.h"
#include "../components.h"
#include "../Render.h"
#include "../helpers/Query.h"

void SystemRenderStructs()
{
	EASY_FUNCTION(profiler::colors::Dark);

	auto group = registry.group<RenderC>(entt::get<TrsC>);
	for (auto entity : group)
	{
		auto& render_c = group.get<RenderC>(entity);
		auto& trs_c = group.get<TrsC>(entity);

		render.AddSpriteToDraw(render_c.id, trs_c);
		//render.AddLaserToDraw({ vec2(GridPosFromObjPos(trs_c.pos)) * grid_cell_size, trs_c.pos, vec3(1, 1, 1), 1 });
	}
}

void SystemRenderStructsTurrets()
{
	EASY_FUNCTION(profiler::colors::Dark);

	auto group = registry.group<RenderTurretC, TurretRotateC>(entt::get<TrsC>);
	for (auto entity : group)
	{
		auto& render_turret_c = group.get<RenderTurretC>(entity);
		auto& turret_rotate_c = group.get<TurretRotateC>(entity);
		TrsC trs{ .pos = group.get<TrsC>(entity).pos, .angle = turret_rotate_c.a };

		render.AddSpriteToDraw(render_turret_c.id, trs);
	}
}

void SystemRender()
{
	EASY_FUNCTION(profiler::colors::Dark);

	SystemRenderStructs();
	SystemRenderStructsTurrets();
}
