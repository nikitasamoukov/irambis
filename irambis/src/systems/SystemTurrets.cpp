#include "SystemTurrets.h"
#include "../registry.h"
#include "../components.h"
#include "../Render.h"
#include "../helpers/Query.h"
#include "../Events.h"
#include "../Assets.h"

void DoLaserShoot(TurretLaserC& turret_laser_c, TrsC& trs_c, TurretTargetC& turret_target_c)
{
	auto& target_trs = turret_target_c.target.GetTrs();
	render.AddLaserToDraw({ trs_c.pos, target_trs.pos, turret_laser_c.color, turret_laser_c.width });

	DmgC dmg{ .dmg = turret_laser_c.dps * seconds_in_frame };

	turret_target_c.target.Damage(dmg);
}

void TargetRepairTurret(Object obj)
{
	auto&& [turret_repair_c, turret_c, trs_c] = registry.get<TurretRepairC, TurretC, TrsC>(obj.entity);

	float best_target_score = -99999999.9f;
	entt::entity best_target_entity = entt::null;

	f32 range = turret_c.range;


	QueryStructs(trs_c.pos, range, [&](Object enemy_obj) {
		auto& other_trs_c = enemy_obj.GetTrs();
		f32 dist = glm::distance(trs_c.pos, other_trs_c.pos);
		f32 target_score = -dist;

		if (!registry.all_of<HpC>(enemy_obj.entity))
		{
			target_score = -999999999.9f;
		}
		else
		{
			auto& hp_c = registry.get<HpC>(enemy_obj.entity);
			if (hp_c.hp < hp_c.hp_max)
				target_score = hp_c.hp_max - hp_c.hp;
			else
				target_score = -999999999.9f;

		}

		if (target_score > best_target_score && dist <= range)
		{
			best_target_score = target_score;
			best_target_entity = enemy_obj.entity;
		}
		});

	if (best_target_entity != entt::null)
	{
		TurretTargetC turret_target_c{ best_target_entity };
		registry.emplace_or_replace<TurretTargetC>(obj.entity, turret_target_c);
	}
}

void SystemTurretsTargeting()
{
	for (auto entity : registry.view<TurretLaserC, TurretC, TrsC>())
	{
		TargetToEnemyInRange({ entity }, registry.get<TurretC>(entity).range);
	}

	for (auto entity : registry.view<TurretHighLaserC, TurretC, TrsC>(entt::exclude<TurretTargetC>))
	{
		TargetToEnemyInRange({ entity }, registry.get<TurretC>(entity).range, true);
	}

	for (auto entity : registry.view<TurretRepairC, TurretC, TrsC>(entt::exclude<TurretTargetC>))
	{
		TargetRepairTurret({ entity });
	}

	for (auto entity : registry.view<TurretCannonC, TurretC, TrsC>(entt::exclude<TurretTargetC>))
	{
		TargetToEnemyInRange({ entity }, registry.get<TurretC>(entity).range);
	}
}

//HACK this file full of copy-paste

void SystemTurretsDoLaserShoots()
{
	EASY_FUNCTION(profiler::colors::Dark);

	for (auto entity : registry.view<TurretC, TurretLaserC, TrsC, EnergyC>())
	{
		auto&& [turret_c, turret_laser_c, trs_c, energy_c] = registry.get<TurretC, TurretLaserC, TrsC, EnergyC>(entity);
		float en_per_frame = turret_laser_c.en_per_second * seconds_in_frame;

		if (turret_laser_c.cooldown == 0)
		{
			if (registry.all_of<TurretTargetC>(entity))
			{
				if (energy_c.en >= en_per_frame)
				{
					auto& turret_target_c = registry.get<TurretTargetC>(entity);
					if (!turret_target_c.target.IsNull())
					{
						auto& target_trs = turret_target_c.target.GetTrs();
						if (distance(target_trs.pos, trs_c.pos) <= turret_c.range)
						{
							energy_c.en -= en_per_frame;
							DoLaserShoot(turret_laser_c, trs_c, turret_target_c);
						}
						else
							registry.remove<TurretTargetC>(entity);
					}
					else
						registry.remove<TurretTargetC>(entity);
				}
				else
				{
					turret_laser_c.cooldown = turret_laser_c.failure_cooldown;
				}
			}
		}
		else
		{
			if (energy_c.en < en_per_frame)
			{
				turret_laser_c.cooldown = max(2, turret_laser_c.cooldown); // HACK to fast start on power up
			}
		}

		turret_laser_c.cooldown = clamp(turret_laser_c.cooldown - 1, 0, turret_laser_c.failure_cooldown);
	}
}

void DoHighLaserShoot(TurretHighLaserC& turret_high_laser_c, TrsC& trs_c, TurretTargetC& turret_target_c)
{
	auto& target_trs = turret_target_c.target.GetTrs();
	render.AddLaserToDraw({ trs_c.pos, target_trs.pos, turret_high_laser_c.color, turret_high_laser_c.width * turret_high_laser_c.charge });

	DmgC dmg{ .dmg = turret_high_laser_c.dps * seconds_in_frame * turret_high_laser_c.charge };

	turret_target_c.target.Damage(dmg);
}

void SystemTurretsDoHighLaserShoots()
{
	EASY_FUNCTION(profiler::colors::Dark);

	for (auto entity : registry.view<TurretC, TurretHighLaserC, TrsC, EnergyC>())
	{
		auto&& [turret_c, turret_high_laser_c, trs_c, energy_c] = registry.get<TurretC, TurretHighLaserC, TrsC, EnergyC>(entity);
		float en_per_frame = turret_high_laser_c.en_per_second * seconds_in_frame * turret_high_laser_c.charge;

		if (turret_high_laser_c.cooldown == 0)
		{
			if (registry.all_of<TurretTargetC>(entity))
			{
				if (energy_c.en >= en_per_frame)
				{
					auto& turret_target_c = registry.get<TurretTargetC>(entity);
					if (!turret_target_c.target.IsNull())
					{
						auto& target_trs = turret_target_c.target.GetTrs();
						if (distance(target_trs.pos, trs_c.pos) <= turret_c.range)
						{
							energy_c.en -= en_per_frame;
							DoHighLaserShoot(turret_high_laser_c, trs_c, turret_target_c);
						}
						else
						{
							registry.remove<TurretTargetC>(entity);
							turret_high_laser_c.charge = 0;
						}
					}
					else
					{
						registry.remove<TurretTargetC>(entity);
						turret_high_laser_c.charge = 0;
					}

					turret_high_laser_c.charge = clamp(turret_high_laser_c.charge + turret_high_laser_c.charge_per_frame, 0.0f, 1.0f);
				}
				else
				{
					turret_high_laser_c.cooldown = turret_high_laser_c.failure_cooldown;
					turret_high_laser_c.charge = 0;
				}
			}
		}
		else
		{
			if (energy_c.en < en_per_frame)
			{
				turret_high_laser_c.cooldown = max(2, turret_high_laser_c.cooldown); // HACK to fast start on power up
			}
		}

		turret_high_laser_c.cooldown = clamp(turret_high_laser_c.cooldown - 1, 0, turret_high_laser_c.failure_cooldown);
	}
}

void DoRepairShoot(TurretRepairC& turret_repair_c, TrsC& trs_c, TurretTargetC& turret_target_c, HpC& target_hp_c)
{
	auto& target_trs = turret_target_c.target.GetTrs();
	render.AddLaserToDraw({ trs_c.pos, target_trs.pos, turret_repair_c.color, turret_repair_c.width });

	f32 repair = turret_repair_c.dps * seconds_in_frame;
	target_hp_c.hp = clamp(target_hp_c.hp + repair, 0.0f, target_hp_c.hp_max);
}

void SystemTurretsDoRepairShoots()
{
	EASY_FUNCTION(profiler::colors::Dark);

	for (auto entity : registry.view<TurretC, TurretRepairC, TrsC, EnergyC>())
	{
		auto&& [turret_c, turret_repair_c, trs_c, energy_c] = registry.get<TurretC, TurretRepairC, TrsC, EnergyC>(entity);
		float en_per_frame = turret_repair_c.en_per_second * seconds_in_frame;

		if (turret_repair_c.cooldown == 0)
		{
			if (registry.all_of<TurretTargetC>(entity))
			{
				if (energy_c.en >= en_per_frame)
				{
					auto& turret_target_c = registry.get<TurretTargetC>(entity);


					if (!turret_target_c.target.IsNull() && registry.all_of<HpC>(turret_target_c.target.entity))
					{
						auto& target_hp_c = registry.get<HpC>(turret_target_c.target.entity);
						auto& target_trs = turret_target_c.target.GetTrs();
						if (target_hp_c.hp < target_hp_c.hp_max && distance(target_trs.pos, trs_c.pos) <= turret_c.range)
						{
							energy_c.en -= en_per_frame;
							DoRepairShoot(turret_repair_c, trs_c, turret_target_c, target_hp_c);
						}
						else
							registry.remove<TurretTargetC>(entity);
					}
					else
						registry.remove<TurretTargetC>(entity);

				}
				else
				{
					turret_repair_c.cooldown = turret_repair_c.failure_cooldown;
				}
			}
		}
		else
		{
			if (energy_c.en < en_per_frame)
			{
				turret_repair_c.cooldown = max(2, turret_repair_c.cooldown); // HACK to fast start on power up
			}
		}

		turret_repair_c.cooldown = clamp(turret_repair_c.cooldown - 1, 0, turret_repair_c.failure_cooldown);
	}
}

void DoCannonShoot(TurretC& turret_c, TurretCannonC& turret_cannon_c, TurretRotateC& turret_rotate_c, TrsC& trs_c, TrsC& target_trs_c)
{
	TrsC trs{ .pos = trs_c.pos, .angle = turret_rotate_c.a };
	EShoot ev{ .range = turret_c.range, .trs = trs, .id = turret_cannon_c.shoot_obj_id };
	dispatcher.enqueue(ev);
}

void DoCannonRotate(TurretCannonC& turret_cannon_c, TurretRotateC& turret_rotate_c, TrsC& trs_c, TrsC& target_trs_c, PhysC& target_phys_c)
{
	vec2 turret_pos = trs_c.pos;
	vec2 target_pos = target_trs_c.pos;
	vec2 target_pos_at_end = target_pos;
	f32 dist = distance(turret_pos, target_pos);

	auto& shot_bo = assets.GetBoxedObject(turret_cannon_c.shoot_obj_id);
	if (shot_bo.Has<BulletC>())
		target_pos_at_end += target_phys_c.vel * frames_per_second * dist / max(0.001f, shot_bo.Get<BulletC>().speed);
	//                              u/f        *     f/s           *  u   /                        u/s = u

	vec2 offset_pos = target_pos_at_end - turret_pos;
	if (offset_pos.x == 0 && offset_pos.y == 0) return; // HACK

	turret_rotate_c.a = atan2(offset_pos.y, offset_pos.x);
}


void SystemTurretsDoCannonShoots()
{
	EASY_FUNCTION(profiler::colors::Dark);

	{
		EASY_BLOCK("cooldown", profiler::colors::Dark);
		for (auto entity : registry.view<TurretCannonC>())
		{
			auto& turret_cannon_c = registry.get< TurretCannonC>(entity);

			turret_cannon_c.cooldown = clamp<FramesF>(turret_cannon_c.cooldown - 1, 0, turret_cannon_c.shoot_cooldown);
		}
	}

	auto view = registry.view<TurretC, TurretCannonC, TurretRotateC, TrsC, EnergyC, TurretTargetC>();
	for (auto entity : view)
	{
		auto&& [turret_c, turret_cannon_c, turret_rotate_c, trs_c, energy_c, turret_target_c] =
			view.get<TurretC, TurretCannonC, TurretRotateC, TrsC, EnergyC, TurretTargetC>(entity);

		if (!turret_target_c.target.IsNull())
		{
			if (auto& target_trs = turret_target_c.target.GetTrs(); distance(target_trs.pos, trs_c.pos) <= turret_c.range)
			{
				if (registry.all_of<PhysC>(turret_target_c.target.entity))
				{
					auto& target_phys_c = registry.get<PhysC>(turret_target_c.target.entity);
					DoCannonRotate(turret_cannon_c, turret_rotate_c, trs_c, target_trs, target_phys_c);
				}
				if (turret_cannon_c.cooldown < 1 && energy_c.en >= turret_cannon_c.en_per_shoot)
				{
					energy_c.en -= turret_cannon_c.en_per_shoot;
					turret_cannon_c.cooldown += turret_cannon_c.shoot_cooldown;
					DoCannonShoot(turret_c, turret_cannon_c, turret_rotate_c, trs_c, target_trs);
				}
			}
			else
				registry.remove<TurretTargetC>(entity);
		}
		else
			registry.remove<TurretTargetC>(entity);
	}
}

void SystemTurrets()
{
	EASY_FUNCTION(profiler::colors::DarkBlue);

	SystemTurretsTargeting();
	SystemTurretsDoLaserShoots();
	SystemTurretsDoHighLaserShoots();
	SystemTurretsDoRepairShoots();
	SystemTurretsDoCannonShoots();
}
